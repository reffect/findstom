<?php

namespace themes\fullcalendar;

use Yii;
use yii\web\AssetBundle;

/**
 * @link http://www.frenzel.net/
 * @author Philipp Frenzel <philipp@frenzel.net>
 */

class Scheduler extends AssetBundle
{
    /**
     * [$sourcePath description]
     * @var string
     */
    public $sourcePath = '@bower/fullcalendar-scheduler/dist';

    
    /**
     * [$css description]
     * @var array
     */
    public $css = [
        'scheduler.min.css',
    ];

    /**
     * [$js description]
     * @var array
     */
    public $js = [
        'scheduler.min.js',
    ];
    
    /**
     * [$depends description]
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii2fullcalendar\MomentAsset',
        'yii2fullcalendar\PrintAsset',
        'themes\fullcalendar\Asset'
    ];


}
