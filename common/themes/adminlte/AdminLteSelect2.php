<?php
/**
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 */

namespace themes\adminlte;

/**
 * Class AdminLteAsset.
 * Register admin-lte-2 to view.
 *
 * @author  Agiel K. Saputra
 * @since   1.0
 */
class AdminLteSelect2 extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/adminlte/plugins/select2';
    public $css = ['select2.css'];
    public $js = ['select2.full.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'codezeen\yii2\fastclick\FastClickAsset',
    ];
}
