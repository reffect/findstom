<?php

use cebe\gravatar\Gravatar;
use codezeen\yii2\adminlte\widgets\Menu;
use yii\helpers\Html;

?>
<aside class="main-sidebar">
    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <?= Gravatar::widget([
                        'email' => Yii::$app->user->identity->email,
                        'options' => [
                            'alt' => Yii::$app->user->identity->username,
                            'class' => 'img-circle',
                        ],
                        'size' => 45,
                    ]) ?>

                </div>
                <div class="pull-left info">
                    <p><?= Yii::$app->user->identity->username; ?></p>
                    <?= Html::a(
                        '<i class="fa fa-circle text-success"></i>' . Yii::t('app', 'Online'),
                        ['/user/profile']
                    ) ?>

                </div>
            </div>
        <?php endif ?>
        <?php
        $adminSiteMenu[] = [
            'label' => Yii::t("common","Home"),
            'url' => ['/'],
            'icon' => 'fa fa-home',
        ];


        $adminSiteMenu[] = [
            'label' => Yii::t("common", "Companies"),
            'icon' => 'fa fa-building-o',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-building',
                    'label' => Yii::t("common", "Companies"),
                    'url' => ['/company/dental-company/index'],
                ],
                [
                    'icon' => 'fa fa-tags',
                    'label' => Yii::t("common", "Category"),
                    'url' => ['/company/dental-category/index'],
                ],
                [
                    'icon' => 'fa fa-tachometer',
                    'label' => Yii::t("common", "Модерация"),
                    'url' => ['/company/dental-company/moderation'],
                ],
                [
                    'icon' => 'fa fa-tachometer',
                    'label' => Yii::t("common", "Модерация Адресов"),
                    'url' => ['/company/dental-company/moderation-address'],
                ],

            ],
        ];
        
        
        $adminSiteMenu[] = [
            'label' => Yii::t("common", "Parameters"),
            'icon' => 'fa fa-list',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-list',
                    'label' => Yii::t("common", "Parameters"),
                    'url' => ['/eav/eav-param/index'],
                ],
                [
                    'icon' => 'fa fa-indent',
                    'label' => Yii::t("common", "Types of parameters"),
                    'url' => ['/eav/eav-param-type/index'],
                ],
                [
                    'icon' => 'fa fa-sliders',
                    'label' => Yii::t("common", "Measure unit"),
                    'url' => ['/eav/eav-measure-unit/index'],
                ],
            ],
        ];


        $adminSiteMenu[] = [
            'label' => Yii::t("common", "Users"),
            'icon' => 'fa fa-users',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-user',
                    'label' => Yii::t("common", "Users"),
                    'url' => ['/user/user/index'],
                ],
                [
                    'icon' => 'fa fa-key',
                    'label' => Yii::t("common", "User roles"),
                    'url' => ['/user/rbac/index'],
                ],
            ],
        ];

        $adminSiteMenu[] = [
            'label' => Yii::t("common", "Seo"),
            'icon' => 'fa fa-th-large',
            'url' => ['/seo/seo/index'],
        ];

        $adminSiteMenu[] = [
            'label' => Yii::t("common", "System"),
            'icon' => 'fa fa-th-large',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-exchange',
                    'label' => Yii::t("common", "Import"),
                    'url' => ['/main/import/index'],
                ],
                [
                    'icon' => 'fa fa-exchange',
                    'label' => Yii::t("common", "Key storage"),
                    'url' => ['/main/key-storage/index'],
                ],
                [
                    'icon' => 'fa fa-map',
                    'label' => "Карта (betta)",
                    'url' => ['/main/default/map'],
                ],
            ],
        ];

        ksort($adminSiteMenu);

        echo Menu::widget([
            'items' => $adminSiteMenu,
        ])
        ?>

    </section>
</aside>
