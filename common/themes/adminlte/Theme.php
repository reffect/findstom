<?php

namespace themes\adminlte;

use yii;
use yii\helpers\Html;

class Theme extends yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@backend/views' => '@themes/adminlte/views',
        '@modules' => '@themes/adminlte/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$container->set('yii\grid\CheckboxColumn', [
            'options' => [
                'style' => 'width: 30px'
            ]
        ]);
         
        Yii::$container->set('yii\grid\GridView', [
            'layout' => "<div class=\"box\"><div class=\"box-body\">{items}\n{pager}</div></div>"
        ]);

        Yii::$container->set('yii\grid\ActionColumn', [
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-eye"></i>', $url, [
                        'class' => 'btn btn-xs btn-success',
                        'title' => 'Просмотр',
                        'data-pjax' => 0,
                    ]);
                },
                'view_slug' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-eye"></i>', ['view', 'slug'=>$model['slug']], [
                        'class' => 'btn btn-xs btn-success',
                        'title' => 'Просмотр',
                        'data-pjax' => 0,
                    ]);
                },
                'view2' => function ($url, $model, $key) {
                    $url = str_replace('view2', 'view', $url);
                    return Html::a('<i class="fa fa-eye"></i> '.Yii::t("common","View"), $url, [
                        'class' => 'btn btn-xs btn-success',
                        'title' => Yii::t("common","View"),
                        'data-pjax' => 0,
                    ]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-edit"></i>', $url, [
                        'class' => 'btn btn-xs btn-primary',
                        'title' => 'Изменить',
                        'data-pjax' => 0,
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-trash-o"></i>', $url, [
                        'class' => 'btn btn-xs btn-danger', 'data-method' => 'post',
                        'title' => 'Удалить',
                        'data' => [
                            'pjax' => 0,
                            'confirm' => 'Вы уверены, что хотите удалить этот элемент?'
                        ],
                    ]);
                }
            ],
            'template' => '<div class="text-center">{view} {update} {delete}</div>'
        ]);
    }
}
