<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use themes\smartadmin\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$asset = Yii::$app->assetManager->getPublishedUrl('@themes/smartadmin/assets/');

if (isset($this->params['place'])){
    $this->registerJs('
        var place = $("#place-' . Html::encode($this->params['place']) . '");
        if (place.parent().parent().prop("tagName") == "LI"){
            place.parent().parent().addClass("open");
            place.parent().show();
        }
        place.addClass("active");
    ');
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="smart-style-0">

<!-- #HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"> <img src="<?= $asset ?>/img/logo.png" alt="SmartAdmin"> </span>
        <!-- END LOGO PLACEHOLDER -->
    </div>

    <!-- #TOGGLE LAYOUT BUTTONS -->
    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right hidden-lg hidden-md">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
            <li class="">
                <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                    <img src="<?= $asset ?>/img/avatars/sunny.png" alt="John Doe" class="online" />
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#ajax/profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?= Url::to(['/users/user/logout']) ?>" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                    </li>
                </ul>
            </li>
        </ul>



        <!-- logout button -->
        <div class="btn-header transparent pull-right">
            <span>
                <a href="<?= Url::to(['/users/user/logout']) ?>" title="Выход">
                    <i class="fa fa-sign-out"></i>
                </a>
            </span>
        </div>
        <!-- end logout button -->
        <div class="btn-header transparent pull-right">
            <?php if (Yii::$app->user->id !== 1 &&
                isset($_COOKIE['admin_reaut']) &&
                $_COOKIE['admin_reaut'] === md5('731'.date("Y-m-d"))
            ) { ?>
                <span>
                    <a href="<?= Url::to(['/agency/agency/auth-admin']) ?>" title="Войти как админ">
                        <i class="fa fa-reply-all"></i> Варнутся к админу
                    </a>
                </span>
            <?php } ?>
        </div>
    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- #NAVIGATION -->
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS/SASS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <img src="<?= $asset ?>/img/avatars/sunny.png" alt="me" class="online" />
						<span>
							<?= Yii::$app->user->identity->name ?>
						</span>
                        <i class="fa fa-angle-down"></i>
                    </a>

				</span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>
        <!--
        NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
        -->


        <?php $adminSiteMenu[] = [
            'label' => Yii::t("common", "System"),
            'icon' => 'fa fa-th-large',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-exchange',
                    'label' => Yii::t("common", "Import"),
                    'url' => ['/main/import/index'],
                ],
                [
                    'icon' => 'fa fa-map',
                    'label' => "Карта (betta)",
                    'url' => ['/main/default/map'],
                ],
            ],
        ];

        ksort($adminSiteMenu);

        echo \yii\widgets\Menu::widget([
                'items' => $adminSiteMenu,
            ])
        ?>

        <ul>
            <?php if (Yii::$app->user->can('admin')): ?>

                <li id="place-agency">
                    <a href="<?= Url::to(['/agency/agency/index']) ?>">
                        <i class="fa fa-lg fa-fw fa-building"></i>
                        <span class="menu-item-parent"><?=Yii::t('common','Agencies');?></span>
                    </a>
                </li>

            <?php else: ?>

                <li id="place-agency">
                    <a href="<?= Url::to(['/agency/agency/view','id'=>Yii::$app->user->identity->agency_id]) ?>">
                        <i class="fa fa-lg fa-fw fa-building-o"></i>
                        <span class="menu-item-parent"><?=Yii::t('common','My Agency');?></span>
                    </a>
                </li>

            <?php endif; ?>

            <li id="place-rent">
                <a href="<?= Url::to(['/agency/rent/index']) ?>">
                    <i class="fa fa-lg fa-fw fa-building-o"></i>
                    <span class="menu-item-parent"><?=Yii::t('common','Objects fot rent');?></span>
                </a>
            </li>


            <li id="place-forRent">
                <a href="<?= Url::to(['/agency/for-rent/index']) ?>">
                    <i class="fa fa-lg fa-fw fa-calendar"></i>
                    <span class="menu-item-parent"><?=Yii::t('common', 'Calendar');?></span>
                </a>
            </li>


            <li id="place-clients">
                <a href="<?= Url::to(['/agency/clients/index']) ?>">
                    <i class="fa fa-lg fa-fw fa-users"></i>
                    <span class="menu-item-parent"><?=Yii::t('common','Clients');?></span>
                </a>
            </li>

            <li id="place-workers">
                <a href="<?= Url::to(['/agency/workers/index']) ?>">
                    <i class="fa fa-lg fa-fw fa-wrench "></i>
                    <span class="menu-item-parent"><?=Yii::t('common','Workers');?></span>
                </a>
            </li>


            <li class="">
                <a href="<?= Url::to(['/main/arenda-mail/index']) ?>">
                    <i class="fa fa-lg fa-fw fa-inbox"></i>
                    <span class="menu-item-parent"><?= Yii::t("common", "Sent emails"); ?></span>
                </a>
            </li>



            <?php if (Yii::$app->user->can('users')): ?>
            <li>
                <a href="#">
                    <i class="fa fa-lg fa-fw fa-users"></i>
                    <span class="menu-item-parent"><?=Yii::t('common','Users');?></span>
                    <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>
                </a>
                <ul style="display: none;">

                    <li id="place-users">
                        <a href="<?= Url::to(['/users']) ?>">
                            <i class="fa fa-lg fa-fw fa-users"></i>
                            <span class="menu-item-parent"><?=Yii::t('common','Users');?></span>
                        </a>
                    </li>
                    <?php if (Yii::$app->user->can('roles_view')): ?>
                        <li id="place-roles">
                            <a href="<?= Url::to(['/rbac/roles']) ?>">
                                <i class="fa fa-lg fa-fw fa-puzzle-piece"></i>
                                <span class="menu-item-parent"><?=Yii::t('common','User roles');?></span>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('rules_view')): ?>
                        <li id="place-rules">
                            <a href="<?= Url::to(['/rbac/rules']) ?>">
                                <i class="fa fa-lg fa-fw fa-male"></i>
                                <span class="menu-item-parent"><?=Yii::t('common','Access rules');?></span>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('permissions_view')): ?>
                        <li id="place-rbac">
                            <a href="<?= Url::to(['/rbac']) ?>">
                                <i class="fa fa-lg fa-fw fa-unlock-alt"></i>
                                <span class="menu-item-parent"><?=Yii::t('common','Permissions');?></span>
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
            </li>
            <?php endif ?>
            <?php /* if (
                Yii::$app->user->can('product_view') ||
                Yii::$app->user->can('import_view') ||
                Yii::$app->user->can('moderation_view')
            ): ?>
                <li>
                    <a href="#">
                        <i class="fa fa-lg fa-fw fa-cubes"></i>
                        <span class="menu-item-parent">Товары</span>
                        <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>
                    </a>
                    <ul style="display: none;">
                        <?php if (Yii::$app->user->can('product_view')): ?>
                            <li id="place-product-default">
                                <a href="<?= Url::to(['/product']) ?>">Каталог</a>
                            </li>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->can('import_view')): ?>
                            <li id="place-product-import">
                                <a href="<?= Url::to(['/product/import']) ?>">Импорт</a>
                            </li>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->can('moderation_view')): ?>
                            <li id="place-product-moderation">
                                <a href="<?= Url::to(['/product/moderation']) ?>">На модерации</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('category_crud')): ?>
                <li id="place-product-category">
                    <a href="<?= Url::to(['/product/category']) ?>">
                        <i class="fa fa-fw fa-fw fa-code-fork"></i>
                        <span class="menu-item-parent">Категории</span>
                    </a>
                </li>
            <?php endif; ?>
            */ ?>

            <?php if (Yii::$app->user->can('log_view')): ?>
                <li id="place-log">
                    <a href="<?= Url::to(['/log']) ?>">
                        <i class="fa fa-lg fa-fw fa-retweet"></i>
                        <span class="menu-item-parent"><?=Yii::t('common','Logs action');?></span>
                    </a>
                </li>
            <?php endif ?>
        </ul>
    </nav>
</aside>
<!-- END NAVIGATION -->

<!-- #MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <a href="<?= Url::to() ?>" class="ribbon-button-alignment">
		    <span class="btn btn-ribbon">
                <i class="fa fa-refresh"></i>
            </span>
        </a>

        <!-- breadcrumb -->
        <?= Breadcrumbs::widget([
            'encodeLabels' => false,
            'homeLink' => [
                'label' => Yii::t('common','Home'),
                'url' => ['/']
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
        ]) ?>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- #MAIN CONTENT -->
    <div id="content">

        <div class="row">

            <!-- col -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <?php if (isset($this->params['pageTitle'])): ?>
                        <!-- PAGE HEADER -->
                        <?php if (isset($this->params['pageImage'])): ?>
                            <img width="48" src="<?= $this->params['pageImage'] ?>" alt=""/>
                        <?php else: ?>
                            <?php if (isset($this->params['pageIcon'])): ?>
                                <i class="fa-fw fa fa-<?= $this->params['pageIcon'] ?>"></i>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?= ($this->params['pageTitle']); ?>
                    <?php endif; ?>
                </h1>
            </div>

            <div class="col-xs-12">
                <div class="row">

                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="jarviswidget">
                            <div>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            </div>
                        </div>
                    <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                        <div class="jarviswidget">
                            <div>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?= $content ?>
                </div>
            </div>

        </div>

    </div>

    <!-- END #MAIN CONTENT -->

</div>
<!-- END #MAIN PANEL -->

<!-- #PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?= Yii::$app->name ?> © <?= date('Y') ?></span>
        </div>

        <div class="col-xs-6 col-sm-6 text-right hidden-xs">
            <div class="txt-color-white inline-block">
                <div class="btn-group dropup">
                    <button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
                        <i class="fa fa-link"></i> <span class="caret"></span>
                    </button>
                </div>
                <!-- end btn-group-->
            </div>
            <!-- end div-->
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>
<!-- END FOOTER -->

<!-- #SHORTCUT AREA : With large tiles (activated via clicking user name tag)
     Note: These tiles are completely responsive, you can add as many as you like -->
<div id="shortcut">
    <ul>
        <li>
            <a href="<?= Url::to(['/users/default/update', 'id' => Yii::$app->user->id]) ?>"
               class="jarvismetro-tile big-cubes bg-color-blueLight">
                <span class="iconbox">
                    <i class="fa fa-user fa-4x"></i>
                    <span>
                        <?=Yii::t('common','Profile');?>
                    </span>
                </span>
            </a>
            <a href="<?= Url::to(['/users/default/gcalendar']) ?>" class="jarvismetro-tile big-cubes bg-color-greenDark">
                <span class="iconbox">
                    <i class="fa fa-calendar fa-4x"></i>
                    <span>
                        <?=Yii::t('common','Calendar');?>
                    </span>
                </span>
            </a>
            <?php if (Yii::$app->user->can('admin')): ?>
                <a href="<?= Url::to(['/agency/agency-tariff/index']) ?>" class="jarvismetro-tile big-cubes bg-color-blueDark">
                    <span class="iconbox">
                        <i class="fa fa-money fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Tariffs');?>
                        </span>
                    </span>
                </a>
                <a href="<?= Url::to(['/eav/eav-param/index']) ?>" class="jarvismetro-tile big-cubes bg-color-blue">
                    <span class="iconbox">
                        <i class="fa fa-sitemap fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Parameters');?>
                        </span>
                    </span>
                </a>
                <a href="<?= Url::to(['/agency/default/trash']) ?>" class="jarvismetro-tile big-cubes bg-color-red">
                    <span class="iconbox">
                        <i class="fa fa-trash-o fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Trash');?>
                        </span>
                    </span>
                </a>
                <a href="<?= Url::to(['/main/key-storage/index']) ?>"
                   class="jarvismetro-tile big-cubes bg-color-greenLight">
                    <span class="iconbox">
                        <i class="fa fa-gears fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Settings');?>
                        </span>
                    </span>
                </a>
            <?php endif; ?>
        </li>
    </ul>
</div>
<!-- END SHORTCUT AREA -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
