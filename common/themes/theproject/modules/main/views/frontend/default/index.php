<?php

use yii\helpers\Url;
use themes\theproject\assets\TheProjectAsset;

/* @var $this yii\web\View */
/* @var $countClinics int */
/* @var $seo \common\modules\seo\models\Seo */
/* @var $geo \modules\geo\components\Geo */

$theme = TheProjectAsset::register($this);
$domainSlug = Yii::$app->geo->slug;

$countClinics = empty($countClinics)?0:$countClinics;

?>


<!-- section start -->
<!-- ================ -->
<section class="pv-30 light-gray-bg clearfix">
    <div class="container">
        <h1 class="title logo-font text-center text-default">
            <?= $seo->seo_h1; ?>
        </h1>
        <div class="separator"></div>
        <p class="text-center">
            <?= $seo->body; ?>
        </p>
        <br>
        <div class="row grid-space-10">
            <div class="col-sm-6 col-md-3">
                <div class="pv-30 ph-20 white-bg feature-box bordered text-center">
                    <span class="icon default-bg circle"><i class="fa fa-hospital-o"></i></span>
                    <h3>Стоматологии</h3>
                    <div class="separator clearfix"></div>
                    <p>Мы собрали все стоматологии <?= $geo->titlePp; ?> в одну базу,
                        чтобы вы могли найти клинику рядом с домом или работой.</p>
                    <a href="<?= Url::to([
                        '/company/company/index',
                        'city' => $domainSlug
                    ]);?>" class="btn btn-default btn-animated">
                        <?php /*= \MessageFormatter::formatMessage(
                            'ru-RU', '{count, plural, one{# клиника} few{# клиники} many{# клиник} other{# клиники}}', ['count' => $countClinics]);
                        ; */ echo $countClinics?> клиники
                        <i class="fa fa-angle-double-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="pv-30 ph-20 white-bg feature-box bordered text-center">
                    <span class="icon default-bg circle"><i class="fa fa-stethoscope"></i></span>
                    <h3>Врачи</h3>
                    <div class="separator clearfix"></div>
                    <p>Записаться к врачу-стоматологу за пару минут — легко, если пользоваться FindStom.
                        У нас вы найдете актуальные места работы лучших врачей <?= $geo->titlePp; ?>.</p>
                    <a href="#" class="btn btn-default btn-animated">
                        0 врача
                        <i class="pl-5 fa fa-angle-double-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="pv-30 ph-20 default-bg feature-box bordered text-center">
                    <span class="icon dark-bg circle"><i class="fa fa-ambulance"></i></span>
                    <h3>Подобрать клинику</h3>
                    <div class="separator clearfix"></div>
                    <p>Подберите стоматологическую клинику в два клика!
                        Наша база охватывает весь город, в ней есть учреждения
                        с доступными ценами и широким перечнем услуг. </p>
                    <a href="#" class="btn btn-default btn-animated">
                        Поиск
                        <i class="pl-5 fa fa-binoculars"></i>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="pv-30 ph-20 white-bg feature-box bordered text-center">
                    <span class="icon default-bg circle"><i class="glyphicon glyphicon-time"></i></span>
                    <h3>Статьи</h3>
                    <div class="separator clearfix"></div>
                    <p>Интересные статьи и научные публикации о последних
                        новостях из области стоматологии.</p>
                    <a href="#" class="btn btn-default btn-animated">
                        Читать
                        <i class="pl-5 fa fa-angle-double-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

<?php /*
<!-- section start -->
<!-- ================ -->
<section class="section clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Latest <span class="text-default">News</span></h3>
                <div class="separator-2"></div>
                <div class="block">
                    <div class="media margin-clear">
                        <div class="media-left">
                            <div class="overlay-container">
                                <img class="media-object" src="images/medical-blog-thumb-1.jpg" alt="blog-thumb">
                                <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet</a></h5>
                            <p class="small"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
                            <p class="margin-clear small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt corrupti, cum exercitationem.</p>
                            <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                        </div>
                        <hr>
                    </div>
                    <div class="media margin-clear">
                        <div class="media-left">
                            <div class="overlay-container">
                                <img class="media-object" src="images/medical-blog-thumb-2.jpg" alt="blog-thumb">
                                <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"><a href="blog-post.html">Perspiciatis laborum necess</a></h5>
                            <p class="small"><i class="fa fa-calendar pr-10"></i>Mar 22, 2015</p>
                            <p class="margin-clear small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt corrupti, cum exercitationem.</p>
                            <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                        </div>
                        <hr>
                    </div>
                    <div class="media margin-clear">
                        <div class="media-left">
                            <div class="overlay-container">
                                <img class="media-object" src="images/medical-blog-thumb-3.jpg" alt="blog-thumb">
                                <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"><a href="blog-post.html">Cum eligendi nisi rerum porro</a></h5>
                            <p class="small"><i class="fa fa-calendar pr-10"></i>Mar 21, 2015</p>
                            <p class="margin-clear small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt corrupti, cum exercitationem.</p>
                            <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h3>Our <span class="text-default">Departments</span></h3>
                <div class="separator-2"></div>
                <!-- accordion start -->
                <!-- ================ -->
                <div class="panel-group collapse-style-2" id="accordion-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
                                    <i class="fa fa-eye pr-10"></i>Ophthalmology
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne-2" class="panel-collapse collapse in">
                            <div class="panel-body bordered p-15">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officiaasd. <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
                                    <i class="fa fa-leaf pr-10"></i>Primary care
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo-2" class="panel-collapse collapse">
                            <div class="panel-body bordered p-15">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officiaasd. <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                    <i class="icon-users-1 pr-10"></i>Paediatrics
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree-2" class="panel-collapse collapse">
                            <div class="panel-body bordered p-15">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officiaasd. <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                    <i class="fa-female fa pr-10"></i>Gynaecology
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour-2" class="panel-collapse collapse">
                            <div class="panel-body bordered p-15">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officiaasd. <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                    <i class="fa-road fa pr-10"></i>Orthopaedics
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive-2" class="panel-collapse collapse">
                            <div class="panel-body bordered p-15">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officiaasd. <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                    <i class="fa-heart fa pr-10"></i>Cardiology
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix-2" class="panel-collapse collapse">
                            <div class="panel-body bordered p-15">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officiaasd. <a href="#">Read More<i class="fa fa-long-arrow-right pl-5"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- accordion end -->
            </div>
            <div class="col-md-3">
                <h3>Health <span class="text-default">Services</span></h3>
                <div class="separator-2"></div>
                <div class="owl-carousel content-slider-with-large-controls-autoplay dark-controls light-gray-bg buttons-hide">
                    <div class="image-box style-2">
                        <div class="overlay-container overlay-visible">
                            <img src="images/medical-service-1.jpg" alt="">
                            <a href="#" class="overlay-link"><i class="fa fa-link"></i></a>
                            <div class="overlay-bottom hidden-xs">
                                <div class="text">
                                    <p class="lead margin-clear text-left">Medical Education</p>
                                </div>
                            </div>
                        </div>
                        <div class="body padding-horizontal-clear">
                            <p>Lorem ipsum dolor sit amet, consectetur adipis elit. Aliquam atque ipsam nihil dood truck quinoa.</p>
                            <a class="link-dark" href="page-services.html">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="image-box style-2">
                        <div class="overlay-container overlay-visible">
                            <img src="images/medical-service-2.jpg" alt="">
                            <a href="#" class="overlay-link"><i class="fa fa-link"></i></a>
                            <div class="overlay-bottom hidden-xs">
                                <div class="text">
                                    <p class="lead margin-clear text-left">Seminars For Medicals Professionals</p>
                                </div>
                            </div>
                        </div>
                        <div class="body padding-horizontal-clear">
                            <p>Lorem ipsum dolor sit amet, consectetur adipis elit. Aliquam atque ipsam nihil dood truck quinoa.</p>
                            <a class="link-dark" href="page-services.html">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    <div class="image-box style-2">
                        <div class="overlay-container overlay-visible">
                            <img src="images/medical-service-3.jpg" alt="">
                            <a href="#" class="overlay-link"><i class="fa fa-link"></i></a>
                            <div class="overlay-bottom hidden-xs">
                                <div class="text">
                                    <p class="lead margin-clear text-left">Research &amp; Development</p>
                                </div>
                            </div>
                        </div>
                        <div class="body padding-horizontal-clear">
                            <p>Lorem ipsum dolor sit amet, consectetur adipis elit. Aliquam atque ipsam nihil dood truck quinoa.</p>
                            <a class="link-dark" href="page-services.html">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->
 */ ?>