<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;


?>


<!-- main-container start -->
<!-- ================ -->
<div class="main-container parallax jumbotron border-clear light-translucent-bg text-center margin-clear" style="background-image:url('images/fullscreen-bg.jpg');">
    <div class="container">
        <div class="row">
            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-6 col-md-offset-3 pv-40">
                <h1 class="page-title"><span class="text-default"><?=$exception->statusCode;?></span></h1>
                <h2><?= Html::encode($this->title) ?></h2>
                <p><?= nl2br(Html::encode($message)) ?></p>
                <form role="search">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Search">
                        <i class="fa fa-search form-control-feedback"></i>
                    </div>
                </form>
                <a href="javascript:history.back();" class="btn btn-default-transparent btn-lg">
                    <i class="fa fa-chevron-left pr-10"></i>
                    <?= Yii::t("common", "Come back"); ?>
                </a>
                <a href="/" class="btn btn-default btn-animated btn-lg">
                    <?= Yii::t("common", "Return Home"); ?>
                    <i class="fa fa-home"></i>
                </a>
            </div>
            <!-- main end -->
        </div>
    </div>
</div>
<!-- main-container end -->
