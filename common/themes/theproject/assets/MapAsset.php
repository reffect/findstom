<?php

namespace themes\theproject\assets;

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapAsset;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MapAsset extends AssetBundle
{
    public $sourcePath = '@common/themes/theproject/assets';

    public $css = [

    ];
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        'js/google.map.config.js'
    ];



    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'themes\theproject\assets\TheProjectAsset',
    ];
}
