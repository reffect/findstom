<?php

namespace themes\theproject\assets;

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapAsset;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TheProjectAsset extends AssetBundle
{
    public $sourcePath = '@common/themes/theproject/assets';

    public $css = [

        'http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic',
        'http://fonts.googleapis.com/css?family=Raleway:700,400,300',
        'http://fonts.googleapis.com/css?family=Pacifico',
        'http://fonts.googleapis.com/css?family=PT+Serif',

        'fonts/fontello/css/fontello.css',

        'plugins/magnific-popup/magnific-popup.css',
        'plugins/rs-plugin/css/settings.css',
        'css/animations.css',
        'plugins/owl-carousel/owl.carousel.css',
        'plugins/owl-carousel/owl.transitions.css',
        'plugins/hover/hover-min.css',
        'css/style.css',
        'css/typography-default.css',
        'css/skins/dark_cyan.css',
        'css/custom.css',
    ];
    public $js = [
        'plugins/modernizr.js',
        'plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js?rev=5.0',
        'plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js?rev=5.0',
        'plugins/isotope/isotope.pkgd.min.js',
        'plugins/magnific-popup/jquery.magnific-popup.min.js',
        'plugins/waypoints/jquery.waypoints.min.js',
        'plugins/jquery.countTo.js',
        'plugins/jquery.parallax-1.1.3.js',
        'plugins/jquery.validate.js',
        'plugins/vide/jquery.vide.js',
        'plugins/owl-carousel/owl.carousel.js',
//        'plugins/pace/pace.min.js',
        'plugins/jquery.browser.js',
        'plugins/SmoothScroll.js',
        'js/template.js',
        'js/custom.js',
    ];



    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'themes\theproject\assets\Html5shiv',
        'themes\theproject\assets\FontAwesome',
    ];
}
