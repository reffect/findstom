<?php

namespace themes\theproject;

use yii; 

class Theme extends yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@themes/theproject/views',
        '@modules' => '@themes/theproject/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
