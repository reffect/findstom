<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use themes\theproject\assets\TheProjectAsset;

$theme = TheProjectAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="<?=Yii::$app->language;?>" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?=Yii::$app->language;?>">
<!--<![endif]-->

<head>
    <meta charset="<?php echo Yii::$app->charset ?>"/>
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo Html::encode($this->title) ?></title>

    <meta name="author" content="rabadan.ru">
    <?php $this->head() ?>
    <?php echo Html::csrfMetaTags() ?>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=$theme->baseUrl;?>/images/favicon.ico">
</head>
<?php $this->beginBody() ?>
<!-- body classes:  -->
<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
<!-- "gradient-background-header": applies gradient background to header -->
<!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
<body class="no-trans transparent-header ">
<?php echo $content ?>
<?php if (file_exists(__DIR__."_stat.php")) echo $this->render("_stat.php");?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>