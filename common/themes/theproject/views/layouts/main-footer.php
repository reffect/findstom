<?php

use common\modules\main\models\KeyStorageItem;

/* @var $this \yii\web\View */
/* @var $theme \yii\web\AssetBundle */

$titleCityPp = Yii::$app->geo->titlePp;
$footerText = isset(Yii::$app->params['footerText']) ?
    Yii::$app->params['footerText'] :
    "Полная база стоматологий {$titleCityPp}. Здесь вы найдете хорошую стоматологическую клинику с европейскими стандартами качества лечения и комфортной обстановкой.";

?>
<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
<!-- ================ -->
<footer id="footer" class="clearfix dark">

    <!-- .footer start -->
    <!-- ================ -->
    <div class="footer">
        <div class="container">
            <div class="footer-inner">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="footer-content text-center padding-ver-clear">
                            <div class="logo-footer">
                                <img id="logo-footer" class="center-block"
                                     src="<?= "{$theme->baseUrl}/img/logo_black.png"; ?>" alt="">
                            </div>

                            <p>
                                <?= $footerText; ?>
                            </p>

                            <?php if (file_exists(__DIR__."/_stat.php")) {
                                echo $this->render("_stat.php");
                            } ?>

                            <ul class="social-links circle animated-effect-1 margin-clear">
                                <li class="facebook">
                                    <a target="_blank" href="<?=KeyStorageItem::getValue("social.fb");?>">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="facebook">
                                    <a target="_blank" href="<?=KeyStorageItem::getValue("social.vk");?>">
                                        <i class="fa fa-vk"></i>
                                    </a>
                                </li>
                                <li class="twitter">
                                    <a target="_blank" href="<?=KeyStorageItem::getValue("social.twitter");?>">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                            </ul>
                            <br />
                            <a href="/sitemap">Карта сайта</a>
                            <div class="separator"></div>
                            <p class="text-center margin-clear">
                                The Project by
                                <a target="_blank" href="http://rabadan.ru">rabadan731</a>
                                © 2016
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->
</footer>
<!-- footer end -->