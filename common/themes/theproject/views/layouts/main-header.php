<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use common\modules\main\models\KeyStorageItem;

/* @var $this \yii\web\View */
/* @var $thisUser \rabadan731\users\models\User */
/* @var $theme \yii\web\AssetBundle */

$thisUser = Yii::$app->user->identity;

?>
<!-- header-container start -->
<div class="header-container">
    <!-- header-top start -->
    <!-- classes:  -->
    <!-- "dark": dark version of header top e.g. class="header-top dark" -->
    <!-- "colored": colored version of header top e.g. class="header-top colored" -->
    <!-- ================ -->
    <div class="header-top colored">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 col-sm-5">
                    <!-- header-top-first start -->
                    <!-- ================ -->
                    <div class="header-top-first clearfix">
                        <ul class="social-links circle small clearfix hidden-xs">
                            <li class="facebook">
                                <a target="_blank" href="<?=KeyStorageItem::getValue("social.fb");?>">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="facebook">
                                <a target="_blank" href="<?=KeyStorageItem::getValue("social.vk");?>">
                                    <i class="fa fa-vk"></i>
                                </a>
                            </li>
                            <li class="twitter">
                                <a target="_blank" href="<?=KeyStorageItem::getValue("social.twitter");?>">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="social-links hidden-lg hidden-md hidden-sm circle small">
                            <div class="btn-group dropdown">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i
                                        class="fa fa-share-alt"></i></button>
                                <ul class="dropdown-menu dropdown-animation">
                                    <li class="facebook">
                                        <a target="_blank" href="<?=KeyStorageItem::getValue("social.fb");?>">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="facebook">
                                        <a target="_blank" href="<?=KeyStorageItem::getValue("social.vk");?>">
                                            <i class="fa fa-vk"></i>
                                        </a>
                                    </li>
                                    <li class="twitter">
                                        <a target="_blank" href="<?=KeyStorageItem::getValue("social.twitter");?>">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- header-top-first end -->
                </div>
                <div class="col-xs-10 col-sm-7">

                    <!-- header-top-second start -->
                    <!-- ================ -->
                    <div id="header-top-second" class="clearfix text-right">
                        <nav>
                            <ul class="list-inline">
                                <li>Мой регион:</li>
                                <li>
                                    <?= Html::a(Yii::$app->geo->title, ['/setregion']); ?>
                                </li>
                                <li> |</li>
                            </ul>
                            <?= Menu::widget([
                                'options' => ['class' => 'list-inline'],
                                'encodeLabels' => 0,
                                'linkTemplate' => '<a class="link-light" href="{url}">{label}</a>',
                                'items' => [
                                    [
                                        'label' => "<i class=\"fa fa-user-md pr-5\"></i> ".(Yii::$app->user->isGuest?"Гость":$thisUser->username),
                                        'url' => ['/users/default/view', 'id' => Yii::$app->user->id],
                                        'visible' => !Yii::$app->user->isGuest
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Login'),
                                        'url' => ['/login'], 'visible' => Yii::$app->user->isGuest
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Sign up'),
                                        'url' => ['/signup'], 'visible' => Yii::$app->user->isGuest
                                    ],
                                    [
                                        'label' => "<i class=\"fa fa-flask pr-5\"></i>" . Yii::t('common', 'Backend'),
                                        'url' => ['/backend'],
                                        'visible' => Yii::$app->user->can('backend')
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Logout'),
                                        'url' => ['/logout'],
                                        'visible' => !Yii::$app->user->isGuest
                                    ]
                                ]
                            ]); ?>
                        </nav>
                    </div>
                    <!-- header-top-second end -->
                </div>
            </div>
        </div>
    </div>
    <!-- header-top end -->

    <!-- header start -->
    <!-- classes:  -->
    <!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
    <!-- "dark": dark version of header e.g. class="header dark clearfix" -->
    <!-- "full-width": mandatory class for the full-width menu layout -->
    <!-- "centered": mandatory class for the centered logo layout -->
    <!-- ================ -->
    <header class="header  fixed   clearfix">

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- header-left start -->
                    <!-- ================ -->
                    <div class="header-left clearfix">
                        <!-- logo -->
                        <div id="logo" class="logo">
                            <?= Html::a(Html::img("{$theme->baseUrl}/img/logo.png", [
                                'id' => 'logo_img',
                                'alt' => Yii::$app->name
                            ]), ["/main/default/index", "city" => Yii::$app->geo->slug]); ?>
                        </div>
                    </div>
                    <!-- header-left end -->

                </div>
                <div class="col-md-9">

                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">

                        <!-- main-navigation start -->
                        <!-- classes: -->
                        <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                        <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                        <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                        <!-- ================ -->
                        <div class="main-navigation  animated with-dropdown-buttons">

                            <!-- navbar start -->
                            <!-- ================ -->
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">

                                    <!-- Toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                                data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>

                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <!-- main-menu -->
                                        <?php echo Nav::widget([
                                            'options' => ['class' => 'nav navbar-nav'],
                                            'items' => [
                                                [
                                                    'label' => Yii::t('common', 'Dental clinics') . " " . Yii::$app->geo->titlePp,
                                                    'url' => ['/company/company/index', 'city' => Yii::$app->geo->slug]
                                                ],
//                                                [
//                                                    'label' => Yii::t('common', 'Doctors') . " " . Yii::$app->geo->titlePp,
//                                                    'url' => ['/dentist/index']
//                                                ],
                                            ]
                                        ]); ?>
                                        <!-- main-menu end -->
                                        <?php /*
                                        <!-- header dropdown buttons -->
                                        <div class="header-dropdown-buttons">
                                            <a href="/page/addclinic" class="btn btn-sm hidden-xs btn-default">Добавить
                                                клинику <i class="fa fa-hospital-o pl-5"></i></a>
                                            <a href="/page/addclinic"
                                               class="btn btn-lg visible-xs btn-block btn-default">Добавить
                                                клинику <i class="fa fa-hospital-o pl-5"></i></a>
                                        </div>
                                        <!-- header dropdown buttons end-->
                                        */ ?>
                                    </div>

                                </div>
                            </nav>
                            <!-- navbar end -->

                        </div>
                        <!-- main-navigation end -->
                    </div>
                    <!-- header-right end -->

                </div>
            </div>
        </div>

    </header>
    <!-- header end -->
</div>
<!-- header-container end -->