<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $theme \yii\web\AssetBundle */

use yii\widgets\Breadcrumbs;
use themes\theproject\assets\TheProjectAsset;

$theme = TheProjectAsset::register($this);


$breadcrumbs = isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : false;


$this->beginContent('@frontend/views/layouts/blank.php')

?>
    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">
        
        <?= $this->render('main-header', ['theme' => $theme]); ?>

        <?php if ($breadcrumbs !== false) { ?>
            <!-- breadcrumbs -->
            <div class="breadcrumb-container">
                <div class="container">
                    <?php echo Breadcrumbs::widget(['links' => $breadcrumbs, 'tag' => 'ol']); ?>
                </div>
            </div>
            <!-- /breadcrumbs -->
        <?php } ?>

        <?php echo $content ?>

        <?= $this->render('main-footer', ['theme' => $theme]); ?>

    </div>
    <!-- page-wrapper end -->


<?php $this->endContent() ?>