<?php
return [
    'name' => 'FindStom',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'defaultRoute' => 'main/default',
    'bootstrap' => [
        'modules\main\Bootstrap',
        'modules\company\Bootstrap',
        'modules\geo\Bootstrap',
        'rabadan731\users\Bootstrap',
    ],
    'sourceLanguage'=>'en-US',
    'language'=>'ru-RU',
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'linkAssets' => true
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d M Y',
            'datetimeFormat' => 'php:d M в H:i',
            'timeFormat' => 'php:H:i:s',
        ],
        'i18n' => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                ],
                '*'=> [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                    'fileMap'=>[
                        'common'=>'common.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'googleApi' => [
            'class'                 => 'common\components\GoogleApiClient',
         //   'developer_key'         => 'AIzaSyDefJmz8OtRbADufgwwja16etzY4CJ5qa8',
            'client_id'             => '498748516865-bk1bj1riefbjigcj99nojeab40blcm0u.apps.googleusercontent.com',
            'client_secret'         => 'UWMDcwrmvOpC6xD23t3JdgKE',
        ],
    ],
    'as locale' => [
        'class' => 'common\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true
    ]
];