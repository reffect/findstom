<?php

namespace common\components;

use yii;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class Query extends yii\db\ActiveQuery{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}