<?php
namespace common\grid;

use Yii;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;

/**
 * @package common\components\grid
 */
class TypeWorkerEnum extends DataColumn
{
    /**
     * @var array List of value => name pairs
     */
    public $enum = [
        3 => 'Cleaner',
        6 => 'Driver',
        7 => 'Master',
        9 => 'Agent',
    ];


    /**
     * @var bool
     */
    public $loadFilterDefaultValues = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        foreach ($this->enum as $k => $i) {
            $this->enum[$k] = Yii::t('common', $i);
        }
        if ($this->loadFilterDefaultValues && $this->filter === null) {
            $this->filter = $this->enum;
        }
    }

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return mixed
     */
    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);
        return ArrayHelper::getValue($this->enum, $value, $value);
    }

    /**
     * @return mixed
     */
    public static function listData()
    {
        $obj = new self();
        return $obj->enum;
    }


    public static function getByKey($key)
    {
        $obj = new self();
        return (isset($obj->enum[$key])) ? $obj->enum[$key] : null;
    }
}
