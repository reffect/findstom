<?php
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 21.01.16
 * Time: 0:43
 */

namespace modules\geo\models\query;


use yii\db\ActiveQuery;

class GeoQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => 1]);
        return $this;
    }

}