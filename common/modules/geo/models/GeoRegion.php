<?php

namespace modules\geo\models;

use modules\geo\models\query\GeoQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geo_region".
 *
 * @property integer $country_id
 * @property integer $id
 * @property string $title
 * @property string $title_rp
 * @property string $title_pp
 * @property string $slug
 * @property integer $status
 *
 * @property GeoCity[] $cities
 */
class GeoRegion extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class'=>SluggableBehavior::className(),
                'slugAttribute'=>'slug',
                'attribute'=>'title',
                'ensureUnique'=>true,
                'immutable' => true
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_region';
    }

    public static function find() {
        return new GeoQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'title'], 'required'],
            [['country_id', 'status'], 'integer'],
            [['title', 'title_rp', 'title_pp', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'Country'),
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Region'),
            'title_rp' => Yii::t('common', 'Region Rp'),
            'title_pp' => Yii::t('common', 'Region Pp'),
            'slug' => Yii::t('common', 'Domain Url'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(GeoCity::className(), ['region_id' => 'id']);
    }


    /**
     * @return boolean Model before Save
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (!strlen($this->title_rp)) {
                $this->title_rp = $this->title;
            }
            if (!strlen($this->title_pp)) {
                $this->title_pp = $this->title;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $value string
     * @param $title string
     *
     * @return mixed
     */
    public static function getListDropdown($value='id',$title='title'){
        $cats=self::find()->select([$value, $title])->all();
        return ArrayHelper::map($cats,$value,$title);
    }

}
