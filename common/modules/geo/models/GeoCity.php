<?php

namespace modules\geo\models;

use common\modules\company\models\DentalCompanyAddress;
use modules\geo\models\query\GeoQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geo_city".
 *
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property string $title_rp
 * @property string $title_pp
 * @property integer $status
 * @property string $district
 * @property string $longitude
 * @property string $latitude
 *
 * @property GeoRegion $region
 */
class GeoCity extends \yii\db\ActiveRecord
{

    public static $country=[
        255=>'Российская федерация',
        187=>'Украина',
        159=>'Казахстан'
    ];


    public function behaviors()
    {
        return [
            [
                'class'=>SluggableBehavior::className(),
                'slugAttribute'=>'slug',
                'attribute'=>'title',
                'ensureUnique'=>true,
                'immutable' => true
            ],
        ];
    }



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'title'], 'required'],
            [['country_id', 'region_id', 'status'], 'integer'],
            [['longitude', 'latitude'], 'number'],
            [['slug', 'title', 'title_rp', 'title_pp', 'district'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'Country ID'),
            'region_id' => Yii::t('common', 'Region ID'),
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Domain Url'),
            'title' => Yii::t('common', 'City'),
            'title_rp' => Yii::t('common', 'City Rp'),
            'title_pp' => Yii::t('common', 'City Pp'),
            'status' => Yii::t('common', 'Status'),
            'district' => Yii::t('common', 'District'),
            'longitude' => Yii::t('common', 'Longitude'),
            'latitude' => Yii::t('common', 'Latitude'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::className(), ['id' => 'region_id']);
    }

    /**
     * @return GeoQuery
     */
    public static function find() {
        return new GeoQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (!strlen($this->title_rp)) {
                $this->title_rp = $this->title;
            }
            if (!strlen($this->title_pp)) {
                $this->title_pp = $this->title;
            }

            $this->country_id = GeoRegion::findOne($this->region_id)->country_id;

            return true;
        } else {
            return false;
        }
    }


    /**
     * @param string $p1
     * @param string $p2
     * @param bool $in
     * @return array
     */
    public static function listDropdown($p1='id',$p2='title',$in=false) {
        $find = self::find()
            ->select([$p1,$p2])
            ->orderBy($p2);

        if ($in) {
            $find->andWhere(['in','id',$in]);
        }

        return ArrayHelper::map($find->asArray()->all(), $p1,$p2);
    }

    /**
     * @param $title
     * @return array|GeoCity|null|\yii\db\ActiveRecord
     */
    public static function createNewOrGetExist($title)
    {
        $model = self::find()->andWhere(['title'=>$title])->one();
        if (is_null($model)) {
            $model = new self();
            $model->title = $title;
            $model->region_id = 1;
            $model->save();
        }
        return $model;
    }


    public static function unionCities($oldCityIDs, $toCityID)
    {
        $findOldCities = GeoCity::find()->andWhere(['id' => $oldCityIDs])->all();
        $toCity = GeoCity::find()->andWhere(['id' => $toCityID])->one();

        if ((count($findOldCities) < 1) || ($toCity === null)) return;

        foreach ($findOldCities as $oldCity) {
            $address = DentalCompanyAddress::find()->andWhere(['city_id' => $oldCity->id])->all();
            foreach ($address as $itemAddress) {
                $itemAddress->locality = $toCity->title;
                $itemAddress->city_id = $toCity->id;
                $itemAddress->save(false, ['locality', 'city_id']);
            }

            if (DentalCompanyAddress::find()->andWhere(['city_id' => $oldCity->id])->count() == 0) {
                $oldCity->delete();
            }
        }
    }

}
