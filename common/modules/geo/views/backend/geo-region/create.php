<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GeoRegion */

$this->title = Yii::t('common', 'Create Geo Region');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Geo Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-region-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
