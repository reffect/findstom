<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\GeoRegion;
/* @var $this yii\web\View */
/* @var $model common\models\GeoCity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-city-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'domain_url')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'region_id')->dropDownList(GeoRegion::getListDropdown()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'city_rp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'city_pp')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
