<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\GeoCitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-city-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'country_id') ?>

    <?= $form->field($model, 'region_id') ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'domain_url') ?>

    <?= $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'city_rp') ?>

    <?php // echo $form->field($model, 'city_pp') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'district') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
