<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Geo Cities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Create Geo City'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'country_id',
            'region_id',
            'id',
            'domain_url:url',
            'city',
            // 'city_rp',
            // 'city_pp',
            // 'status',
            // 'district',
            // 'longitude',
            // 'latitude',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
