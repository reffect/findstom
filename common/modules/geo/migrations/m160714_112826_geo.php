<?php

use yii\db\Migration;

class m160714_112826_geo extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%geo_city}}', [
            'id' => $this->primaryKey(),

            'country_id' => $this->integer(),
            'region_id' => $this->integer(),

            'slug' => $this->string(255),
            'title' => $this->string(255),
            'title_rp' => $this->string(255),
            'title_pp' => $this->string(255),
            'district' => $this->string(255),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'longitude' => $this->float(),
            'latitude' => $this->float(),


        ], $tableOptions);


        $this->createTable('{{%geo_region}}', [
            'id' => $this->primaryKey(),

            'country_id' => $this->integer(),

            'title' => $this->string(255)->notNull(),
            'title_rp' => $this->string(255),
            'title_pp' => $this->string(255),

            'slug' => $this->string(255),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'longitude' => $this->float(),
            'latitude' => $this->float(),

        ], $tableOptions);

        $this->batchInsert('geo_region', ['country_id', 'title', 'title_rp', 'title_pp', 'slug', 'status'], [
            [255, 'Все', 'Все', 'Все', 'all', 1],
        ]);

        $this->batchInsert('geo_city', ['country_id', 'region_id', 'title', 'title_rp', 'title_pp', 'slug', 'status'], [
            [255, 1, 'Москва', 'Москве', 'Москвы', 'moskva', 1],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%geo_city}}');
        $this->dropTable('{{%geo_region}}');
    }


}
