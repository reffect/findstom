<?php

namespace modules\geo\controllers\frontend;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class DefaultController
 * @package modules\geo\controllers\backend
 */
class DefaultController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['setregion']
                    ],
                ],
            ]
        ]);
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionSetregion($city=null)
    {
        if (!is_null($city)) {
            Yii::$app->geo->setDomain($city);
            return $this->redirect(Url::to(["/main/default/index", "city" => $city]));
        }
        return $this->render('setregion');
    }
}