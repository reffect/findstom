<?php

use yii\db\Migration;

class m140703_123106_page_seo extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%seo}}', [
            'id'            => $this->primaryKey(),
            'controller'    => $this->string(512)->notNull(),
            'action'        => $this->string(512)->notNull(),
            'title'         => $this->string(512)->notNull(),

            'seo_title'      => $this->string(512)->notNull(),
            'seo_h1'         => $this->string(512),
            'seo_key'        => $this->text(),
            'seo_desc'       => $this->text(),
            'body'           => $this->text(),
            'footer'         => $this->text()->defaultValue(null),
            'breadcrumb'     => $this->string(512),

            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%seo}}');
    }
}
