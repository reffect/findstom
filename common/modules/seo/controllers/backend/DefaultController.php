<?php

namespace modules\seo\controllers\backend;

use Yii;
use backend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class DefaultController
 * @package modules\seo\controllers\backend
 */
class DefaultController extends Controller
{
//    /**
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['error']
//                    ],
//                ],
//            ]
//        ]);
//    }
 

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderContent('seo INDEX');
    }
}