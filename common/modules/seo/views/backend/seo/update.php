<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\seo\models\Seo */
?>
<div class="seo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
