<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\seo\models\Seo */
?>
<div class="seo-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'controller',
            'action',
            'title',
            'seo_title',
            'seo_h1',
            'seo_key:ntext',
            'seo_desc:ntext',
            'body:ntext',
            'footer:ntext',
            'breadcrumb',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
