<?php

namespace modules\seo;

/**
 * Class Module
 * @package modules\seo
 */
class Module extends \yii\base\Module
{
    /**
     * @var bool
     */
    public $isBackend = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isBackend === true) {
            $this->setViewPath('@common/modules/seo/views/backend');
            $this->setLayoutPath('@backend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                'common\modules\seo\controllers\backend' : $this->controllerNamespace;
        } else {
            $this->setViewPath('@common/modules/seo/views/frontend');
            $this->setLayoutPath('@frontend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                'common\modules\seo\controllers\frontend' : $this->controllerNamespace;
        }

        parent::init();
    }


}
