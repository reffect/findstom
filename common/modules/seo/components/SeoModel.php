<?php
/**
 * @property string $title
 * @property string $title_en
 * @property string $title_ru
 * @property string $seo_h1_ru
 * @property string $h1
 * @property string $seo_h1_en
 * @property string $seo_key_ru
 * @property string $seoKey
 * @property string $seo_key_en
 * @property string $seoDesc
 * @property string $seo_desc_ru
 * @property string $seo_desc_en
 * @property string $body
 * @property string $body_en
 * @property string $body_ru
 */

namespace common\modules\seo\components;

use Yii;
use yii\helpers\Url;


class SeoModel extends \yii\db\ActiveRecord {

    public $urlStart = '';

    /**
     * Link to object
     *
     * @return string
     */
    public function getUrl()
    {
        return Url::to([$this->urlStart,'slug'=>$this->slug]);
    }


    /**
     * The id of the current or next object id
     *
     * @return int
     */
    public function getThisId() {
        if ($this->isNewRecord) {
            return self::find()->max('id')+1;
        } else {
            return $this->id;
        }
    }


    /**
     * Displays a single Page model.
     */
    public function setSeo() {
        Yii::$app->view->h1    = $this->h1;
        Yii::$app->view->title = $this->title;

        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $this->seo_desc]);
        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $this->seo_key]);
    }



} 