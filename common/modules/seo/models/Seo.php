<?php

namespace common\modules\seo\models;

use Yii;
use common\modules\seo\models\query\SeoQuery;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $action
 * @property string $controller
 * @property string $title
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_key
 * @property string $seo_desc
 * @property string $body
 * @property string $breadcrumb
 * @property string $footer
 * @property integer $created_at
 * @property integer $updated_at
 * @property $replaceData []
 */
class Seo extends \yii\db\ActiveRecord
{
    public $imgDir = 'seo';
    public $replaceData = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * The id of the current or next object id
     *
     * @return int
     */
    public function getThisId()
    {
        if ($this->isNewRecord) {
            return self::find()->max('id') + 1;
        } else {
            return $this->id;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller', 'action', 'title', 'seo_title'], 'required'],
            [['seo_key', 'seo_desc', 'body', 'footer'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['controller', 'action', 'title', 'seo_title', 'seo_h1', 'breadcrumb'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'action' => Yii::t('common', 'Action'),
            'controller' => Yii::t('common', 'Controller'),
            'title' => Yii::t('common', 'Title'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),

            'seo_title' => Yii::t('common', 'Title'),
            'footer' => Yii::t('common', 'Footer text'),
            'seo_h1' => Yii::t('common', 'Title H1'),
            'body' => Yii::t('common', 'Body'),
            'seo_key' => Yii::t('common', 'Seo Key'),
            'seo_desc' => Yii::t('common', 'Seo Desc'),
            'breadcrumb' => Yii::t('common', 'Breadcrumb'),
        ];
    }

    /**
     * @inheritdoc
     * @return SeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SeoQuery(get_called_class());
    }

    /**
     * @return boolean Model before Save
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $controller
     * @param $action
     * @param array $replace
     * @param string $default
     * @return string
     */
    public static function br($controller, $action, $replace = [], $default = "")
    {
        $model = self::find()->select('breadcrumb')->ca($controller, $action)->one();

        if (is_null($model)) {
            $model = new Seo();
            $model->controller = $controller;
            $model->action = $action;
            $model->title = $default;
            $model->seo_title = $default;
            $model->body = '';
            $model->seo_h1 = $default;
            $model->seo_desc = $default;
            $model->seo_key = $default;
            $model->breadcrumb = $default;
            $model->save();
        }
        $model->replace($replace);
        return $model->breadcrumb;
    }


    /**
     * Displays a single Page model.
     * @param string $controller
     * @param string $action
     * @param [] $replace
     * @param string $default
     * @return Seo
     */
    public static function parse($controller, $action, $replace = [], $default = "")
    {

        $model = Seo::f($controller, $action, $default);

        $model->replaceData = $replace;
        $model->replace($replace);
        $model->replace([
            'city' => Yii::$app->geo->title,
            'cityPp' => Yii::$app->geo->titlePp,
            'cityRp' => Yii::$app->geo->titleRp,
        ]);

        Yii::$app->view->title = $model->seo_title;
        Yii::$app->params['h1'] = $model->seo_h1;
        Yii::$app->params['footerText'] = $model->footer;
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $model->seo_desc]
        );
        Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $model->seo_key]
        );

        return $model;
    }


    /**
     * @param $replace
     */
    public function replace($replace)
    {
        if (sizeof($replace)) {
            $search = array_map(function ($item) {
                return "{{{$item}}}";
            }, array_keys($replace));

            $this->attributes = array_map(function ($item) use ($search, $replace) {
                return str_replace($search, $replace, $item);
            }, $this->attributes);
        }
    }


    /**
     * @param string $id
     * @param string $title
     * @param null $where
     * @return array
     */
    public static function dropdownList($id = 'id', $title = 'title', $where = null)
    {
        $categories = self::find()->select([$id, $title])->distinct();
        if (!is_null($where)) {
            $categories->andWhere($where);
        }
        return ArrayHelper::map($categories->all(), $id, $title);
    }


    /**
     * @param $controller
     * @param $action
     * @param $default
     * @return Seo
     */
    public static function f($controller, $action, $default)
    {
        /** @var $model Seo */
        $model = self::find()->ca($controller, $action)->one();

        if (is_null($model)) {
            $model = new Seo();
            $model->controller = $controller;
            $model->action = $action;
            $model->title = $default;
            $model->seo_title = $default;
            $model->body = '';
            $model->seo_h1 = $default;
            $model->seo_desc = $default;
            $model->seo_key = $default;
            $model->breadcrumb = $default;
            $model->save();
        }
        return $model;
    }


}
