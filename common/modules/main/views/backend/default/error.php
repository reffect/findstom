<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->params['pageTitle'] = Html::encode($this->title);
$this->params['pageIcon'] = 'warning';
?>
<div class="jarviswidget">
    <div>
        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>
    </div>
</div>
