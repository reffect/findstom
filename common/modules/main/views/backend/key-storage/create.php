<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\main\models\KeyStorageItem */

?>
<div class="key-storage-item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
