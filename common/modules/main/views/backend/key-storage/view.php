<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\KeyStorageItem */
?>
<div class="key-storage-item-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'key',
            'value:ntext',
            'comment:ntext',
            'updated_at',
            'created_at',
        ],
    ]) ?>

</div>
