<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\ArendaMail */
?>
<div class="arenda-mail-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'from',
            'to',
            'subject',
         //   'html:ntext',
            'statusText',
            'data_try_count',
            'data_try_sent:datetime',
            'data_sent:datetime'
        ],
    ]) ?>

    <iframe style="border: 1px solid #000"
            width="100%"
            height="600"
            src="<?=\yii\helpers\Url::to(["/main/arenda-mail/email-html", "id" => $model->id]);?>"
            frameborder="0"
            allowfullscreen>
    </iframe>

</div>
