<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'from',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'to',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'subject',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'html',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'statusText',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'data_try_count',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'data_try_sent',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'data_sent',
         'value' => function ($data) {
             return is_null($data->data_sent)?null:(Yii::$app->formatter->asDatetime($data->data_sent));
         },
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '116px',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip','class'=>'btn btn-default btn-xs'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip','class'=>'btn btn-primary btn-xs'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete','class'=>'btn btn-danger btn-xs',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=> Yii::t("common", "Are you sure?"),
            'data-confirm-message'=> Yii::t("common","Are you sure want to delete this item")],
    ],

];   