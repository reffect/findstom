<?php

use yii\helpers\Html;
use common\modules\eav\models\EavParam;

/* @var $model common\modules\agency\models\Rent */
/* @var $user \modules\users\models\backend\Users */
/* @var $eavParamProvider \yii\data\ActiveDataProvider */


$user = Yii::$app->user->identity;
$eavParamProvider = $model->getEavParamProvider();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this tag, the sky will fall on your head -->
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Hero</title>


</head>

<body bgcolor="#FFFFFF">

<!-- HEADER -->
<table style="width: 100%" background="http://192.241.236.31/themes/preview/smartadmin/1.8.x/Email_Templates/border.png">
    <tr>
        <td></td>
        <td class="header" style="display: block !important;max-width: 600px !important;margin: 0 auto !important;clear: both !important;">

            <div style="padding: 15px;max-width: 600px;margin: 0 auto;">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <h5 style="margin: 0 !important;font-weight: 900;font-size: 17px">
                                ЛОГОТИП
                            </h5>
                        </td>
                        <td align="right">
                            <h6 style="margin: 0 !important;font-weight: 900;font-size: 14px;text-transform: uppercase;color: #444">
                                <?=Yii::$app->name;?>
                            </h6>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table style="width: 100%;">
    <tr>
        <td></td>
        <td style="display: block !important;max-width: 600px !important;margin: 0 auto !important;clear: both !important;" bgcolor="#FFFFFF">

            <div style="padding: 15px;max-width: 600px;margin: 0 auto;">
                <table style="width: 100%">
                    <tr>
                        <td>

                            <h3 style="margin:0;font-weight: 500; font-size: 27px;font-family:'HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;line-height: 1.1;margin-bottom: 15px;">
                                <?=$model->title;?>
                            </h3>
                            <p style="font-size: 17px;text-align: justify;font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;">
                                <?=$model->description;?>
                            </p>

                            <!-- A Real Hero (and a real human being) -->
                            <p>
                                <?php foreach ($model->getImages() as $img) {
                                    echo Html::a(
                                        Html::img("http://{$_SERVER['SERVER_NAME']}/".ltrim($img->getUrl("600"), '/backend')),
                                        "http://{$_SERVER['SERVER_NAME']}/".ltrim($img->getUrl(), '/backend'),
                                        ['target'=>'_blank', 'style' => 'max-width: 100%']
                                    );
                                    echo '<br /><br />';
                                }
                                ?>
                            </p><!-- /hero -->

                            <p style="padding: 15px;background-color: #ecf8ff;margin-bottom: 15px">
                                <?=$model->address_text;?>
                                <br />
                                <br />
                                <?=Html::img("https://maps.googleapis.com/maps/api/staticmap?center={$model->getLat()},{$model->getLng()}&markers=color:red%7Clabel:%7C{$model->getLat()},{$model->getLng()}&zoom=18&size=600x400&key=AIzaSyB46KSNrovdqau_s3LVIc_AmLNN07yHS7E");?>
                            </p>

 
                           <br />
                            <ul style="margin:0;padding:0;background:#ebebeb;display: block;list-style-type: none;font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;">
                                <li style="display: block;  margin: 0;margin-left: 5px;list-style-position: inside">
                                    <a>
                                        <h5 style="color:black;font-weight: 900;font-size: 17px;margin:0 0 15px 0px;padding: 10px 0 0 10px;">
                                            <?= Yii::t("common", "Information about object"); ?>
                                        </h5>
                                    </a>
                                </li>
                                <?php foreach ($eavParamProvider->getModels() as $key => $paramValue) {
                                    $param = $paramValue->param;
                                    ?>
                                     <li style="display: block;  margin: 0;margin-left: 5px;list-style-position: inside">
                                         <a style="<?=($key+1==$eavParamProvider->count)?"":"border-bottom: 1px solid #777;";?>text-decoration: none;color: #666;padding: 10px 16px;margin-right: 10px;cursor: pointer;border-top: 1px solid #fff;display: block;margin: 0">
                                         <?php
                                         echo $param->title;
                                         echo "<span style='float:right;'>";
                                         if ($param->param_type == EavParam::EAV_TYPE_BOOLEAN) {
                                             echo $paramValue->value?Yii::t("common","Yes"):Yii::t("common","No");
                                         } else {
                                             echo "$paramValue->value $paramValue->dimension";
                                         }
                                         echo "</span>";
                                         ?>
                                         </a>
                                     </li>
                                <?php } ?>
                            </ul>

                            <br/>
                            <br/>

                            <!-- social & contact -->
                            <table style="background-color: #ebebeb;width: 100%">
                                <tr>
                                    <td>
                                        <!--- column 1 -->
                                        <table align="left" style="width: 50%;float: left">
                                            <tr>
                                                <td style="padding: 15px">

                                                    <h5 style="margin:0;font-weight: 900;font-size: 17px;font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;">
                                                        Connect with Us:
                                                    </h5>
                                                    <?=$user->name;?>

                                                </td>
                                            </tr>
                                        </table><!-- /column 1 -->

                                        <!--- column 2 -->
                                        <table align="left" style="width: 50%;float: right">
                                            <tr>
                                                <td style="padding: 15px">

                                                    <h5 style="margin:0;font-weight: 900;font-size: 17px;font-family: 'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;">
                                                        <?= Yii::t("common", "Contact"); ?>:
                                                    </h5>
                                                    <p>
                                                        <?= Yii::t("common", "Phone"); ?>: <strong>
                                                            <?=$user->phone;?>
                                                        </strong>
                                                        <br />
                                                        <?= Yii::t("common", "Email"); ?>: <strong>
                                                            <a href="emailto:<?=$user->email;?>">
                                                                <?=$user->email;?>
                                                            </a>
                                                        </strong>
                                                    </p>

                                                </td>
                                            </tr>
                                        </table><!-- /column 2 -->
                                        <span style="display: block;clear: both"></span>
                                    </td>
                                </tr>
                            </table><!-- /social & contact -->
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table style="width: 100%; clear: both !important">
    <tr>
        <td></td>
        <td style="display: block !important;max-width: 600px !important;margin: 0 auto !important;clear: both !important;">

            <!-- content -->
            <div style="padding: 15px;max-width: 600px;margin: 0 auto;text-align: center">
                <?= Yii::t("common", "All Rights Reserved."); ?>
            </div><!-- /content -->

        </td>
        <td></td>
    </tr>
</table><!-- /FOOTER -->

</body>
</html>