<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\ArendaMail */
?>
<div class="arenda-mail-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
