<?php

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $urls [] */
?>

<section class="main-container">
    <div class="container">
<?php
foreach ($urls as $url) {
    if (isset($url['url'])) {
        echo Html::a($url['title'], Url::toRoute($url['url'], true));
    } else {
        echo Html::a(Url::toRoute($url, true), Url::toRoute($url, true));
    }
    echo "<br />";
}
?>
    </div>
</section>
