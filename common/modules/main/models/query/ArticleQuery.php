<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:31 PM
 */

namespace common\models\query;

use common\models\Article;
use yii\db\ActiveQuery;

class ArticleQuery extends ActiveQuery
{
    public function active()
    {
        $this->andWhere(['status' => Article::STATUS_PUBLISHED]);
        return $this;
    }

    public function next()
    {
        $this->andWhere(['>','updated_at',$this->updated_at])->orderBy('updated_at ASC');
        return $this;
    }

    public function prev()
    {
        $this->andWhere(['<','updated_at',$this->updated_at])->orderBy('updated_at DESC');
        return $this;
    }

    public function latest()
    {
        $this->andWhere(['status'=>Article::STATUS_PUBLISHED])->orderBy('updated_at DESC');
        return $this;
    }

    public function slug($slug)
    {
        $this->andWhere(['slug'=>$slug]);
        return $this;
    }

}
