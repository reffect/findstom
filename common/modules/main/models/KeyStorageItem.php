<?php

namespace common\modules\main\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "key_storage_item".
 *
 * @property string $key
 * @property string $value
 * @property string $comment
 */
class KeyStorageItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%key_storage_item}}';
    }

    public function behaviors()
    {
        return [
            [
              'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key'], 'string', 'max'=>128],
            [['value', 'comment'], 'safe'],
            [['key'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('common', 'Key'),
            'value' => Yii::t('common', 'Value'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }

    public static function getValue($key)
    { 
        $model = self::find()->andWhere(['key' => $key])->one();
        if (is_null($model)) {
            $model = new self();
            $model->key = $key;
            $model->value = "-";
            $model->comment = $key;
            $model->save();
        }
        return $model->value;
    }
}
