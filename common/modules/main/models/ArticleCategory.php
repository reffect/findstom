<?php

namespace common\models;

use common\components\SeoModel;
use common\models\query\ArticleCategoryQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property string $slug
 * @property integer $parent_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $title_ru
 * @property string $body_ru
 * @property string $seo_h1_ru
 * @property string $seo_key_ru
 * @property string $seo_desc_ru
 * @property string $title_en
 * @property string $body_en
 * @property string $seo_h1_en
 * @property string $seo_key_en
 * @property string $seo_desc_en
 *
 * @property string $title
 * @property string $body
 * @property string $h1
 * @property string $seoKey
 * @property string $seoDesc
 *
 * @property Article[] $articles
 * @property ArticleCategory $parent
 * @property ArticleCategory[] $articleCategories
 */
class ArticleCategory extends SeoModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 0;

    public $urlStart = 'article/category';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title',
                'ensureUnique'=>true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_category';
    }


    /**
     * @return ArticleCategoryQuery
     */
    public static function find()
    {
        return new ArticleCategoryQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['body_ru', 'seo_key_ru', 'seo_desc_ru', 'body_en', 'seo_key_en', 'seo_desc_en'], 'string'],
            [['title_ru', 'seo_h1_ru', 'title_en', 'seo_h1_en'], 'string', 'max' => 512],
            [['slug'], 'unique'],
            [['slug'], 'string', 'max' => 1024],
            [['parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            ['parent_id', 'exist', 'targetClass' => ArticleCategory::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('common', 'ID'),
            'slug'          => Yii::t('common', 'Slug'),
            'parent_id'     => Yii::t('common', 'Parent Category'),
            'status'        => Yii::t('common', 'Active'),
            'created_at'    => Yii::t('common', 'Created At'),
            'updated_at'    => Yii::t('common', 'Updated At'),

            'title'                 => Yii::t('common', 'Title'),
            'title_ru'              => Yii::t('common', 'Title'),
            'title_en'              => Yii::t('common', 'Title'),
            'h1'                    => Yii::t('common', 'Title H1'),
            'seo_h1_ru'             => Yii::t('common', 'Title H1'),
            'seo_h1_en'             => Yii::t('common', 'Title H1'),
            'body'                  => Yii::t('common', 'Body'),
            'body_ru'               => Yii::t('common', 'Body'),
            'body_en'               => Yii::t('common', 'Body'),
            'seoKey'                => Yii::t('common', 'Seo Key'),
            'seo_key_ru'            => Yii::t('common', 'Seo Key'),
            'seo_key_en'            => Yii::t('common', 'Seo Key'),
            'seoDesc'               => Yii::t('common', 'Seo Desc'),
            'seo_desc_en'           => Yii::t('common', 'Seo Desc'),
            'seo_desc_ru'           => Yii::t('common', 'Seo Desc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasMany(ArticleCategory::className(), ['id' => 'parent_id']);
    }

    public function getChildren()
    {
        return $this->hasMany(ArticleCategory::className(), ['parent_id' => 'id']);
    }



    public function openCategory($categoryModel =null ) {
        if (is_null($categoryModel)) return false;
        return ($categoryModel->id == $this->id);
    }



    public static function dropdownList($id='id',$title='title_ru')
    {
        $categories = self::find()->select([$id,$title])->active()->all();
        return ArrayHelper::map($categories, $id, $title);
    }

    public static function dropdownParentList($id='id',$title='title_ru')
    {
        $categories = self::find()->noParents()->select([$id,$title])->active()->all();
        return ArrayHelper::map($categories, $id, $title);
    }




}
