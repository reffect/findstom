<?php

namespace common\models;

use common\components\SeoModel;
use common\components\users\models\User;
use common\models\query\ArticleQuery;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $slug
 * @property integer $category_id
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $hits
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $title
 * @property string $title_en
 * @property string $title_ru
 * @property string $seo_h1_ru
 * @property string $h1
 * @property string $seo_h1_en
 * @property string $seo_key_ru
 * @property string $seoKey
 * @property string $seo_key_en
 * @property string $seoDesc
 * @property string $seo_desc_ru
 * @property string $seo_desc_en
 * @property string $body
 * @property string $body_en
 * @property string $body_ru
 *
 * @property ArticleCategory $category
 * @property User $author
 * @property User $updater
 * @property ArticleAttachment[] $articleAttachments
 * @property TagArticle[] $tagArticles
 */

class Article extends SeoModel
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    public $urlStart = 'article/view';
    public $imgDir = 'article';

    /**
     * @var array
     */
    public $attachments;

    /**
     * @var array
     */
    public $thumbnail;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }


    public function getThumbUrl($no_host = false) {
        if ($no_host) {
        return (str_replace(Yii::getAlias("@frontendUrl"),"",$this->thumbnail_base_url)."/".($this->thumbnail_path));
        } else {
            return ($this->thumbnail_base_url).DIRECTORY_SEPARATOR.($this->thumbnail_path);
        }
    }

    public function getImg($w=null,$h=null) {
        if (is_null($w) && is_null($h)) {
            return $this->getThumbUrl(true);
        } else {
        return Yii::$app->image->getImage([
                'path'      =>  ($this->getThumbUrl(true)),
            'actions'   =>  ['thumbnail'=>['w'=>$w,'h'=>$h]]
        ]);
       }
    }


    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
             R731Images::deleteItems($this->imgDir,$this->id);
            return true;
         } else {
             return false;
         }
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title_ru',
                'ensureUnique'=>true,
                'immutable' => true
            ], [
            'class' => UploadBehavior::className(),
            'attribute' => 'attachments',
            'multiple' => true,
            'uploadRelation' => 'articleAttachments',
            'pathAttribute' => 'path',
            'baseUrlAttribute' => 'base_url',
            'orderAttribute' => 'order',
            'typeAttribute' => 'type',
            'sizeAttribute' => 'size',
            'nameAttribute' => 'name',
            ], [
            'class' => UploadBehavior::className(),
            'attribute' => 'thumbnail',
            'pathAttribute' => 'thumbnail_path',
            'baseUrlAttribute' => 'thumbnail_base_url'
            ]
        ];
    }




    //Новый показ топика
    public function hits() {
        //если мы в этой сесеии не показвали
        if (is_null(Yii::$app->session->get("hits_articles_".($this->id)))) {
            //инкремируем новый показ в базе
            $this->hits =  ++$this->hits;
            if ($this->save(false,['hits'])) {
                Yii::$app->session->set("hits_articles_".($this->id),$this->hits);
            }
        }
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['slug'], 'unique'],
            [['category_id', 'created_by', 'updated_by', 'hits', 'status', 'created_at', 'updated_at'], 'integer'],
            [['body_ru', 'seo_key_ru', 'seo_desc_ru', 'body_en', 'seo_key_en', 'seo_desc_en'], 'string'],
            [['slug', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['title_ru', 'seo_h1_ru', 'title_en', 'seo_h1_en'], 'string', 'max' => 512],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['attachments', 'thumbnail', 'tags','createdTime'], 'safe'],
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            'thumbnail' => Yii::t('common', 'Thumbnail'),
            'created_by'    => Yii::t('common', 'Author'),
            'updated_by'    => Yii::t('common', 'Updater'),
            'category_id' => Yii::t('common', 'Category'),
            'status' => Yii::t('common', 'Published'),
            'hits' => Yii::t('common', 'Hits'),
            'published_at' => Yii::t('common', 'Published At'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'tags' => Yii::t('common', 'Tags'),


            'title'                 => Yii::t('common', 'Title'),
            'title_ru'              => Yii::t('common', 'Title'),
            'title_en'              => Yii::t('common', 'Title'),
            'h1'                    => Yii::t('common', 'Title H1'),
            'seo_h1_ru'             => Yii::t('common', 'Title H1'),
            'seo_h1_en'             => Yii::t('common', 'Title H1'),
            'body'                  => Yii::t('common', 'Body'),
            'body_ru'               => Yii::t('common', 'Body'),
            'body_en'               => Yii::t('common', 'Body'),
            'seoKey'                => Yii::t('common', 'Seo Key'),
            'seo_key_ru'            => Yii::t('common', 'Seo Key'),
            'seo_key_en'            => Yii::t('common', 'Seo Key'),
            'seoDesc'               => Yii::t('common', 'Seo Desc'),
            'seo_desc_en'           => Yii::t('common', 'Seo Desc'),
            'seo_desc_ru'           => Yii::t('common', 'Seo Desc'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNext()
    {
        $model = self::find()->next()->one();
        if (is_null($model)) {
            $model = self::find()->orderBy('updated_at ASC')->one();
        }
        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrev()
    {
        $model = self::find()->prev()->one();
        if (is_null($model)) {
            $model = self::find()->orderBy('updated_at DESC')->one();
        }
        return $model;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleAttachments()
    {
        return $this->hasMany(ArticleAttachment::className(), ['article_id' => 'id']);
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        TagArticle::deleteAll(['article_id' => $this->id]);
        $values = [];
        if (is_array($this->tags) && count($this->tags)) {
            foreach ($this->tags as $id) {
                $values[] = [$this->id, $id];
            }

            self::getDb()->createCommand()
                ->batchInsert(TagArticle::tableName(), ['article_id', 'tag_id'], $values)->execute();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Список тэгов, закреплённых за постом.
     * @var array
     */
    protected $tags = [];

    /**
     * Устанавлиает тэги поста.
     * @param $tagsId
     */
    public function setTags($tagsId)
    {
        $this->tags = (array) $tagsId;
    }
    /**
     * Возвращает массив идентификаторов тэгов.
     */
    public function getTags()
    {
        return ArrayHelper::getColumn(
            $this->getTagArticle()->all(), 'tag_id'
        );
    }
    /**
     * Возвращает тэги поста.
     * @return ActiveQuery
     */
    public function getTagArticle()
    {
        return $this->hasMany(
            TagArticle::className(), ['article_id' => 'id']
        );
    }

    public function getCreatedTime() {
        return date("Y:m:d H:i:s",$this->isNewRecord?time():$this->created_at);
    }

    public function setCreatedTime($value) {
        $this->created_at = strtotime($value);
    }




    /**
     * @return boolean Model before Save
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (!strlen($this->seo_h1_ru)) {
                $this->seo_h1_ru = $this->title_ru;
            }
            return true;
        } else {
            return false;
        }
    }


    public function setMeta()
    {
        Yii::$app->view->h1    = $this->h1;
        Yii::$app->view->title = $this->title;

        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $this->seoDesc]);
        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $this->seoKey]);
    }


}
