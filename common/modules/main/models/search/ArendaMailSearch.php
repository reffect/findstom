<?php

namespace common\modules\main\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\main\models\ArendaMail;

/**
 * ArendaMailSearch represents the model behind the search form about `common\modules\main\models\ArendaMail`.
 */
class ArendaMailSearch extends ArendaMail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'data_try_count', 'data_try_sent', 'data_sent', 'created_by', 'updated_by'], 'integer'],
            [['from', 'to', 'subject', 'html'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArendaMail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'from',
                'to',
                'subject',
                'data_sent',
                'data_try_count',
                'statusText' => [
                    'asc' => ['status' => SORT_ASC],
                    'desc' => ['status' => SORT_DESC],
                    'label' => Yii::t('common', 'Status')
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'data_try_count' => $this->data_try_count,
            'data_try_sent' => $this->data_try_sent,
            'data_sent' => $this->data_sent,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'from', $this->from])
            ->andFilterWhere(['like', 'to', $this->to])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'html', $this->html]);

        return $dataProvider;
    }
}
