<?php

namespace modules\main\controllers\frontend;

use common\modules\company\models\DentalCompany;
use common\modules\company\models\DentalCompanyAddress;
use modules\geo\models\GeoCity;
use yii\helpers\Url;
use rabadan731\base\components\sitemap\SitemapInterface;
use rabadan731\base\components\sitemap\Sitemap;
use yii\web\Controller;

class SitemapController extends Controller
{
    /**
     * @var int Cache duration, set null to disabled
     */
    protected $cacheDuration = 43200; // default 12 hour 43200

    /**
     * @var string Cache filename
     */
    protected $cacheFilename = 'sitemap.xml';
    protected $cacheFilenameGoogle = 'sitemap_google.xml';

    public function models()
    {
        return [];
    }

    public function getUrls()
    {

        $items[]=[
            'url' => '/',
            'change' => Sitemap::PERIOD_MONTHLY,
            'priority' => 1,
            'title' => \Yii::$app->name
        ];

        $items[]=[
            'url' => '/sitemap',
            'change' => Sitemap::PERIOD_DAILY,
            'priority' => 1,
            'title' => "Карта сайта FindStom"
        ];

        $cityIndex = GeoCity::find()->active()->all();
        if (count($cityIndex)) foreach($cityIndex as $city) {

            /** @var $city GeoCity */

            $items[]=[
                'url' => Url::to(["/main/default/index", "city" => $city->slug]),
                'change' => Sitemap::PERIOD_MONTHLY,
                'priority' => 0.9,
                'title' => "Стоматологии в $city->title_rp"
            ];
        }

//        $pages = Page::find()->select('status,slug,updated_at')->where(['>','status','0'])
//            ->andWhere(['not like','slug','/user/sign-in/login'])
//            ->andWhere(['not like','slug','index-page'])->
//            all();
//        if (count($pages)) foreach($pages as $page) {
//
//            if ($page->status == Page::STATUS_SEO) {
//                $url = $page->slug;
//            } elseif ($page->status == Page::STATUS_PUBLISHED) {
//                $url = Url::toRoute(['page/view','slug'=>$page->slug]);
//            } else continue;
//
//            $items[]=[
//                'url' => $url,
//                'change' => Sitemap::MONTHLY,
//                'priority' => 0.9,
//                'lastmod' => $page->updated_at
//            ];
//        }

//        $companyCategories = CategoryCompany::find()->select('slug,updated_at')->all();
//        if (count($companyCategories)) foreach($companyCategories as $companyCategory) {
//
//            $items[]=[
//                'url' => $companyCategory->url,
//                'change' => Sitemap::MONTHLY,
//                'priority' => 0.8,
//                'lastmod' => $companyCategory->updated_at
//            ];
//        }

        $companies = DentalCompany::find()
            ->select('slug, updated_at, company_id, title')
            ->active()
            ->all();
        if (count($companies)) {

            foreach($companies as $company) {

                foreach($company->addresses as $address) {
                    /** @var $address DentalCompanyAddress */

                    $url = Url::to([
                        '/company/company/view',
                        'slug' => $company->slug,
                        'city' => $address->getCitySlug(),
                        'address' => $address->slug
                    ]);

                    $items[]=[
                        'url' => $url,
                        'change' => Sitemap::PERIOD_MONTHLY,
                        'priority' => 0.8,
                        'lastmod' => $company->updated_at,
                        'title' => "{$company->title} / {$address->getText()}"
                    ];
                }
            }
        }


//        $articles = Article::find()->select('slug,updated_at')->where(['status'=>Article::STATUS_PUBLISHED])->all();
//        if (count($articles)) foreach($articles as $article) {
//
//            $items[]=[
//                'url' => $article->url,
//                'change' => Sitemap::MONTHLY,
//                'priority' => 0.7,
//                'lastmod' => $article->updated_at
//            ];
//        }

        return $items;
    }

    /**
     *
     */
    public function actionGoogle()
    {
        $cachePath = \Yii::$app->runtimePath.DIRECTORY_SEPARATOR.$this->cacheFilenameGoogle;

        if (empty($this->cacheDuration) || !is_file($cachePath) || filemtime($cachePath) < time() - $this->cacheDuration)
        {
            $sitemap = new Sitemap();

            foreach ($this->getUrls() as $item)
            {
                $sitemap->addUrl(
                    isset($item['url']) ? Url::toRoute($item['url'], true) : Url::toRoute($item, true),
                    isset($item['change']) ? $item['change'] : Sitemap::PERIOD_DAILY,
                    isset($item['priority']) ? $item['priority'] : 0.8,
                    isset($item['lastmod']) ? $item['lastmod'] : 0,
                    isset($item['image']) ? $item['image'] : null
                );
            }

            foreach ($this->models() as $model)
            {
                $obj = new $model['class'];
                if ($obj instanceof SitemapInterface) {

                    $sitemap->addModels(
                        $obj::sitemap()->all(),
                        isset($model['change']) ? $model['change'] : Sitemap::PERIOD_DAILY,
                        isset($model['priority']) ? $model['priority'] : 0.8
                    );
                }
            }

            $xml = $sitemap->render();
            file_put_contents($cachePath, $xml);
        }
        else
        {
            $xml = file_get_contents($cachePath);
        }

        header("Content-type: text/xml");
        echo $xml;
        exit();
    }

    /**
     *
     */
    public function actionIndex()
    {
        $cachePath = \Yii::$app->runtimePath.DIRECTORY_SEPARATOR.$this->cacheFilename;

        if (empty($this->cacheDuration) || !is_file($cachePath) || filemtime($cachePath) < time() - $this->cacheDuration)
        {
            $sitemap = new Sitemap();

            foreach ($this->getUrls() as $item)
            {
                $sitemap->addUrl(
                    isset($item['url']) ? Url::toRoute($item['url'], true) : Url::toRoute($item, true),
                    isset($item['change']) ? $item['change'] : Sitemap::PERIOD_DAILY,
                    isset($item['priority']) ? $item['priority'] : 0.8,
                    isset($item['lastmod']) ? $item['lastmod'] : 0
                );
            }

            foreach ($this->models() as $model)
            {
                $obj = new $model['class'];
                if ($obj instanceof SitemapInterface) {

                    $sitemap->addModels(
                        $obj::sitemap()->all(),
                        isset($model['change']) ? $model['change'] : Sitemap::PERIOD_DAILY,
                        isset($model['priority']) ? $model['priority'] : 0.8
                    );
                }
            }

            $xml = $sitemap->render();
            file_put_contents($cachePath, $xml);
        }
        else
        {
            $xml = file_get_contents($cachePath);
        }

        header("Content-type: text/xml");
        echo $xml;
        exit();
    }


    public function actionAllLinks()
    {
        return $this->render('map', [
            'urls' => $this->getUrls()
        ]);
    }
}
