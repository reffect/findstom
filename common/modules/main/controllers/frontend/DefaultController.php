<?php

namespace modules\main\controllers\frontend;

use common\modules\company\models\DentalCompanyAddress;
use common\modules\company\models\search\DentalCompanyAddressSearch;
use common\modules\company\models\search\DentalCompanySearch;
use common\modules\main\models\KeyStorageItem;
use common\modules\seo\models\Seo;
use modules\geo\models\GeoCity;
use Yii;
use frontend\components\Controller;

/**
 * Class DefaultController
 * @package modules\main\controllers\frontend
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex($city='moskva')
    {
        Yii::$app->geo->cheekDomain($city);

        if ($city=='moskva' && Yii::$app->request->url != '/') {
            $this->redirect('/', 301);
        }

        $geo = Yii::$app->geo;

        $cityID = $geo->city_id;

        $cacheKey = "CountClinics_{$cityID}";

        $countClinics = Yii::$app->cache->get($cacheKey);
        if ($countClinics === false) {
            $searchModel = new DentalCompanyAddressSearch();
            $dataProvider = $searchModel->city($geo->city_id);
            $countClinics = $dataProvider->query->count();
            Yii::$app->cache->set($cacheKey, $countClinics, 24 * 60 * 60);
        }

        $seo = Seo::parse("main/{$this->id}", $this->action->id, [], "Стоматологии {{cityPp}}");

        return $this->render('index', [
            'countClinics' => $countClinics,
            'seo' => $seo,
            'geo' => $geo
        ]);
    }

    /**
     * Файл robots.txt
     * @return string
     */
    public function actionRobots()
    {
        $host = Yii::$app->request->hostInfo;
        $host = preg_replace("/w+\./", '', $host);
        $host = str_replace("http://", "", $host);

        header("Content-Type: text/plain");
        $robots = KeyStorageItem::getValue('robots.txt');
        $robots .= "\nHost: $host\n";
        echo $robots;
        exit();
    }

}
