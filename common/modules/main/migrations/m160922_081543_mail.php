<?php

use yii\db\Migration;

class m160922_081543_mail extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci';
        }

        $this->createTable('{{%arenda_mail}}', [
            'id'                => $this->primaryKey(),
            'from'              => $this->string(512)->notNull(),
            'to'                => $this->string(512)->notNull(),
            'subject'           => $this->string(512),
            'html'              => $this->text(),
            'status'            => $this->integer(1)->defaultValue(0),
            'data_try_count'    => $this->integer()->defaultValue(0),
            'data_try_sent'     => $this->integer(),
            'data_sent'         => $this->integer(),
            'created_by'        => $this->integer(),
            'updated_by'        => $this->integer(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%arenda_mail}}');
    }
}
