<?php

namespace modules\main;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\main
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules(
            [
                '<city>/index' => 'main/default/index',
                'index' => 'main/default/index',
                'sitemap' => '/main/sitemap/all-links',
                'robots.txt' => 'main/default/robots',
                'sitemap.xml' => 'main/sitemap/index'
            ],
            false
        );
    }
}
