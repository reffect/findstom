/**
 * Created by rabadan on 05.01.16.
 */


function createMap(divID, setUrl) {

    th = new Object([]);
    th.latitude = $(".latitude"+divID);
    th.longitude = $(".longitude"+divID);
    th.setCoordinatesUrl = setUrl;

    th.latitude.change(function () {
        window.document.companyMap.updatePlacemarkOnInputData(divID);
    });
    th.latitude.keyup(function () {
        window.document.companyMap.updatePlacemarkOnInputData(divID);
    });
    th.longitude.keyup(function () {
        window.document.companyMap.updatePlacemarkOnInputData(divID);
    });
    th.longitude.change(function () {
        window.document.companyMap.updatePlacemarkOnInputData(divID);
    });

    th.myMap = new ymaps.Map("map"+divID, {
        center: [th.latitude.val(), th.longitude.val()],
        zoom: 17,
        controls: ['zoomControl', 'typeSelector']
    });

    th.myPlacemark = new ymaps.Placemark(
        th.myMap.getCenter(),
        {},
        {
            preset: 'islands#redIcon',
            draggable: true
        }
    );

    th.myMap.geoObjects.add(th.myPlacemark);

    //Отслеживаем событие перемещения метки
    th.myPlacemark.events.add("dragend", function (e) {
        coords = this.geometry.getCoordinates();
        newCoords = [coords[0].toFixed(6), coords[1].toFixed(6)];
        if (newCoords !== null) {
            console.log(window.document.companyMap[divID]);
            //window.document.companyMap[divID].myPlacemark.geometry.setCoordinates(newCoords);
            window.document.companyMap[divID].latitude.val(newCoords[0]);
            window.document.companyMap[divID].longitude.val(newCoords[1]);
            window.document.companyMap.updatePlacemarkOnInputData(divID);
        }
    }, th.myPlacemark);

    window.document.companyMap[divID] = th;
}

ymaps.ready(function () {

    window.document.companyMap = new Object([]);

    $('.maps').map(function (i, item) {
        createMap(
            $(item).data('modelId'),
            $(item).data('set-url')
        );
    });

    window.document.companyMap.updatePlacemarkOnInputData = function(addressID) {
        coords = [
            (window.document.companyMap[addressID].latitude.val().length > 1) ?
                window.document.companyMap[addressID].latitude.val() : '55.751574',
            (window.document.companyMap[addressID].longitude.val().length > 1) ?
                window.document.companyMap[addressID].longitude.val() : '37.573856'
        ];
        window.document.companyMap[addressID].myMap.setCenter(coords, 17);
        window.document.companyMap[addressID].myPlacemark.geometry.setCoordinates(coords);

        $.ajax({
            url: window.document.companyMap[addressID].setCoordinatesUrl,
            method: 'post',
            data: {
                'hasEditable' : 1,
                'attribute-primary' : addressID,
                'DentalCompanyAddress[latitude]' :
                    (window.document.companyMap[addressID].latitude.val().length > 1) ?
                window.document.companyMap[addressID].latitude.val() : '0',
                'DentalCompanyAddress[longitude]' :
                    (window.document.companyMap[addressID].longitude.val().length > 1) ?
                window.document.companyMap[addressID].longitude.val() : '0'
            },
            success: function (response) {

            }
        });

    };

    $(".search_geo").on('click', function () {

        addressID = $(this).data('address-id');
        $(this).prop("disable", true);
        $.get($(this).data('loadUrl')).done(function (data) {
            window.document.companyMap[addressID].latitude.val(data[0]);
            window.document.companyMap[addressID].longitude.val(data[1]);
            window.document.companyMap.updatePlacemarkOnInputData(addressID);
            $(this).prop("disable", false);
        });
    });

});