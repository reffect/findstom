/**
 * Created by rabadan on 05.01.16.
 */
/*
function importFirm() {
    var me = $(".progress-bar");
    var prdn_max = me.attr("aria-valuemax");
    $.get("/backend/companies/category-company/imp",{}).done(function (data) {
        me.attr("aria-valuenow",parseInt(data,10));
        perc = (data/prdn_max)*100;
        me.css('width', perc+'%');
        if (data!=0){
            importFirm();
        }
    });

    importFirm();
}
*/
// A $( document ).ready() block.
$(document).ready(function () {

    $('body').on('beforeSubmit', 'form#address_form', function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }
        // submit form
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                $('#address-modal').modal('hide');
                // do something with response
                $('#grid-address').html(response);
                $('.modal-backdrop.fade.in').remove();
                $("body").attr('class','desktop-detected');
            }
        });
        return false;
    });

});