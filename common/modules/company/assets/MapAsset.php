<?php

namespace common\modules\company\assets;

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapAsset;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MapAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/company/assets';

    public $css = [

    ];
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        'js/map.js'
    ];



    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
