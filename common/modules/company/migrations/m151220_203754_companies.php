<?php

use yii\db\Schema;
use yii\db\Migration;

class m151220_203754_companies extends Migration
{

    public $company = "{{%dental_company}}";
    public $companyParse = "{{%dental_company_parser}}";
    public $companySocial = "{{%dental_company_social}}";
    public $companyAddress = "{{%dental_company_address}}";
    public $companyOpeningHours = "{{%dental_opening_hours}}";
    public $companyCategory = "{{%dental_category}}";
    public $companyCompanyCategory = "{{%dental_company_category}}";

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->company, [
            'company_id'        => $this->primaryKey(),
            'slug'              => $this->string(512)->notNull(),

            'title'             => $this->string(512)->notNull(),
            'body'              => $this->text(),
            'prices'            => $this->text(),
            'seo_h1'            => $this->string(512),
            'seo_key'           => $this->text(),
            'seo_desc'          => $this->text(),

            'email'             => $this->string(512),
            'website'           => $this->string(512),

            'logo'              => $this->string(1024),

            'status'            => $this->smallInteger()->defaultValue(1),
            'created_by'        => $this->integer(),
            'updated_by'        => $this->integer(),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ], $tableOptions);

        $this->createTable($this->companySocial, [
            'company_social_id' => $this->primaryKey(),
            'company_id'        => $this->integer()->notNull(),
            'type'              => $this->integer(),
            'url'               => $this->string(1024),
            'status'            => $this->smallInteger()->defaultValue(1),
        ]);

        $this->createTable($this->companyParse, [
            'company_parser_id' => $this->primaryKey(),
            'company_id'        => $this->integer()->notNull(),
            'parsing_url'       => $this->string(1024),
            'parsing_hash'      => $this->string(33),
            'parsing_id'        => $this->integer(),
            'parsing_class'     => $this->string(512),
            'parsing_time'      => $this->integer(),
            'status'            => $this->smallInteger()->defaultValue(1),
        ]);

        $this->createTable($this->companyAddress, [
            'company_address_id'=> $this->primaryKey(),
            'company_id'        => $this->integer()->notNull(),
            'type'              => $this->integer()->defaultValue(1),
            'slug'              => $this->string(512)->notNull(),
            'latitude'          => $this->string(128),
            'longitude'         => $this->string(128),
            'country_name'      => $this->string(512),
            'locality'          => $this->string(512),

            'administrative_area_name'    => $this->string(512),
            'sub_administrative_area_name'=> $this->string(512),
            'dependent_locality_name'     => $this->string(512),
            'premise_number'              => $this->string(128),


            /**
             * [Address] => Россия, Краснодар, микрорайон 9-й километр, проезд Репина, 38
                [AdministrativeAreaName] => Краснодарский край
                [SubAdministrativeAreaName] => городской округ Город Краснодар
                [LocalityName] => Краснодар
                [DependentLocalityName] => микрорайон 9-й километр
                [ThoroughfareName] => проезд Репина
                [PremiseNumber] => 38
             */

            'metro'             => $this->string(256),
            'district'          => $this->string(256),
            'city_id'           => $this->integer(),

            'street_type'       => $this->string(256),
            'street_address'    => $this->string(1024),
            'extended_address'  => $this->string(512),
            'getting_there'     => $this->string(1024),

            'type_build'        => $this->string(1024),
            'name_build'        => $this->string(1024),


            'opening_hours_id'  => $this->integer(),
            'phone'             => $this->string(512),
            'status'            => $this->smallInteger()->defaultValue(1),
            //old

            'region'            => $this->string(512),
            'parsing_hash'      => $this->string(33),
        ], $tableOptions);

        $this->createTable($this->companyOpeningHours, [
            'opening_hours_id'  => $this->primaryKey(),
            'itemprop_datetime' => $this->string(128),
            'public_text'       => $this->text()
        ], $tableOptions);

        $this->createTable($this->companyCompanyCategory, [
            'company_category_id' => $this->primaryKey(),
            'company_id'          => $this->integer()->notNull(),
            'category_id'         => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable($this->companyCategory, [
            'category_id'           => $this->primaryKey(),
            'parent_id'             => $this->integer(),

            'slug'                  => $this->string(512)->notNull(),
            'hits'                  => $this->integer(),
            'status'                => $this->smallInteger()->notNull()->defaultValue(1),

            'title'                 => $this->string(512)->notNull(),
            'body'                  => $this->text(),
            'seo_h1'                => $this->string(512),
            'seo_key'               => $this->text(),
            'seo_desc'              => $this->text(),

            'created_at'            => $this->integer(),
            'updated_at'            => $this->integer(),

        ], $tableOptions);

        $this->createIndex('idx_company_adr',        $this->companyAddress,         'company_id');
        $this->createIndex('idx_ca_opening_hours',   $this->companyAddress,         'opening_hours_id');
        $this->createIndex('idx_company_category',   $this->companyCompanyCategory, 'company_id');
        $this->createIndex('idx_category_company',   $this->companyCompanyCategory, 'category_id');
        $this->createIndex('idx_company_categories', $this->companyCategory,        'parent_id');
        $this->createIndex('idx_company_parser',     $this->companyParse,           'company_id');
        $this->createIndex('idx_company_social',     $this->companySocial,          'company_id');

        $this->addForeignKey('fk_companies_address',  $this->companyAddress,            'company_id',    $this->company,                'company_id', 'cascade', 'cascade');
        $this->addForeignKey('fk_companies_oh',       $this->companyAddress,            'opening_hours_id', $this->companyOpeningHours,    'opening_hours_id', 'cascade', 'cascade');
        $this->addForeignKey('fk_company_category',   $this->companyCompanyCategory,    'company_id',    $this->company,                'company_id', 'cascade', 'cascade');
        $this->addForeignKey('fk_company_categories', $this->companyCompanyCategory,    'category_id',   $this->companyCategory,        'category_id', 'cascade', 'cascade');
        $this->addForeignKey('fk_categories_parent',  $this->companyCategory,           'parent_id',     $this->companyCategory,        'category_id', 'cascade', 'cascade');
        $this->addForeignKey('fk_companies_parser',   $this->companyParse,              'company_id',    $this->company,                'company_id', 'cascade', 'cascade');
        $this->addForeignKey('fk_companies_social',   $this->companySocial,             'company_id',    $this->company,                'company_id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_companies_social',    $this->companySocial);
        $this->dropForeignKey('fk_companies_parser',    $this->companyParse);
        $this->dropForeignKey('fk_companies_address',   $this->companyAddress);
        $this->dropForeignKey('fk_companies_oh',        $this->companyAddress);
        $this->dropForeignKey('fk_company_category',    $this->companyCompanyCategory);
        $this->dropForeignKey('fk_company_categories',  $this->companyCompanyCategory);
        $this->dropForeignKey('fk_categories_parent',   $this->companyCategory);

        $this->dropTable($this->companyCategory);
        $this->dropTable($this->companyCompanyCategory);
        $this->dropTable($this->companyOpeningHours);
        $this->dropTable($this->companyAddress);
        $this->dropTable($this->companyParse);
        $this->dropTable($this->companySocial);
        $this->dropTable($this->company);
    }
}
