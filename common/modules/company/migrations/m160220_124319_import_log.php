<?php

use yii\db\Migration;

class m160220_124319_import_log extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%import_log}}', [
            'id'        => $this->primaryKey(),
            'errors'    => $this->integer()->defaultValue(0),
            'new'       => $this->integer()->defaultValue(0),
            'update'    => $this->integer()->defaultValue(0),
            'body'      => $this->text(),
            'created_at'=> $this->integer()
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%import_log}}');
    }

}
