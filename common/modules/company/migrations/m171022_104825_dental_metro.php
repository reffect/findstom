<?php

use yii\db\Migration;

class m171022_104825_dental_metro extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable("{{%dental_address_metro}}", [
            'address_metro_id'    => $this->primaryKey(),
            'company_address_id'  => $this->integer()->notNull(),
            'metro_id'            => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%dental_address_metro}}");
    }
}
