<?php

use yii\db\Schema;
use yii\db\Migration;

class m151220_203755_companies_2 extends Migration
{

    public $company = "{{%dental_company}}";
    public $companyMetro = "{{%dental_metro}}";
    public $companyAddress = "{{%dental_company_address}}";

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->companyMetro, [
            'metro_id' => $this->primaryKey(),
            'city_id'  => $this->integer()->notNull(),
            'title'    => $this->string(512)->notNull(),
            'status'   => $this->smallInteger()->defaultValue(1),
        ], $tableOptions);


    }

    public function safeDown()
    {


        $this->dropTable($this->companyMetro);
    }
}
