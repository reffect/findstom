<?php

namespace modules\company\controllers\frontend;

use Yii;
use frontend\components\Controller;
use common\modules\company\models\CategoryCompany;
use common\modules\company\models\Company;
use common\modules\company\models\search\CompanySearch;
use common\modules\seo\models\Seo;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package modules\company\controllers\frontend
 */
class DefaultController extends Controller
{

    public function actionIndex($city)
    {
        Yii::$app->geo->cheekDomain($city);

        $seo = $this->getSeo('Стоматологии {{cityPp}}', [
            'cityPp' => Yii::$app->geo->titlePp
        ]);

        $categories = CategoryCompany::find()->parent(1)->orderBy("title")->all();

        return $this->render('index', [
            'categories' => $categories,
            'seo' => $seo
        ]);
    }

    public function actionCategory($category)
    {
        $category = $this->findCategory($category);
        $geo = \Yii::$app->geo;
        $replace = [
            'category' => $category->title
        ];

        $seo = Seo::parse($this->id, $this->action->id, $replace, '{{category}}');

        $searchModel = new CompanySearch();
        $searchModel->category_id = $category->id;
        $searchModel->city_id = $geo->city_id;
        $companyProvider = $searchModel->search([]);


        return $this->render('category', [
            'category' => $category,
            'replace' => $replace,
            'controllerID' => $this->id,
            'seo' => $seo,
            'companyProvider' => $companyProvider
        ]);
    }

    public function actionView($slug)
    {
        $company = $this->findCompany($slug);
        $category = $company->getCategories()->one();

        $replace = [
            'category' => $category->title,
            'title' => $company->title
        ];

        $seo = Seo::parse($this->id, $this->action->id, $replace, '{{title}}');

        return $this->render('view', [
            'slug' => $slug,
            'category' => $category,
            'company' => $company,
            'seo' => $seo,
            'controllerID' => $this->id,
            'replace' => $replace,
        ]);
    }


    /**
     * @param @slug String
     * @return CategoryCompany
     */
    protected function findCategory($slug)
    {
        $model = CategoryCompany::find()->where(['slug' => $slug])->local()->one();
        if ($model !== null) {
            return $model;
        }
        return $this->redirect(["index"]);
    }

    /**
     * @param @slug String
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findCompany($slug)
    {
        $model = Company::find()->where(['slug' => $slug])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getSeo($default = '', $replaceArray = [])
    {
        return Seo::parse("company/{$this->id}", $this->action->id, $replaceArray, $default);
    }


     

}