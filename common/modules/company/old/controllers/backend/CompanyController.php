<?php

namespace modules\company\controllers\backend;

use backend\components\Controller;
use common\modules\company\models\Company;
use common\modules\company\models\CompanyImportForm;
use common\modules\company\models\CategoryCompany;
use common\modules\company\models\CompanyAdr;
use common\modules\company\models\CompanyCategory;
use common\modules\company\models\search\CompanySearch;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use Yandex\Geo\Api;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'import-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }




    public function actionView($id,$address)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if (isset($post['CompanyAdr']['id']) && $post['CompanyAdr']['id'] > 0) {
            if (($modelAdr = CompanyAdr::findOne($post['CompanyAdr']['id'])) === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } else {
            $modelAdr = new CompanyAdr();
        }
        $modelAdr->company_id = $model->id;

        if ($modelAdr->load(Yii::$app->request->post()) && $modelAdr->save()) {
            return $this->refresh();
        }

        $modelAdr->country_name = "Россия";
        $modelAdr->region = "Республика Дагестан";
        $modelAdr->locality = "Махачкала";

        return $this->render('view', [
            'model' => $model,
            'modelAdr' => $modelAdr
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed

    public function actionUpdate($id)
     * {
     * $model = $this->findModel($id);
     *
     * if ($model->load(Yii::$app->request->post()) && $model->save()) {
     * return $this->redirect(['view', 'id' => $model->id]);
     * } else {
     * return $this->render('update', [
     * 'model' => $model,
     * ]);
     * }
     * }
     */
    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $post = Yii::$app->request->post();

        $model = $this->findModel($id);

        if ($model->load($post) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                return "GOOD SAVE";
            }
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionDeleteAddress($model_id)
    {

        if (($model = CompanyAdr::findOne($model_id)) !== null) {
            /** @var $model CompanyAdr */
            $company = $model->company;
            $model->delete();
            return $this->renderAjax('address', ['model' => $company]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionUpdateAddress($id, $company_id = false)
    {

        if ($id == "new") {
            $model = new CompanyAdr();
            $model->company_id = $company_id;
        } else {
            $model = CompanyAdr::findOne($id);
        }

        if ($model !== null) {
            /** @var $model CompanyAdr */

            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    if (Yii::$app->request->isAjax) {
                        return $this->renderAjax('address', ['model' => $model->company]);
                    }
                    return ['success' => false, 'errors' => $model->errors];
                }
            }

            if ($id == "new") {
                $model->id = 'new';
            }
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_form_address', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('_form_address', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionTest()
    {
        if (($model = CompanyAdr::findOne(5324)) !== null) {
            return $this->render('_modal_address', [
                'model' => $model,
            ]);
        }
    }


    public function actionDeleteCategory($company, $category)
    {
        $model = $this->findModel($company);
        $find = CompanyCategory::find()->where([
            'company_id' => $model->id,
            'category_id' => $category
        ])->one();
        if (!is_null($find)) {
            $find->delete();
        }
        return $this->renderAjax('_category_tree', ['model' => $model]);
    }

    public function actionAddCategory($company, $category)
    {
        $model = $this->findModel($company);
        $find = CompanyCategory::find()->where([
            'company_id' => $model->id,
            'category_id' => $category
        ])->one();
        if (is_null($find)) {
            $find = new CompanyCategory();
            $find->company_id = $model->id;
            $find->category_id = $category;
            if ($find->save()) {
                return $this->renderAjax('_category_tree', ['model' => $model]);
            } else {
                return "ERROR SAVE";
            }
        } else {
            return "HAVE BASE";
        }
    }

//
//    public function actionAddService($company, $category)
//    {
//        $model = $this->findModel($company);
//        $find = CompanyPrice::find()->where([
//            'company_id' => $model->id,
//            'services_id' => $category
//        ])->one();
//        if (is_null($find)) {
//            $find = new CompanyPrice();
//            $find->company_id = $model->id;
//            $find->services_id = $category;
//            if ($find->save()) {
//                return $this->renderAjax('_service_tree', ['model' => $model]);
//            } else {
//                return "ERROR SAVE";
//            }
//        } else {
//            return "HAVE BASE";
//        }
//    }
//



    public function actionEditAddress($company, $address)
    {

        $model = $this->findModel($company);
        if (($modelAdr = CompanyAdr::findOne($address)) !== null) {
            return $this->render('edit-address', ['model' => $model, 'modelAdr' => $modelAdr]);
        }

    }


    public function actionCategorySelect()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                foreach (CategoryCompany::listMainCategories($cat_id) as $id => $title) {
                    $out[] = [
                        'id' => $id,
                        'name' => $title
                    ];
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

//    public function actionServiceSelect()
//    {
//        $out = [];
//        if (isset($_POST['depdrop_parents'])) {
//            $parents = $_POST['depdrop_parents'];
//            if ($parents != null) {
//                $cat_id = $parents[0];
//                foreach (CompanyServices::listMainCategories($cat_id) as $id => $title) {
//                    $out[] = [
//                        'id' => $id,
//                        'name' => $title
//                    ];
//                }
//                echo Json::encode(['output' => $out, 'selected' => '']);
//                return;
//            }
//        }
//        echo Json::encode(['output' => '', 'selected' => '']);
//    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist. ID:' . $id);
        }
    }


    public function actionGeo($address)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $api = new Api();
        //$api->setQuery('Россия, Республика Дагестан, Махачкала, Ленина 8 ');
        // Настройка фильтров
        $api->setQuery($address)
            ->setLimit(1)// кол-во результатов
            ->setLang(Api::LANG_RU)// локаль ответа
            ->load();
        $response = $api->getResponse()->getList();
        if (count($response) == 1) {
            return $response[0]->getData();
        }
    }


    public function actionFormContact()
    {
        return $this->renderAjax('form-contact');
    }


    // получение панели фотографий
    public function actionPanelImages($company)
    {
        $model = $this->findModel($company);
        return $this->renderAjax("_images_grid", ['model' => $model]);
    }


//    public function actionParsed()
//    {
//
//        $model = Company::find()->parsed()->orderBy('id')->one();
//        //$model->status = StomEnumColumn::findKey('Publish');
//        $post = Yii::$app->request->post();
//        if ($model->load($post) && $model->save()) {
//            if (isset($post['R731Images'])) R731Images::updateList($post['R731Images']);
//        }
//
//        return $this->render('parsed', [
//            'model' => $model
//        ]);
//
//    }


    /**
     *
     * @return mixed
     */
    public function actionImport()
    {
        //company_import_file

        $model = new CompanyImportForm();
        if (!file_exists(\Yii::getAlias($model->dir))) {
            mkdir(\Yii::getAlias($model->dir));
        }

        if (Yii::$app->request->isPost) {
            $model->csvFile = UploadedFile::getInstance($model, 'csvFile');
            if ($model->upload()) {
                // file is uploaded successfully
                return $this->redirect(['import-company', 'md5' => $model->md5]);
            }
        }

        $oldFiles = FileHelper::findFiles(rtrim(Yii::getAlias($model->dir,'/')));
        $oldFiles=array_map(function($path){return basename($path);},$oldFiles);

        return $this->render('import', ['model' => $model,'oldFiles'=>$oldFiles]);
    }



    public function actionImportCompany($md5)
    {
        $model = new CompanyImportForm();
        $model->md5 = $md5;
        $model->row = 250;
        $file = $model->getAlias();

        if (Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
            return $model->parse(Yii::$app->request->post("position"));
        }

        if (!file_exists($file)) {
            throw new Exception("File not exists");
        }

        return $this->render('import-company', ['file' => $file,'md5'=>$md5,'model'=>$model]);
    }

    public function actionImportDelete($md5)
    {
        $model = new CompanyImportForm();
        $model->md5 = $md5;
        $model->deleteFile();
        return $this->redirect(['import']);
    }




}
