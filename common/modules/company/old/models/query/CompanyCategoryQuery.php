<?php

namespace common\modules\company\models\query;
use common\modules\company\models\CompanyCategory;

/**
 * This is the ActiveQuery class for [[\common\models\CompanyCategory]].
 *
 * @see \common\models\CompanyCategory
 */
class CompanyCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CompanyCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
