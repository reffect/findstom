<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:31 PM
 */

namespace common\modules\company\models\query;

use Yii;
use yii\db\ActiveQuery;

class CompanyQuery extends ActiveQuery
{

    public function active()
    {
        $this->andWhere(['>=','status',1]);
        return $this;
    }

    public function slug($slug)
    {
        $this->andWhere(['slug' => $slug]);
        return $this;
    }

    public function local($city_id=null)
    {
        if(is_null($city_id)) {
            $city_id = Yii::$app->geo->city_id;
        }
        $this->innerJoin('company_adr','company_adr.company_id = companies.id');
        $this->andWhere(['company_adr.city_id'=>$city_id]);
        return $this;
    }
}
