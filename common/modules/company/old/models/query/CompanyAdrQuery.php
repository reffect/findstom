<?php
 
namespace common\modules\company\models\query;


use yii\db\ActiveQuery;

class CompanyAdrQuery extends ActiveQuery
{

    public function active()
    {
        $this->leftJoin('company','company_id = company.id ')->where(['status'=>1]);
        return $this;
    }

}
