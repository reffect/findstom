<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:31 PM
 */

namespace common\modules\company\models\query;

use yii\db\ActiveQuery;

class CategoryCompanyQuery extends ActiveQuery
{

    public function published()
    {
        $this->andWhere(['>=','status',1]);
        return $this;
    }


    public function parent($parent_id)
    {
        $this->andWhere(['categories_company.parent_id'=>$parent_id]);
        return $this;
    }

    /** Default sort */
    public function ds()
    {
        $this->orderBy("title");
        return $this;
    }

    public function slug($slug)
    {
        $this->andWhere(['slug' => $slug]);
        return $this;
    }

    public function local()
    {
        $this->innerJoin('company_category','company_category.category_id = categories_company.id');
        $this->innerJoin('company_adr','company_adr.company_id = company_category.company_id');
        $this->andWhere(['company_adr.city_id'=>\Yii::$app->geo->city_id]);
        return $this;
    }
}
