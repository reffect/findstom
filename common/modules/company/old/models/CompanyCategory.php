<?php

namespace common\modules\company\models;

use common\modules\company\models\query\CompanyCategoryQuery;
use Yii;
use yii\rbac\Item;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "company_category".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $category_id
 *
 * @property CategoryCompany $category
 * @property Company $company
 */
class CompanyCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'category_id'], 'required'],
            [['company_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'company_id' => Yii::t('common', 'Company ID'),
            'category_id' => Yii::t('common', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CategoryCompany::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public static function import($companyId, $categoryId)
    {
        $model = self::find()->where(['company_id'=>$companyId, 'category_id'=>$categoryId])->one();
        if (is_null($model)) {
            $model = new CompanyCategory();
            $model->company_id = $companyId;
            $model->category_id = $categoryId;
            if (!$model->save()) {
                throw new NotFoundHttpException('error import: ' . print_r($model->errors, 1));
            }
        }
        return $model->id;
    }


    /**
     * @inheritdoc
     * @return CompanyCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyCategoryQuery(get_called_class());
    }




}
