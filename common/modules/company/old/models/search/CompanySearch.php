<?php

namespace common\modules\company\models\search;

use common\modules\company\models\CompanyCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\company\models\Company;

/**
 * CompanySearch represents the model behind the search form about `common\models\Company`.
 */
class CompanySearch extends Company
{
    public $category_id=null;
    public $city_id=null;

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hits', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['slug', 'title', 'body', 'email', 'website', 'seo_h1', 'seo_key', 'seo_desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        if (!is_null($this->category_id)) {
            $query->innerJoin(CompanyCategory::tableName(),Company::tableName().".id = ".CompanyCategory::tableName().".company_id");
            $query->andWhere([CompanyCategory::tableName().".category_id"=>$this->category_id]);
        }

        if (!is_null($this->city_id)) {
            $query->local($this->city_id);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'hits' => $this->hits,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'seo_h1', $this->seo_h1])
            ->andFilterWhere(['like', 'seo_key', $this->seo_key])
            ->andFilterWhere(['like', 'seo_desc', $this->seo_desc]);

        return $dataProvider;
    }
}
