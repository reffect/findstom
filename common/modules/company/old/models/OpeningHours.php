<?php

namespace common\modules\company\models;

use Yii;

/**
 * This is the model class for table "opening_hours".
 *
 * @property integer $id
 * @property string $itemprop_datetime
 * @property string $public_text
 * @property string $parsing_text
 *
 * @property CompanyAdr[] $companyAdrs
 */
class OpeningHours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opening_hours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itemprop_datetime', 'public_text','parsing_text'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'itemprop_datetime' => Yii::t('common', 'Itemprop Datetime'),
            'public_text' => Yii::t('common', 'Public Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAdrs()
    {
        return $this->hasMany(CompanyAdr::className(), ['opening_hours' => 'id']);
    }

    public static function parsLoad($text) {
        $base = self::find()->select('id')->where(['parsing_text'=>$text])->one();
        if (is_null($base)) {
            $base = new OpeningHours();
            $base->itemprop_datetime = $base->public_text = $base->parsing_text = $text;
            $base->save();
        }
        return $base->id;
    }

    public static function listOptions(){
        $result = [];

        foreach (self::find()->all() as $item) {
            $result[$item->id] = $item->public_text;
        }

        return $result;
    }
}
