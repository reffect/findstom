<?php
 
namespace common\modules\company\models;

use yii\base\Exception;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class CompanyImportForm extends Model
{
    /**
     * @var UploadedFile
     */


    public $delimiter = ';';
    public $enclosure = '"';
    public $escape = '\\';
    public $row = 500;


    public $csvFile;
    public $md5;
    public $dir = "@runtime/company_import_file/";

    public function rules()
    {
        return [
            [['csvFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'csvFile' => "Выбрать файл",
        ];
    }



    public function parseTest($lines=10) {
        $row = 1;
        $result = [];
        $encoding='utf-8';
        //Если файл существует
        if (($handle = fopen(\Yii::getAlias($this->dir) . $this->md5, 'r')) !== FALSE) {

            $keys = null;
            if (($data = fgetcsv($handle, 0, $this->delimiter,$this->enclosure,$this->escape)) !== FALSE) {
                $encoding = $this->detect_encoding(implode($data,' '));

                if ($encoding!='utf-8') {
                    $keys = array_map(function($item) use ($encoding) {
                        return iconv($encoding,"UTF-8",$item);
                    },$data);
                }
            }


            //обходим
            while (($data = fgetcsv($handle, 0, $this->delimiter,$this->enclosure,$this->escape)) !== FALSE) {



                if ($row==1) {
                    $encoding = $this->detect_encoding(implode($data,' '));
                }
                $data = array_map(function($item) use ($encoding) {
                    if ($encoding=='utf-8') {
                        return $item;
                    } else {
                        return iconv($encoding,"UTF-8",$item);
                    }
                },$data);

             //   $data[7] = explode(',',trim($data[7]));
                $data[11] = trim(trim($data[11],'тел:'));
//                $result[] = $this->parsingData($data);
                if ($keys !==null) {
                    foreach($data as $key=>$val)  {
                        $data[$key.":".$keys[$key]] = $val;
                        unset($data[$key]);
                    }
                }


                $result[] = $data;

                $row++;

                if ($row==10) {
                    fclose($handle);
                    return $result;
                }
            }
            fclose($handle);
            return $result;
        }

        return "ERROR_LOAD";
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->md5 = md5($this->csvFile->baseName.time()) . '.' . $this->csvFile->extension;
            if (FileHelper::createDirectory(\Yii::getAlias($this->dir))) {
                $this->csvFile->saveAs($this->getAlias());
            } else {
                throw new Exception("ERROR CREATE FOLDER");
            }
            return true;
        } else {
            return false;
        }
    }



    public function parse($pos=0) {

        $row = 1;
        $encoding='utf-8';
        //Если файл существует
        if (($handle = fopen(\Yii::getAlias($this->dir) . $this->md5, 'r')) !== FALSE) {

            //если задана позиция, начинаем с нее. Иначе с нуля
            if ($pos>0) {
                fseek($handle,$pos);
            }

            //обходим
            while (($data = fgetcsv($handle, 0, $this->delimiter,$this->enclosure,$this->escape)) !== FALSE) {

                if ($row==1) {
                    $encoding = $this->detect_encoding(implode($data,' '));
                    if ($pos==0) {
                        $row++;
                        continue;
                    }
                }

                if ($encoding!='utf-8') {
                    $data = array_map(function($item) use ($encoding) {
                        return iconv($encoding,"UTF-8",$item);
                    },$data);
                }

                Company::import($this->parsingData($data));

                $row++;

                if ($row==$this->row) {
                    $endRow = ftell($handle);
                    fclose($handle);
                    //if ($pos>28000) return tr($data,0);
                    return ("POS_".$endRow);
                }
            }
            fclose($handle);
            return "END";
        }

        return "ERROR_LOAD";

    }


    public static function detect_encoding($string, $pattern_size = 50)
    {
        $list = array('cp1251', 'utf-8', 'ascii', '855', 'KOI8R', 'ISO-IR-111', 'CP866', 'KOI8U');
        $c = strlen($string);
        if ($c > $pattern_size)
        {
            $string = substr($string, floor(($c - $pattern_size) /2), $pattern_size);
            $c = $pattern_size;
        }

        $reg1 = '/(\xE0|\xE5|\xE8|\xEE|\xF3|\xFB|\xFD|\xFE|\xFF)/i';
        $reg2 = '/(\xE1|\xE2|\xE3|\xE4|\xE6|\xE7|\xE9|\xEA|\xEB|\xEC|\xED|\xEF|\xF0|\xF1|\xF2|\xF4|\xF5|\xF6|\xF7|\xF8|\xF9|\xFA|\xFC)/i';

        $mk = 10000;
        $enc = 'ascii';
        foreach ($list as $item)
        {
            $sample1 = @iconv($item, 'cp1251', $string);
            $gl = @preg_match_all($reg1, $sample1, $arr);
            $sl = @preg_match_all($reg2, $sample1, $arr);
            if (!$gl || !$sl) continue;
            $k = abs(3 - ($sl / $gl));
            $k += $c - $gl - $sl;
            if ($k < $mk)
            {
                $enc = $item;
                $mk = $k;
            }
        }
        return $enc;
    }


    private function parsingData($data)
    {

        $result = [
            'company' => [
                'title'         => $data[5],
                'body'          => $data[6],
                'email'         => $data[12],
                'website'       => $data[13],
                'skype'         => $data[15],
                'facebook'      => $data[16],
                'instagram'     => $data[17],
                'twitter'       => $data[18],
                'vkontakte'     => $data[19],
            ],
            'address' => [
                'country_name'      => $data[3],
                'locality'          => $data[4],
                'street_address'    => $data[9],
                'tel'               => $data[11],
            ],
            'categoryParent' => $data[1],
            'categoryChildren' => $data[2],
            'category' => $data[7],
        ];

        return $result;

    }


    public function getAlias()
    {
        if (!is_dir(\Yii::getAlias($this->dir))) {
            mkdir(rtrim("".\Yii::getAlias($this->dir),"/"));
        }
        return \Yii::getAlias($this->dir.$this->md5);
    }

    public function deleteFile()
    {
        if (is_file($this->getAlias())) {
            unlink($this->getAlias());
        }
    }


}