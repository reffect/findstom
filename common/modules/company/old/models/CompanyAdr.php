<?php

namespace common\modules\company\models;

use Yii;
use common\modules\company\models\query\CompanyAdrQuery;
use modules\geo\models\GeoCity;
use modules\geo\models\GeoRegion;
use yii\base\Exception;

/**
 * This is the model class for table "company_adr".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $type
 * @property string $country_name
 * @property string $region
 * @property string $locality
 * @property string $district
 * @property string $metro
 * @property string $street_address
 * @property string $extended_address
 * @property string $latitude
 * @property string $longitude
 * @property integer $opening_hours
 * @property integer $city_id
 * @property string $phone
 * @property string $parsing_hash
 * @property string $getting_there
 * @property string $fullAddress
 * @property string $address
 *
 * @property OpeningHours $openingHours
 * @property Company $company
 */
class CompanyAdr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_adr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'city_id', 'type', 'opening_hours'], 'integer'],
            [[
                'country_name',
                'parsing_hash',
                'region',
                'locality',
                'extended_address',
                'phone'
            ], 'string', 'max' => 512],
            [['district', 'metro'], 'string', 'max' => 256],
            [['street_address', 'getting_there'], 'string', 'max' => 1024],
            [['latitude', 'longitude'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'company_id' => Yii::t('common', 'Company'),
            'type' => Yii::t('common', 'Type'),
            'country_name' => Yii::t('common', 'Country'),
            'region' => Yii::t('common', 'Region'),
            'locality' => Yii::t('common', 'Locality'),
            'city_id' => Yii::t('common', 'City'),
            'district' => Yii::t('common', 'District'),
            'metro' => Yii::t('common', 'Metro'),
            'street_address' => Yii::t('common', 'Street'),
            'extended_address' => Yii::t('common', 'Extended Address'),
            'latitude' => Yii::t('common', 'Latitude'),
            'longitude' => Yii::t('common', 'Longitude'),
            'opening_hours' => Yii::t('common', 'Opening Hours'),
            'phone' => Yii::t('common', 'Phone'),
            'getting_there' => Yii::t('common', 'Getting There'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpeningHours()
    {
        return $this->hasOne(OpeningHours::className(), ['id' => 'opening_hours']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOthersAddress()
    {
        return self::find()->where(['company_id' => $this->company_id])->andWhere(['!=', 'id', $this->id]);
    }


    /**
     * @return String
     */
    public function getFullAddress()
    {
        $return = '';

        if (strlen($this->country_name)) {
            $return .= $this->country_name;
        }
        if (strlen($this->region)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->region;
        }
        if (strlen($this->locality)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->locality;
        }
        if (strlen($this->street_address)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->street_address;
        }
        if (strlen($this->extended_address)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->extended_address;
        }
        return $return;
    }

    public function getAddress()
    {
        $return = '';
        if (strlen($this->locality)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->locality;
        }
        if (strlen($this->street_address)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->street_address;
        }
        if (strlen($this->extended_address)) {
            $return .= ((strlen($return) > 1) ? ", " : "") . $this->extended_address;
        }
        return $return;
    }

    /**
     * @return CompanyAdrQuery
     */
    public static function find()
    {
        return new CompanyAdrQuery(get_called_class());
    }

    public static function import($companyId, $attributes)
    {
        $parsingHash = md5("{$companyId}" . serialize($attributes));
        $model = self::find()->select('id')->where(['parsing_hash' => $parsingHash])->one();
        if (is_null($model)) {
            $model = new CompanyAdr();
            $model->load($attributes, '');
            $model->city_id = $model->findCity();
            $model->parsing_hash = $parsingHash;
            $model->company_id = $companyId;
            if (!$model->save()) {
                throw new Exception("ERROR SAVE ADDRESS: " . print_r($model->errors, 1));
            }
        }
        return $model->id;
    }

    private function findCity()
    {
        $city = GeoCity::find()->select('id')->where(['title' => $this->locality])->one();
        if (is_null($city)) {
            $city = new GeoCity();
            $city->title = $this->locality;

            $region = GeoRegion::find()->where(['title' => "Others"])->one();
            if (is_null($region)) {
                $region = new GeoRegion();
                $region->title = "Others";
                $region->country_id = 255;
                $region->save();
            }
            $city->region_id = $region->id;
            $city->save();
        }

        return $city->id;
    }
}
