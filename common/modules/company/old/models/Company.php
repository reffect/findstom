<?php

namespace common\modules\company\models;

use Yii;
use common\modules\seo\components\SeoModel;
use common\modules\company\models\query\CompanyQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "companies".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property string $body
 * @property string $seo_h1
 * @property string $seo_key
 * @property string $seo_desc
 * @property integer $hits
 * @property string $email
 * @property string $website
 * @property string $skype
 * @property string $instagram
 * @property string $twitter
 * @property string $facebook
 * @property string $vkontakte
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property integer $status
 * @property string $parsing_hash
 * @property string $parsing_url
 * @property integer $parsing_id
 * @property string $parsing_class
 * @property integer $parsing_time
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CompanyAdr[] $companyAdrs
 * @property CompanyCategory[] $companyCategories
 * @property CompanyCategory[] $notCategories
 */
class Company extends SeoModel
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['slug'], 'unique'],
            [['body', 'seo_key', 'seo_desc'], 'string'],
            [[
                'hits',
                'status',
                'parsing_id',
                'parsing_time',
                'created_by',
                'updated_by',
                'created_at',
                'updated_at'
            ], 'integer'],
            [[
                'slug',
                'title',
                'seo_h1',
                'email',
                'website',
                'skype',
                'instagram',
                'twitter',
                'facebook',
                'vkontakte',
                'thumbnail_base_url',
                'thumbnail_path',
                'parsing_class'
            ], 'string', 'max' => 512],
            [['parsing_url'], 'string', 'max' => 1024],
  //          [['thumbnail'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            
            'email' => Yii::t('common', 'Email'),
            'website' => Yii::t('common', 'Website'),
            
            'skype' => Yii::t('common', 'Skype'),
            'instagram' => Yii::t('common', 'Instagram'),
            'twitter' => Yii::t('common', 'Twitter'),
            'facebook' => Yii::t('common', 'Facebook'),
            'vkontakte' => Yii::t('common', 'Vkontakte'),
            
            'thumbnail_base_url' => Yii::t('common', 'Thumbnail Base Url'),
            'thumbnail_path' => Yii::t('common', 'Thumbnail Path'),
            'status' => Yii::t('common', 'Status'),
            
            'parsing_url' => Yii::t('common', 'Parsing Url'),
            'parsing_id' => Yii::t('common', 'Parsing ID'),
            'parsing_class' => Yii::t('common', 'Parsing Class'),
            'parsing_time' => Yii::t('common', 'Parsing Time'),
            
            'created_by' => Yii::t('common', 'Created By'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),

            'title' => Yii::t('common', 'Title'),
            'h1' => Yii::t('common', 'Title H1'),
            'body' => Yii::t('common', 'Body'),
            'seo_key' => Yii::t('common', 'Seo Key'),
            'seo_desc' => Yii::t('common', 'Seo Desc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasMany(CompanyAdr::className(), ['company_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCategories()
    {
        return $this->hasMany(CompanyCategory::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(CategoryCompany::className(), ['id' => 'category_id'])
            ->viaTable('company_category', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotCategories()
    {
        return $this->hasMany(CategoryCompany::className(), ['id' => 'category_id'])
            ->viaTable('company_category', ['company_id' => 'id']);
    }

    /**
     * Find all category this Company
     *
     * @return mixed
     */
    public function getListCategories()
    {
        $result = [];
        /** @var CategoryCompany $model */
        foreach ($this->getNotCategories()->orderBy('parent_id')->all() as $model) {
            $result[$model->parent->title][$model->id] = $model->title;
        }
        return $result;
    }
    
    /**
     * @return CompanyQuery
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public static function import($data)
    {
        $importHash = md5(serialize($data['company']));

        $model = self::find()->select('id')->where(['parsing_hash' => $importHash])->one();

        if (is_null($model)) {
            $model = new Company();
            $model->load($data, 'company');
            $model->parsing_hash = $importHash;
            $model->parsing_time = time();
            $model->parsing_class = CompanyImportForm::className();
            if (!$model->save()) {
                throw new NotFoundHttpException('error import: ' . print_r($model->errors, 1));
            }
        }

        $parentCategory = CategoryCompany::import($data['categoryParent'], ' ', 1);
        $childrenCategory = CategoryCompany::import($data['categoryChildren'], ' ', $parentCategory);
        CompanyCategory::import($model->id, $childrenCategory);

        //Добавить адрес к компаниям
        CompanyAdr::import($model->id, $data['address']);

        return 1;
    }

    /**
     * Link to object
     *
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['company/default/view', 'slug' => $this->slug]);
    }
}
