<?php

namespace common\modules\company\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use common\modules\company\models\query\CategoryCompanyQuery;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "categories_company".
 *
 * @property integer $id
 * @property string $slug
 * @property integer $hits
 * @property integer $status
 * @property integer $parent_id
 * @property string $title
 * @property string $body
 * @property string $seo_h1
 * @property string $seo_key
 * @property string $seo_desc
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CategoryCompany $parent
 * @property CategoryCompany $children
 * @property CategoryCompany[] $categoryCompanies
 * @property CompanyCategory[] $companyCategories
 */
class CategoryCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_company';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['slug'], 'unique'],
            [['hits', 'status', 'parent_id', 'created_at', 'updated_at'], 'integer'],
            [['body', 'seo_key', 'seo_desc'], 'string'],
            [['slug', 'title', 'seo_h1'], 'string', 'max' => 512],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryCompany::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            'hits' => Yii::t('common', 'Hits'),
            'status' => Yii::t('common', 'Status'),
            'parent_id' => Yii::t('common', 'Parent ID'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'title' => Yii::t('common', 'Title'),
            'h1' => Yii::t('common', 'Title H1'),
            'body' => Yii::t('common', 'Body'),
            'seo_key' => Yii::t('common', 'Seo Key'),
            'seo_desc' => Yii::t('common', 'Seo Desc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasOne(self::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryCompanies()
    {
        return $this->hasMany(CategoryCompany::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCategories()
    {
        return $this->hasMany(CompanyCategory::className(), ['category_id' => 'id']);
    }

    public static function import($title, $body = null, $parent_id = null)
    {
        $model = self::find()->select('id')->where(['title' => $title, 'parent_id' => $parent_id])->one();
        if (is_null($model)) {
            $model = new CategoryCompany();
            $model->title = $title;
            $model->body = $body;
            if ($parent_id !== null) {
                $model->parent_id = $parent_id;
            }
            if (!$model->save()) {
                throw new NotFoundHttpException('error import: ' . print_r($model->errors, 1));
            } 
        }
        return $model->id;
    }


    public static function listMainCategories($parentId = 1)
    {

        $list = [];
        /* @var $item self */
        foreach (self::find()->select('id,title')->parent($parentId)->all() as $item) {
            $list[$item->id] = $item->title;
        }

        return $list;
    }

    public static function getListMenu($parent)
    {
        /* @var $item self */
        $list_mark = [];
        foreach (self::find()->orderBy("title")->parent($parent)->select('id,title,slug')->all() as $item) {
            $list_mark[] = [
                'label' => $item->title,
                'url' => ['/company/index', 'category' => $item->slug],
                'options' => [
                    'class' => 'list-group-item'
                ],
                'items' => $parent == 1 ? self::getListMenu($item->id) : []
            ];
        }
        return $list_mark;
    }


    /**
     * @inheritdoc
     * @return CategoryCompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryCompanyQuery(get_called_class());
    }


}
