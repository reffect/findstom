<?php
use common\models\Seo;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $seo Seo */
/* @var $category \common\models\CategoryCompany */
/* @var $company \common\models\Company */
/* @var $address \common\models\CompanyAdr */
/* @var $replace String */
/* @var $controllerID string */

$this->params['breadcrumbs'][] = ['url'=>['/company/index'],'label'=>Seo::br($controllerID,'index',$seo->replaceData,'Автообъявления')];
$this->params['breadcrumbs'][] = ['url'=>['/company/category','category'=>$category->slug],'label'=>Seo::br($controllerID,'category',$seo->replaceData,"{{category}}")];
$this->params['breadcrumbs'][] = $seo->breadcrumb;

?>
<h1><?=$seo->h1;?></h1>
<p><?=$seo->body;?></p>
<div class="row">
    <div class="col-md-6">
        <h4>Контакты</h4>
        <?= DetailView::widget([
            'model' => $company,
            'attributes' => [
                'email:email',
                'website:url',
                'skype',
            ],
        ]); ?>
        <h4>Информация: </h4>
        <p><?=$company->body;?></p>

    </div>
    <div class="col-md-6">


        <h4>Адреса</h4>
        <?php foreach($company->getCompanyAdrs()->all() as $address) { ?>
            <?= DetailView::widget([
                'model' => $address,
                'attributes' => [
                    'tel',
                    'fulladdress'
                ],
            ]); ?>
        <?php } ?>

        <h4>Социальные сети</h4>
        <?= DetailView::widget([
            'model' => $company,
            'attributes' => [
                'instagram:url',
                'twitter:url',
                'facebook:url',
                'vkontakte:url',
            ],
        ]); ?>

    </div>
</div>
