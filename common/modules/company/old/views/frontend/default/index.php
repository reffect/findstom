<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $seo common\modules\seo\models\Seo */
/* @var $categories [] */
/* @var $category common\modules\company\models\CategoryCompany */
/* @var $parentCategory common\modules\company\models\CategoryCompany */
/* @var $categoryProvider \yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = $seo->breadcrumb;
?>

<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-md-12">
                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title">
                    <?= $seo->seo_h1; ?>
                </h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
                <p class="lead">
                    <?= $seo->body; ?>
                </p>
                
            </div>
        </div>
    </div>
</section>


<div class="row">
    <div class="col-md-12 categoryBlock">
        <?php foreach ($categories as $parentCategory) { ?>
            <div class="panel panel-default">
                <div class="panel-heading"><b><?= $parentCategory->title; ?></div>
                <div class="panel-body">
                    <?php foreach ($parentCategory->getChildren()->local()->all() as $category) {
                        echo Html::a($category->title, ['company/default/category', 'category' => $category->slug], ['class' => 'btn btn-default']);
                    } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


