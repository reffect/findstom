<?php
use common\models\Seo;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $seo Seo */
/* @var $replace String */
/* @var $controllerID string */
/* @var $companyProvider \yii\data\ActiveDataProvider */



$this->params['breadcrumbs'][] = ['url'=>['index'],'label'=>Seo::br($controllerID,'index',$seo->replaceData,'Авто компании')];
$this->params['breadcrumbs'][] = $seo->breadcrumb;
?>
<h1><?=$seo->h1;?></h1>
<p><?=$seo->body;?></p>


<div class="row">
    <?php Pjax::begin(['options' => ['id'=>"company_list", 'timeout'=>5000]]); ?>
        <?php echo ListView::widget([
            'dataProvider' => $companyProvider,
            'itemView' => '_list_item'
        ]); ?>
    <?php Pjax::end(); ?>
</div>