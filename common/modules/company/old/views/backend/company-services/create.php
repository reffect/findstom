<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServices */

$this->title = Yii::t('common', 'Create Company Services');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Company Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
