<?php
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 06.01.16
 * Time: 14:17
 */

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $modelAdr common\models\CompanyAdr */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;


?>


<div class="row">
    <div class="col-md-12" id="grid-address">
        <?php echo GridView::widget([
            'id'=>'grid-view-address',
            'dataProvider' => new ActiveDataProvider(['query' => $model->getCompanyAdrs()]),
            'columns' => [
                [
                    'label' => '',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return ($data->address) . "<br /><b>" . ($data->tel) . "</b><br />";//. ($data->openingHours->public_text);
                    },
                ],
                [
                    'label' => '',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return
                            \yii\bootstrap\Modal::widget([
                                'id' => 'address-modal',
                                'header' => 'Редактирование адреса',
                                'clientOptions' => false,
                                'options'=>[
                                    'data-backdrop'=>'static',
                                ],
                                'toggleButton' => [
                                    'label' => '<i class="fa fa-pencil"></i>',
                                    'tag' => 'a',
                                    'data-target' => '#address-modal',
                                    'class' => 'btn btn-primary btn-xs',
                                    'href' => Url::to(['update-address', 'id' => $data->id]), // Ajax view with form to load
                                ],
                            ]) . "<br /><br />" . Html::button(
                                '<i class="fa fa-times"></i>',
                                [
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-delete-id' => $data->id,
                                    'onclick' => "
                                    if (confirm('подтверждаете удаление?')) {
                                        $.get('" . (Url::to(['delete-address'])) . "', {
                                            model_id: $(this).attr('data-delete-id')
                                        }).done(function( data ) {
                                            $('#grid-address').html(data);
                                        });
                                    }
                                "
                                ]
                            );
                    },
                ],
            ]
        ]); ?>

        <?=\yii\bootstrap\Modal::widget([
            'id' => 'address-modal',
            'header' => 'Новый адрес',
            'clientOptions' => false,
            'options'=>[
                'data-backdrop'=>'static',
            ],
            'toggleButton' => [
                'label' => 'Добавить новый адрес',
                'tag' => 'a',
                'data-target' => '#address-modal',
                'class' => 'btn btn-success btn-xs',
                'href' => Url::to(['update-address', 'id' => 'new','company_id'=>$model->id]),
            ],
        ]);?>

    </div>
</div>
