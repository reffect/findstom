<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\company\models\CategoryCompany;
use common\modules\company\models\CompanyCategory;
use kartik\depdrop\DepDrop; 

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\Company */

$this->registerJs('
$(document).ready(function() {
    $("#button-add-category").click(function () {
        company_id = Number($(this).attr("data-company-id"));
        category_id = Number($("#category_id-id").val());

        if (company_id>0 && category_id>0) {
            get_link = $(this).attr(\'data-category-link\');

            $.get(get_link, {
                company: company_id,
                category: category_id
            }).done(function (data) {
                    if (data == "ERROR SAVE") {
                        alert("Ошибка сохранения");
                    } else {
                        if (data == "HAVE BASE") {
                            alert("Категория уже была добавлена");
                        } else {
                            $("#categoryTree").html(data);
                        }
                    }
                }
            );
        } else {
            alert("Выбирите категорию и подкатегорию");
        }

        return false;
    });
})');


?>


<div class="row">
    <div class="col-md-12" id="categoryTree">
        <?= $this->render('_category_tree',['model'=>$model]); ?>
    </div>
</div>

<label><?= Yii::t("app", "Category company"); ?></label>
<div class="row">

    <div class="col-md-5">
        <?=Html::dropDownList(
            'parent-category',
            null,
            CategoryCompany::listMainCategories(),
            [
                'id'=>'parent-category',
                'class'=>'form-control',
                'prompt' => 'Выберите корневую категорию...',
            ]
        );?>
    </div>
    <div class="col-md-5">
        <?php $cc = new CompanyCategory(); ?>
        <?php echo $form->field($cc, 'category_id')->label(false)->widget(DepDrop::classname(), [
            'options'=>['id'=>'category_id-id'],
            'pluginOptions'=>[
                'depends'=>['parent-category'],
                'placeholder'=>'Выберите...',
                'url'=>Url::to(['category-select'])
            ]
        ]);?>
    </div>
    <div class="col-md-2">
        <a  data-category-link="<?=Url::to("add-category");?>"
            data-company-id="<?=$model->id;?>"
            id="button-add-category"
            class="btn btn-primary" >
            Добавить
        </a>
    </div>

</div>