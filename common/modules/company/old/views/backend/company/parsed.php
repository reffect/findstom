<?php
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 11.02.16
 * Time: 23:26
 */

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->registerJs('
//window.open("http://'.($model->website).'", "_blank");
$(".loadFormAjax").on(\'click\', function () {
        $.get($(this).data(\'model-edit-url\')).done(function (data) {
            $("#editForm").html(data);
        });
    });
');

\backend\assets\CompanyAsset::register($this);

?>

<div class="company-index well">
    <?php echo $this->render('update', [
        'model' => $model,
    ]); ?>
</div>

