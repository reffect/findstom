<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model  */

$this->title = Yii::t('common', 'Create Company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Company'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-services-create">

    <?php $form = ActiveForm::begin(['layout' => 'default']); ?>

    <?= $this->render('_form', [
        'model' => $model, 'form' => $form
    ]) ?>

    <?= \yii\helpers\Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>
