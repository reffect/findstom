<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this \yii\web\View */
/* @var $oldFiles [] */
/* @var $model \common\modules\company\models\CompanyImportForm */
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 24.02.16
 * Time: 12:50
@runtime
 */
$this->registerJs('

');
?>
<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin(['method'=>'post','options'=> ['enctype'=>'multipart/form-data','class'=>"smart-form"]]); ?>
        <?= $form->errorSummary($model); ?>
            <fieldset>
                <section>
                    <label class="label">Выбирите файл для импорта</label>
                    <div class="input input-file">
                        <span class="button">
                            <?= $form->field($model, 'csvFile')->fileInput([
                                'onchange'  => "$('.fileShow').val(this.value);",
                                'accept'    => "text/csv",
                            ]) ?>
                            Выбрать
                        </span>
                        <input class="fileShow" type="text" placeholder="выбирете файл формата *.csv" readonly="">
                    </div>
                    <br />
                    <button class="btn btn-default padding-5" type="submit">
                        Загрузить файл
                    </button>
                </section>
            </fieldset>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6">
    <h1>Ранее загруженые файлы:</h1>
        <?php foreach($oldFiles as $oldFile) { ?>
            <p>
                <b>
                    <?=date("d/m/Y h:i",filemtime(Yii::getAlias($model->dir.$oldFile)));?>
                </b>
                :
                <?=Html::a($oldFile,['import-company','md5'=>$oldFile],['class'=>'btn btn-default']); ?>
                <?=Html::a('<span class="glyphicon glyphicon-trash"></span>', ['import-delete','md5'=>$oldFile], [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'class'=>'btn btn-danger'
                ]); ?>
            </p>
        <?php } ?>
    </div>
</div>