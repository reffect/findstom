<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Progress;
use yii\bootstrap\ActiveForm;
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 26.02.16
 * Time: 12:36
 */

/* @var $file string */
/* @var $md5 string */
/* @var $this yii\web\View; */


list(,$urlForm)=Yii::$app->assetManager->publish('@bower/jquery-form/jquery.form.js');
$this->registerJsFile($urlForm, ['depends' => 'yii\jui\JuiAsset']);


$lines = count(file($file));

$this->registerJs(/** @lang javascript */
    '
var count_lines = '.((int)$lines).';
var importBtn = $("#button_import");
var progressBar = $(".progress-bar");
var count_step = '.($model->row).';
var import_url = "'.(Url::to()).'";

$(document).ready(function() {
    $("#import_number_form").ajaxForm({
        url:import_url,
        data: {
            position: 0,
        },
        beforeSubmit: function() {
            importBtn.prop("disabled", true);
        },
        success: function(data) {
            subimportform(data,count_step);
        }
    });
});


function subimportform(data,loadsss) {
    if (data=="ERROR_CSV") {
        alert("Загружать можно только CSV файл!!!");
        importBtn.prop("disabled", false );
        importBtn.html("Импортировать");
    } else {
        if (data=="ERROR_LOAD" && data=="ERROR_POST") {
            alert("Ошибка загрузки файла на сервер: "+data);
            importBtn.prop("disabled", false );
            importBtn.html("Импортировать");
        } else {
            if (data=="END") {
                progressBar.css("width","100%");
                progressBar.parent().removeClass("active");
                importBtn.prop("disabled", false );
                importBtn.html("Импортировать");
                alert("Импорт успешно завершен.");
            }

            if (data.indexOf("POS_")>-1 ) {
                var return_fseek = data.substr(4);
                $("#import_number_form").ajaxSubmit({
                    url: import_url,
                    data: {
                        position: return_fseek,
                    },
                    beforeSubmit: function() {
                        importBtn.prop("disabled", true );
                        importBtn.html("Загружено "+loadsss+" строк, продолжаем...");
                        progressBar.css("width",""+(loadsss*100/count_lines)+"%");
                    },
                    success: function(data) {
                        subimportform(data,(loadsss+count_step));
                    }
                });
            }

        }
    }
}
');

?>
<h1><?=$md5;?></h1>
<p>Размер файла: <b><?=number_format(filesize($file)/1024/1024,2,'.',' ');?> Мб</b></p>
<p>Дата загрузки: <b><?=date("d/m/Y h:i",filemtime($file));?></b></p>
<p>Количество строк в файле: <b id="lines_count"><?=$lines;?></b></p>

<?php $form = ActiveForm::begin([
    'id'=>'import_number_form',
    'options' => ['enctype'=>'multipart/form-data']
]); ?>
<div class="row">
    <?= $form->field($model, 'delimiter',['options'=>['class'=>'col-md-4']]) ?>
    <?= $form->field($model, 'enclosure',['options'=>['class'=>'col-md-4']]) ?>
    <?= $form->field($model, 'escape',['options'=>['class'=>'col-md-4']]) ?>
</div>

<div class="form-group">
    <?= Html::submitButton('Импортировать', ['id'=>'button_import','class' => 'btn btn-primary']) ?>
</div>
<?php $form->end();?>




<?php
echo Progress::widget([
    'id' => 'progress_bar',
    'percent' => 0,
    'barOptions' => ['class' => 'bg-color-purple'],
    'options' => ['class' => 'progress-striped active']
]);
?>

<?=Html::a('Удалить файл <span class="glyphicon glyphicon-trash"></span>', ['import-delete','md5'=>$md5], [
    'title' => Yii::t('yii', 'Delete'),
    'id' => 'delete_button',
    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
    'data-method' => 'post',
    'class'=>'btn btn-danger'
]); ?>

<hr />
<h3>Демо просмотр файла:</h3>
<?php tr($model->parseTest($lines),0); ?>