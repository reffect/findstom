<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\grid\StomEnumColumn;
use yii\helpers\Url;
use common\modules\company\models\CompanyAdr;

/* @var $this yii\web\View */
/* @var $model \common\modules\company\models\Company */



$this->title = Yii::t('common', 'Update {modelClass}: ', [
        'modelClass' => 'Company',
    ]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(['layout'=>'default']); ?>

    <ul class="nav nav-tabs nav-top-border" role="tablist">
        <li role="presentation" class="active">
            <a href="#tab-company-title" role="tab" data-toggle="tab">
                Инфо
            </a>
        </li>
        <li role="presentation">
            <a href="#tab-desc" role="tab" data-toggle="tab">
                Текст
            </a>
        </li>
        <li role="presentation">
            <a href="#tab-category" role="tab" data-toggle="tab">
                Категории
            </a>
        </li>
        <li role="presentation">
            <a href="#tab-address" role="tab" data-toggle="tab">
                Адреса
            </a>
        </li>
        <li role="presentation">
            <a href="#tab-images" role="tab" data-toggle="tab">
                Изображения
            </a>
        </li>
    </ul>

    <div class="tab-content padding-top-20">
        <div role="tabpanel" class="tab-pane fade in active" id="tab-company-title">
            <?= $this->render('_form',['model'=>$model,'form'=>$form]); ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab-desc">
            <?= $form->field($model, 'body')->widget(\common\widgets\ck\CKEditor::className(), [
                'preset' => 'full',
                'clientOptions' => [
                    'filebrowserUploadUrl' => Url::to(['/file-storage/ckload', 'item_dir' => $model->imgDir, 'item_id' => $model->thisId])
                ]
            ]) ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab-category">
            <?php /*= $this->render('_form_service',['model'=>$model,'form'=>$form]); */ ?>
            <?= $this->render('_form_category',['model'=>$model,'form'=>$form]); ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab-images">
            <?php /* = $this->render("_images", ['model' => $model,'form'=>$form]); */ ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab-address">
            <?= $this->render('address',['model'=>$model,'modelAdr'=>new CompanyAdr()]); ?>
        </div>
    </div>
    <hr />

    <?= \yii\helpers\Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>