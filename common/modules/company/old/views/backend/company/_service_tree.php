 <?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 07.02.16
 * Time: 13:40
 */

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$listPrice = $model->getListPrices();
$this->registerJs('

$(document).ready(function() {

    pageSetUp();

    // PAGE RELATED SCRIPTS

    $(\'.tree > ul\').attr(\'role\', \'tree\').find(\'ul\').attr(\'role\', \'group\');
    $(\'.tree\').find(\'li:has(ul)\').addClass(\'parent_li\').attr(\'role\', \'treeitem\').find(\' > span\').attr(\'title\', \'Collapse this branch\').on(\'click\', function(e) {
        var children = $(this).parent(\'li.parent_li\').find(\' > ul > li\');
        if (children.is(\':visible\')) {
            children.hide(\'fast\');
            $(this).attr(\'title\', \'Expand this branch\').find(\' > i\').removeClass().addClass(\'fa fa-lg fa-plus-circle\');
        } else {
            children.show(\'fast\');
            $(this).attr(\'title\', \'Collapse this branch\').find(\' > i\').removeClass().addClass(\'fa fa-lg fa-minus-circle\');
        }
        e.stopPropagation();
    });
})');

?>



<!-- widget div-->
<div>
    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->
    </div>
    <!-- end widget edit box -->
    <!-- widget content -->
    <div class="widget-body">

        <div class="tree smart-form">
            <ul>
                <li>
                    <span><i class="fa fa-lg fa-folder-open"></i> Корневая категория</span>
                    <ul>
                        <?php foreach($model->listServices as $parentCategoryTitle => $categories) { ?>
                            <li>
                                <span><i class="fa fa-lg fa-minus-circle"></i> <?=$parentCategoryTitle;?></span>
                                <ul>
                                    <?php foreach($categories as $categoryId => $categoryTitle) { ?>
                                        <li>
                                            <span>
                                                <i class="icon-leaf"></i>
                                                <?=$categoryTitle;?>
                                            </span>
                                            Стоимость:

                                            <?=Html::textInput('service['.($categoryId).'][price_start]',$listPrice[$categoryId][0],['style'=>'width:50px']);?>
                                            -
                                            <?=Html::textInput('service['.($categoryId).'][price_end]',$listPrice[$categoryId][1],['style'=>'width:50px']);?>

                                            <?=Html::button(
                                                '<i class="fa fa-times"></i>',
                                                [
                                                    'class'             =>'btn btn-xs btn-default',
                                                    'data-delete-id'    => $categoryId,
                                                    'onclick'           => "
                                                            if (confirm('подтверждаете удаление?')) {
                                                                $.get('".(Url::to(['delete-category']))."', {
                                                                    company: ".($model->id).",
                                                                    category: $(this).attr('data-delete-id')
                                                                }).done(function( data ) {
                                                                    $(\"#categoryTree\").html(data);
                                                                });
                                                            }
                                                        "
                                                ]
                                            ); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- end widget content -->
</div>
<!-- end widget div -->
