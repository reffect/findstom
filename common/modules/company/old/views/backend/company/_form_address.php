<?php

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\CompanyAdr */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\company\models\OpeningHours;
use yii\helpers\Url;
//use karnbrockgmbh\modal\Modal;

$this->registerJsFile('@common/modules/company/assets/js/company.js', ['yii\web\YiiAsset']);
$this->registerJs('
var myMap, coords, myPlacemark;

        var latitude = $("#companyadr-latitude");
        var longitude = $("#companyadr-longitude");

        ymaps.ready(init);

        latitude.change(function () {
            coords = [(latitude.val().length > 1) ? latitude.val() : \'42.9818\', (longitude.val().length > 1) ? longitude.val() : \'47.5075\'];
            myMap.setCenter(coords, 17);
            myPlacemark.getOverlay().getData().geometry.setCoordinates(coords);
        });

        longitude.change(function () {
            coords = [(latitude.val().length > 1) ? latitude.val() : \'42.9818\', (longitude.val().length > 1) ? longitude.val() : \'47.5075\'];
            myMap.setCenter(coords, 17);
            myPlacemark.getOverlay().getData().geometry.setCoordinates(coords);
        });

        $("#show_form_button").on(\'click\', function () {
            if($(this).text()=="Добавить aдрес"){
                $(this).text("Скрыть форму добавления адреса");
            } else {
                $(this).text("Добавить aдрес");
            }
        });


        $(".button_update").on(\'click\', function () {
            if($("#show_form_button").text()=="Добавить aдрес"){
                $("#show_form_button").text("Скрыть форму добавления адреса");
            } else {
                $("#show_form_button").text("Добавить aдрес");
            }
        });



        $("#save_button").on(\'click\', function () {
            if ($("#companyadr-street_address").val().length < 1) {
                alert("Адрес не указан. Укажите Адрес, прежде чем сохранить");
                return false;
            }
        });



        $("#search_geo").on(\'click\', function () {

            var $btn = $(this).button(\'loading\');
            // business logic...


            adr_country = $("#companyadr-country_name");
            adr_region = $("#companyadr-region");
            adr_locality = $("#companyadr-locality");
            adr_street_address = $("#companyadr-street_address");

            if (adr_street_address.val().length > 1) {
                search_address = adr_country.val() + ", " + adr_region.val() + ", " + adr_locality.val() + ", " + adr_street_address.val();

                $.get(\'/adminka/company/geo\', {address: search_address})
                    .done(function (data) {

                            if (adr_country.val() != data.CountryName) {
                                if (confirm(\'Страна указана не правильно, заменить "\' + (adr_country.val()) + \'" на "\' + (data.CountryName) + \'"?\')) {
                                    adr_country.val(data.CountryName);
                                }
                            }

                            if (adr_region.val() != data.AdministrativeAreaName) {
                                if (confirm(\'Регион указан не правильно, заменить "\' + (adr_region.val()) + \'" на "\' + (data.AdministrativeAreaName) + \'"?\')) {
                                    adr_region.val(data.AdministrativeAreaName);
                                }
                            }

                            if (adr_locality.val() != data.LocalityName) {
                                if (confirm(\'Населеный пункт указан не правильно, заменить "\' + (adr_locality.val()) + \'" на "\' + (data.LocalityName) + \'"?\')) {
                                    adr_locality.val(data.LocalityName);
                                }
                            }

                            full_street = (data.ThoroughfareName===undefined?"":data.ThoroughfareName) + ", " + (data.PremiseNumber===undefined?"":data.PremiseNumber);
                            if (adr_street_address.val() != full_street) {
                                if (confirm(\'Указаный адрес, отличается от полученого ответа от сервера, заменить "\' + (adr_street_address.val()) + \'" на "\' + (full_street) + \'"?\')) {
                                    adr_street_address.val(full_street);
                                }
                            }


                            latitude.val(data.Latitude);
                            longitude.val(data.Longitude);

                            coords = [data.Latitude, data.Longitude];
                            myMap.setCenter(coords, 17);
                            myPlacemark.getOverlay().getData().geometry.setCoordinates(coords);
                            $btn.button(\'reset\');
                        }
                    );
            } else {
                $btn.button(\'reset\');
                alert("Адрес не указан. Укажите Адрес для поиска в Яндекс.Картах");
            }

            return false;
        });


        function init() {

            coords = [
                latitude.val(),
                longitude.val()
            ];

            myMap = new ymaps.Map("map",
                {
                    center: coords,
                    zoom: 17,
                    behaviors: ["default", "scrollZoom"]
                },
                {
                    balloonMaxWidth: 300
                });


            //Определяем элемент управления поиск по карте
            var SearchControl = new ymaps.control.SearchControl({noPlacemark: true});

            //Добавляем элементы управления на карту
            myMap.controls
                .add(SearchControl)
                .add(\'zoomControl\')
                .add(\'typeSelector\')
                .add(\'mapTools\');


            //Определяем метку и добавляем ее на карту
            myPlacemark = new ymaps.Placemark(coords, {}, {preset: "twirl#redIcon", draggable: true});
            myMap.geoObjects.add(myPlacemark);

            //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
                coords = this.geometry.getCoordinates();
                savecoordinats();
            }, myPlacemark);

            //Отслеживаем событие щелчка по карте
            myMap.events.add(\'click\', function (e) {
                coords = e.get(\'coordPosition\');
                savecoordinats();
            });

            //Отслеживаем событие выбора результата поиска
            SearchControl.events.add("resultselect", function (e) {
                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
                savecoordinats();
            });


            //Функция для передачи полученных значений в форму
            function savecoordinats() {
                var new_coords = [coords[0].toFixed(6), coords[1].toFixed(6)];
                if (new_coords !== null) {
                    myPlacemark.getOverlay().getData().geometry.setCoordinates(new_coords);
                    document.getElementById("companyadr-latitude").value = new_coords[0];
                    document.getElementById("companyadr-longitude").value = new_coords[1];
                }
            }


        }





');
?>


<?php $form = ActiveForm::begin([
    'id' => 'address_form',
    'action' => Url::to(['update-address', 'id' => $model->id])
]); ?>

<?= $form->field($model, 'company_id')->hiddenInput()->label(false); ?>


    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Редактирование адреса</h4>
    </div>
    <div class="modal-body">

        <div id="dialog_address_<?= $model->id; ?>" title="Dialog Simple Title">

            <div class="row">
                <div class="col-md-7">
                    <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'country_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'locality')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <?= $form->field($model, 'street_address')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'extended_address')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>


                    <?= $form->field($model, 'getting_there')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'opening_hours')->dropDownList(OpeningHours::listOptions()) ?>

                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-md-6">
                            <br/>
                            <button type="button" id="search_geo" data-loading-text="Нацелеваем спутник..."
                                    class="btn btn-default" autocomplete="off">
                                Найти координаты
                            </button>
                        </div>
                    </div>
                    <div id="map" style="width:100%; height:350px"></div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?= Html::submitButton(Yii::t('common', 'Save'), ['id' => 'save_button', 'class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>