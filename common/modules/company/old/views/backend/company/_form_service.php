<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\CategoryCompany;
use common\models\CompanyCategory;
use kartik\depdrop\DepDrop;
use yii\widgets\ActiveForm;
use common\models\CompanyServices;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->registerJs('
$(document).ready(function() {
    $("#button-add-category").click(function () {
        company_id = Number($(this).attr("data-company-id"));
        category_id = Number($("#services_id-id").val());

        if (company_id>0 && category_id>0) {
            get_link = $(this).attr(\'data-category-link\');

            $.get(get_link, {
                company: company_id,
                category: category_id
            }).done(function (data) {
                    if (data == "ERROR SAVE") {
                        alert("Ошибка сохранения");
                    } else {
                        if (data == "HAVE BASE") {
                            alert("Услуга уже была добавлена");
                        } else {
                            $("#categoryTree").html(data);
                        }
                    }
                }
            );
        } else {
            alert("Выбирите категорию и подкатегорию");
        }

        return false;
    });
})');


?>


<div class="row">
    <div class="col-md-12" id="categoryTree">
        <?= $this->render('_service_tree',['model'=>$model]); ?>
    </div>
</div>


<label><?= Yii::t("common", "Service company"); ?></label>
<div class="row">

    <div class="col-md-5">
        <?=Html::dropDownList(
            'parent-services',
            null,
            CompanyServices::listMainCategories(0),
            [
                'id'=>'parent-services',
                'class'=>'form-control',
                'prompt' => 'Выберите категорию...',
            ]
        );?>
    </div>
    <div class="col-md-5">
        <?php $cc = new \common\models\CompanyPrice(); ?>
        <?php echo $form->field($cc, 'services_id')->label(false)->widget(DepDrop::classname(), [
            'options'=>['id'=>'services_id-id'],
            'pluginOptions'=>[
                'depends'=>['parent-services'],
                'placeholder'=>'Выберите...',
                'url'=>Url::to(['service-select'])
            ]
        ]);?>
    </div>
    <div class="col-md-2">
        <a  data-category-link="<?=Url::to("add-service");?>"
            data-company-id="<?=$model->id;?>"
            id="button-add-category"
            class="btn btn-primary" >
            Добавить
        </a>
    </div>

</div>