<?php

use common\components\images\widgets\Uploadifive;
use trntv\filekit\widget\Upload;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $imageProvider yii\data\ActiveDataProvider */

?>
<div style="overflow: hidden;margin-top: 20px;">
    <label><?=$model->getAttributeLabel('thumbnail');?></label>
<?php echo Upload::widget([
    'model' => $model,
    'attribute' => 'thumbnail',
    'url' => ['/file-storage/upload', 'item_dir' => $model->imgDir, 'item_id' => $model->thisId],
    'maxFileSize' => 10 * 1024 * 1024, // 10Mb
    'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
    'clientOptions' => ['delete_url' => Url::to(['/file-storage/delete-rimage2', 'file_url' => $model->thumbUrl, 'item_dir' => $model->imgDir, 'item_id' => $model->thisId])],
]); ?>

<div id="ajax-load-images-grid">
<?=$this->render('_images_grid',['model'=>$model]);?>
</div>

<?php echo Uploadifive::widget([
    'tableAttribute'    => $model->imgDir,
    'idAttribute'       => $model->id,
    'uploadScriptUrl'   => '/file-storage/uploadfive',
    'onComplete'        => '$.get(\''.(Url::to(['panel-images'])).'\', {
                                company: '.($model->id).'
                            }).done(function(data) {
                                $("#ajax-load-images-grid").html(data);
                            });'
]); ?>
</div>
