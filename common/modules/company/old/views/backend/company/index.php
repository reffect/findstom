<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\company\models\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//\backend\assets\CompanyAsset::register($this);

$this->registerJs('
$(".loadFormAjax").on(\'click\', function () {
        $.get($(this).data(\'model-edit-url\')).done(function (data) {
            $("#editForm").html(data);
        });
    });
');



$this->title = Yii::t('common', 'Companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <p>
        <?= Html::a(Yii::t('common', 'Create Company'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="row">
        <div class="col-md-6">
            <?php \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'options' => [
                            'style' => 'width:70px;'
                        ]
                    ],
                    'title',
                    //'slug',
                    [
                        'class'=>\common\grid\EnumColumn::className(),
                        'attribute'=>'status',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '<div class="text-center">{view_slug} {update} {delete}</div>'
                    ],
                ]
            ]); ?>
        </div>
        <div class="col-md-6" id="editForm">

        </div>
    </div>



</div>
