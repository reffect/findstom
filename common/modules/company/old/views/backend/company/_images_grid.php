<?php

/* @var $model common\models\Company */
/* @var $imageProvider yii\data\ActiveDataProvider */

use yii\helpers\Url;
use yii\helpers\Html;

echo \yii\grid\GridView::widget([
    'dataProvider' => $model->imageProvider()['provider'],
    'columns' => [
        [
            'attribute' => 'url',
            'format'=>'raw',
            'value'=> function ($model, $index, $widget) {
                return \yii\helpers\Html::img($model->url);
            },
            'options'=>['style'=>'width:250px;']
        ],
        [
            'attribute' => 'title',
            'format'=>'raw',
            'value'=> function ($model, $index, $widget) {
                return
                    (Html::textarea('R731Images['.($model->id).'][title]',$model->title,['style'=>'width:100%','maxlength' => true])).
                    "<br />". (Html::checkbox('R731Images['.($model->id).'][file]',false)).
                    "<span> &nbsp;".(Yii::t('common','Change the file name'))." </span>";
            },
        ],
        /* [
             'attribute' => 'type',
             'format'=>'raw',
             'value'=> function ($model, $index, $widget) {
                 //return (\yii\helpers\Html::dropDownList('Image['.($model->id).'][type]', $model->type, ,['class'=>'w100','maxlength' => true]));
             },
             'options'=>['style'=>'width:140px;']
         ], */
        [
            'attribute' => 'position',
            'label'=>'',
            'format'=>'raw',
            'value'=> function ($model, $index, $widget) {
                return (Html::textInput('R731Images['.($model->id).'][position]',$model->position,['class'=>'w100','maxlength' => true]));
            },
            'options'=>['style'=>'width:50px;']
        ],
        [
            'attribute' => 'position',
            'label'=>'',
            'format'=>'raw',
            'value'=> function ($model, $index, $widget) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                    null,
                    [
                        'title' => Yii::t('yii', 'Delete'),
                        'data-pjax'=>'0',
                        'class'=>'btn btn-danger btn-xs',
                        'onclick' => '
                            if (confirm("Уалить фото?")) {
                                $.ajax({
                                    type     : "POST",
                                    dataType : "json",
                                    data : { delete_id : "'.($model->id).'", "'.(Yii::$app->request->csrfParam).'" : "'.(Yii::$app->request->getCsrfToken()).'" } ,
                                    url  : "'.(Url::to(['/file-storage/deleteimg'])).'",
                                    success  : function(data) {
                                        if (data == "GOOD") {
                                            $.get(\''.(Url::to(['panel-images'])).'\', {
                                                company: '.($model->object_id).'
                                            }).done(function(data) {
                                                $("#ajax-load-images-grid").html(data);
                                            });
                                        } else {
                                            alert("Ошибка удаления: "+data);
                                        }
                                    }
                                });
                            }
                            ',
                    ]
                );
            },
            'options'=>['style'=>'width:50px;']
        ],
    ],
]); ?>