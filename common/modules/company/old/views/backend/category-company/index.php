<?php

use yii\helpers\Html;
use common\modules\company\models\CategoryCompany;
use yii\helpers\Url;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $searchModel common\modules\company\models\search\CategoryCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Category Companies');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(document).ready(function() {

    $(\'.tree > ul\').attr(\'role\', \'tree\').find(\'ul\').attr(\'role\', \'group\');
    $(\'.tree\').find(\'li:has(ul)\').addClass(\'parent_li\').attr(\'role\', \'treeitem\').find(\' > span\').attr(\'title\', \'Collapse this branch\').on(\'click\', function(e) {
        var children = $(this).parent(\'li.parent_li\').find(\' > ul > li\');
        if (children.is(\':visible\')) {
            children.hide(\'fast\');
            $(this).attr(\'title\', \'Expand this branch\').find(\' > i\').removeClass().addClass(\'fa fa-lg fa-plus-circle\');
        } else {
            children.show(\'fast\');
            $(this).attr(\'title\', \'Collapse this branch\').find(\' > i\').removeClass().addClass(\'fa fa-lg fa-minus-circle\');
        }
        e.stopPropagation();
    });
})');
?>
<div class="category-company-index">
    <p>
        <?= Html::a(Yii::t('common', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <!-- widget div-->
    <div>
        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
        </div>
        <!-- end widget edit box -->
        <!-- widget content -->
        <div class="widget-body">

            <div class="tree smart-form">
                <ul>
                    <li>
                        <span><i class="fa fa-lg fa-folder-open"></i> Корневая категория</span>
                        <ul>
                            <?php foreach(CategoryCompany::find()->parent(1)->ds()->all() as $parentCategories) {
                                $childrenCategories = CategoryCompany::find()->ds()->parent($parentCategories->id)->all();
                                ?>
                                <li>
                                    <span>
                                        <i class="fa fa-lg fa-plus-circle"></i>
                                        <?=$parentCategories->title;?>
                                        <?php if (!$childrenCategories) echo Html::button(
                                            '<i class="fa fa-times"></i>',
                                            [
                                                'class'             =>'btn btn-xs btn-default',
                                                'onclick'           => "
                                                            if (confirm('подтверждаете удаление?')) {
                                                                $.post('".(Url::to(['delete','id'=>$parentCategories->id]))."').done(function( data ) {
                                                                    $(\"#categoryTree\").html(data);
                                                                });
                                                            }
                                                        "
                                            ]
                                        ); ?>
                                    </span>
                                    <ul >
                                        <?php
                                        if ($childrenCategories) foreach($childrenCategories as $category) {  ?>
                                            <li style="display: none">
                                            <span>
                                                <i class="icon-leaf"></i>
                                                <?=$category->title;?>
                                                <?=Html::a(
                                                    '<i class="fa fa-pencil"></i>',
                                                    ['update','id'=>$category->id],
                                                    [
                                                        'class'             =>'btn btn-xs btn-primary',
                                                    ]
                                                ); ?>

                                                <?=Html::button(
                                                    '<i class="fa fa-times"></i>',
                                                    [
                                                        'class'             =>'btn btn-xs btn-default',
                                                        'onclick'           => "
                                                            if (confirm('подтверждаете удаление?')) {
                                                                $.post('".(Url::to(['delete','id'=>$category->id]))."').done(function( data ) {
                                                                    $(\"#categoryTree\").html(data);
                                                                });
                                                            }
                                                        "
                                                    ]
                                                ); ?>
                                            </span>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end widget content -->
    </div>
    <!-- end widget div -->
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
//    'options'=> ['class'=>'width75'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end();
