<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\company\models\CategoryCompany;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\CategoryCompany */
/* @var $form yii\widgets\ActiveForm */

$listParent = CategoryCompany::listMainCategories();
$listParent['1'] = 'Корневая категория';

?>

<div class="category-company-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'parent_id')->dropDownList($listParent) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList(\common\grid\EnumColumn::listData()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            <?= $form->field($model, 'seo_desc')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'seo_key')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
        </div>
    </div>


    <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>
