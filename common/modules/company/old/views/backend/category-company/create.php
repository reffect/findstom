<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CategoryCompany */

$this->title = Yii::t('common', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Category Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-company-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
