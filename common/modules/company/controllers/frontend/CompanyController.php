<?php

namespace common\modules\company\controllers\frontend;

use common\modules\company\models\DentalCategory;
use common\modules\company\models\DentalCompanyAddress;
use common\modules\company\models\search\DentalCompanyAddressSearch;
use common\modules\seo\models\Seo;
use Yii;
use common\modules\company\models\DentalCompany;
use common\modules\company\models\search\DentalCompanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyController implements the CRUD actions for DentalCompany model.
 */
class CompanyController extends Controller
{

    /**
     * Lists all DentalCompany models.
     * @return mixed
     */
    public function actionImport($city=null)
    {
        header('Content-Type: application/xml');
        
        
        //Создает XML-строку и XML-документ при помощи DOM 
        $dom = new \DomDocument('1.0'); 
         
        //добавление корня - <books> 
        $companies = $dom->appendChild($dom->createElement('companies')); 
         
        foreach(DentalCompany::find()->andWhere(['company_id'=> ['237']])->limit(10)->all() as $c) {
            $company = $companies->appendChild($dom->createElement('company')); 
             
            foreach($c->attributes as $attr => $val) {
                $title = $company->appendChild($dom->createElement($attr)); 
                $title->appendChild($dom->createTextNode($val));     
            }
            
            $companyAddresses = $company->appendChild($dom->createElement('addresses')); 
            foreach($c->addresses as $addr) {
                $address = $companyAddresses->appendChild($dom->createElement('address')); 
                
                foreach($addr->attributes as $attr => $val) {
                    $title = $address->appendChild($dom->createElement($attr)); 
                    $title->appendChild($dom->createTextNode($val));   
                } 
                
                if (!empty($addr->city)) {
                    $title = $address->appendChild($dom->createElement('city_name')); 
                    $title->appendChild($dom->createTextNode($addr->city->title));    
                }
                
                if (!empty($addr->opening_hours_id)) {
                    $title = $address->appendChild($dom->createElement('opening_hours')); 
                    $title->appendChild($dom->createTextNode($addr->openingHours->public_text));    
                }
                
                
                $companyAddressesMetros = $companyAddresses->appendChild($dom->createElement('metros')); 
                foreach($addr->addressMetro as $metro) {
                    $companyAddressesMetro = $companyAddressesMetros->appendChild($dom->createElement('metro')); 
                    
                    $tt = \common\modules\company\models\DentalMetro::find()->andWhere(['metro_id'=>$metro->metro_id])->one();
                    if (!empty($tt)) {
                        //$metro
                        $title = $companyAddressesMetro->appendChild($dom->createElement('metro'));
                        
                        $title->appendChild($dom->createTextNode($tt->title));
                                            
                        //$metro
                        $title = $companyAddressesMetro->appendChild($dom->createElement('city')); 
                        $title->appendChild($dom->createTextNode($addr->city->title));
                    }                   
                    
                    
                    
                }
            }
            
            
            
            //
            
            
            $companySocialss = $company->appendChild($dom->createElement('socialss')); 
            foreach($c->social as $social) {
                $socialss = $companySocialss->appendChild($dom->createElement('social')); 
                
                foreach($social->attributes as $attr => $val) {
                    $title = $socialss->appendChild($dom->createElement($attr)); 
                    $title->appendChild($dom->createTextNode($val));   
                } 
            }
            
            
        }

         
        //генерация xml 
        $dom->formatOutput = true; // установка атрибута formatOutput
                                   // domDocument в значение true 
        // save XML as string or file 
        $test1 = $dom->saveXML(); // передача строки в test1 
        
        print ($test1);
        
    }
    
    /**
     * Lists all DentalCompany models.
     * @return mixed
     */
    public function actionIndex($city)
    {
        Yii::$app->geo->cheekDomain($city);

        $seo = $this->getSeo('Стоматологии {{cityPp}}');

        $searchModel = new DentalCompanyAddressSearch();
        $dataProvider = $searchModel->city(Yii::$app->geo->city_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'countClinics' => $dataProvider->query->count(),
            'seo' => $seo
        ]);
    }

    /**
     * @param $slug
     * @param $address
     * @return string
     */
    public function actionView($city, $slug, $address = null)
    {
        $company = $this->findModel($slug);

        /** @var $addressModel DentalCompanyAddress */

        if (is_null($address)) {
            $addressModel = $company->getAddresses()->one();
            $citySlug = $addressModel->getCitySlug();
            $this->redirect([
                'view',
                'city' => $citySlug,
                'slug' => $slug,
                'address' => $addressModel->slug
            ], 301);
        } else {
            $addressModel = $addressModel = $this->findModelAddress($address);
            $citySlug = $addressModel->getCitySlug();
            Yii::$app->geo->setDomain($citySlug);
            if ($citySlug != $city) {
                $this->redirect([
                    'view',
                    'city' => $citySlug,
                    'slug' => $slug,
                    'address' => $addressModel->slug
                ], 301);
            }
        }

//        /** @var DentalCategory $category */
//        $category = $company->getCategories()->one();

        $seo = $this->getSeo('{{title}}', [
          //  'category' => $category->title,
            'title' => $company->title
        ]);

        $breadcrumb = Seo::br(
            "company/{$this->id}",
            "index",
            ['cityPp' => Yii::$app->geo->titlePp],
            'Стоматологии {{cityPp}}'
        );

        $othersAddress = $company->getAddresses()->andWhere([
            '<>',
            'company_address_id',
            $addressModel->company_address_id
        ])->all();

        return $this->render('view', [
            'company' => $company,
            'category' => null,
            'seo' => $seo,
            'breadcrumb' => $breadcrumb,
            'address' => $addressModel,
            'othersAddress' => $othersAddress,
        ]);
    }



    /**
     * Finds the DentalCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return DentalCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = DentalCompany::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the DentalCompanyAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return DentalCompanyAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelAddress($slug)
    {
        if (($model = DentalCompanyAddress::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * @param string $default
     * @param array $replaceArray
     * @return Seo
     */
    public function getSeo($default = '', $replaceArray = [])
    {
        return Seo::parse(
            "company/{$this->id}",
            $this->action->id,
            $replaceArray,
            $default
        );
    }
}
