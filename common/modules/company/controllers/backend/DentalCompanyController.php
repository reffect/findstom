<?php

namespace common\modules\company\controllers\backend;

use common\modules\company\models\DentalCompanyAddress;
use common\modules\company\models\DentalCompanyParser;
use common\modules\company\models\DentalCompanySocial;
use common\modules\company\models\DentalOpeningHours;
use modules\company\models\CompanyNocopy;
use modules\company\models\DentalAddressMetro;
use vova07\imperavi\helpers\FileHelper;
use Yii;
use common\modules\company\models\DentalCompany;
use common\modules\company\models\search\DentalCompanySearch;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DentalCompanyController implements the CRUD actions for DentalCompany model.
 */
class DentalCompanyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @param $id
     * @return array|null
     */
    public function actionFindCoordinates($id)
    {
        $model = $this->findModelAddress($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $model->getYandexCoordinates(true);
    }

    /**
     * @return array
     */
    public function actionSetAddress($type = 'address')
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!is_null(Yii::$app->request->post('hasEditable'))) {
            // use Yii's response format to encode output as JSON

            $model = $this->findModelAddress(Yii::$app->request->post('attribute-primary'));

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return ['output'=> $model->getText($type), 'message'=> ''];
            } else {
                return ['output'=>'', 'message'=> implode(",", $model->getErrors())];
            }
        }
        return ['output'=>'', 'message'=>'Error'];
    }

    /**
     * @return array
     */
    public function actionSetSocial()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // Check if there is an Editable ajax request
        if (!is_null(Yii::$app->request->post('hasEditable'))) {
            // use Yii's response format to encode output as JSON

            $model = $this->findModelSocial(Yii::$app->request->post('attribute-primary'));

            $post = Yii::$app->request->post('DentalCompanySocial');

            if (isset($post['url'])) {
                $model->updateAttributes(["url" => $post['url']]);
                return ['output'=>Html::a($model->url, $model->url, [
                    "class" => "btn btn-default",
                    "target" => "_blank",
                ]), 'message'=>''];
            }
        }
        return ['output'=>'', 'message'=>'Error'];
    }

    /**
     * @return array
     */
    public function actionSetGeoAddress($id)
    {
        $model = $this->findModelAddress($id);
        $model->changeDataToYandexGeoCoder();
    }

    /**
     * @return array
     */
    public function actionSetOpenHours($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $address = $this->findModelAddress($id);

        $oh = new DentalOpeningHours();
        $oh->public_text = $oh->getArrayToText(Yii::$app->request->post('oh'));

        $find = DentalOpeningHours::find()->andWhere([
            'public_text' => $oh->public_text
        ])->one();

        if (is_null($find)) {
            if (!$oh->save()) {
                return ['output'=>'', 'message'=>'Error save Opening Hours'];
            }
            $find = $oh;
        }

        $address->updateAttributes([
            'opening_hours_id' => $find->opening_hours_id
        ]);

        return ['output'=>$find->getNiceText(), 'message'=>''];
    }

    /**
     * @param $id
     * @param int $status
     * @return \yii\web\Response
     */
    public function actionCheckCompany($id, $status =  DentalCompany::STATUS_GOOD) {
        $model = $this->findModel($id);
        foreach ($model->addresses as $address) {
            $address->status = $status;
            $address->save(false, ['status']);
            $address->getCitySlug();
        }
        $model->updateAttributes(['status' =>$status, 'updated_at' => time()]);
        return $this->redirect(['moderation']);
    }

    /**
     * @return array
     */
    public function actionSetCompany()
    {
        // Check if there is an Editable ajax request
        if (!is_null(Yii::$app->request->post('hasEditable'))) {
            // use Yii's response format to encode output as JSON
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model = $this->findModel(Yii::$app->request->post('attribute-primary'));
            $post = Yii::$app->request->post('DentalCompany');

            if (isset($post['logoFile'])) {
                $model->logoFile = UploadedFile::getInstance($model, 'logoFile');

                if (isset($post['isDeleteLogo']) && $post['isDeleteLogo']) {
                    if ($model->deleteLogo() && $model->logoFile === null) {
                        return [
                            'output' => null,
                            'message' => ''
                        ];
                    }
                }

                if ($model->validate()) {
                    $pathSave = Yii::getAlias("@files/company/{$model->company_id}");
                    FileHelper::createDirectory($pathSave);
                    $newName = Inflector::slug($model->logoFile->baseName,'_').
                        "_". uniqid() . '.' . $model->logoFile->extension;

                    if ($model->logoFile->saveAs("{$pathSave}/{$newName}")) {
                        if (!empty($model->logo)) {
                            $model->deleteLogo();
                        }
                        $model->logo = "/files/company/{$model->company_id}/{$newName}";
                        if ($model->save(true, ['logo'])) {
                            return [
                                'output' => Html::img($model->logo),
                                'message' => ''
                            ];
                        }
                    }
                }

                return [
                    'output' => '',
                    'message' => $model->getFirstError('logoFile')
                ];
            }

            foreach ($post as $attribute => $value) {
                if ($attribute == 'company_id') continue;
                $model->updateAttributes([$attribute => $value]);
            }

            if (isset($post['title'])) {
                return ['output'=>$model->title, 'message'=>''];
            }
            if (isset($post['website'])) {
                return ['output'=>$model->website, 'message'=>''];
            }
            if (isset($post['email'])) {
                return ['output'=>$model->email, 'message'=>''];
            }
        }

        return ['output'=>'', 'message'=>'Error'];
    }

    public function actionModerationAddress()
    {
//        if (Yii::$app->request->isPost) {
//            $post = Yii::$app->request->post();
//            $saves = [];
//            foreach ($post as $attribute => $arrayValues) {
//                if ($attribute == '_csrf') {
//                    continue;
//                }
//                foreach ($arrayValues as $primaryKey => $value) {
//                    $saves[$primaryKey][$attribute] = $value;
//                }
//            }
//
//            foreach ($saves as $primaryKey => $value) {
//                $modelSave = $this->findModelAddress($primaryKey);
//                $modelSave->load($value, '');
//                $modelSave->status = DentalCompany::STATUS_GOOD;
//                $modelSave->save();
//            }
//        }
//
//        $good = "";
//        $loaddedModel = 0;
//
//        $moderateModels = [];
//
//        while ($loaddedModel < 1) {
//            $model = DentalCompanyAddress::find()
//                ->orderBy('RAND()')
//                ->andWhere(['status' => DentalCompany::STATUS_IMPORT])
//                ->one();
//
//            $model->changeDataToYandexGeoCoder();
//            if ($model->isSimularAddress()) {
//                $good .= "" . ($model->full_address) . " = " . ($model->getText('full2')). "<br /> \n";
//                $model->status = DentalCompany::STATUS_GOOD;
//                $model->save();
//            } else {
//                $moderateModels[] = $model;
//                $loaddedModel++;
//            }
//        }
//
//
//        return $this->render('moderation/moderation-address', [
//            'moderateModels' => $moderateModels,
//            'good' => $good
//        ]);
    }

    public function actionNocopy($id1, $id2)
    {
        $new = new CompanyNocopy();
        $new->company1_id = $id1;
        $new->company2_id = $id2;
        $new->save();

        return $this->redirect('duplicate');
    }

    public function actionDuplicate()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $saveCompanyAddress = $this->findModelAddress($post['address']['id_confirm']);
            $saveCompanyAddress->load($post, 'address');
            $saveCompanyAddress->save();
            $deleteCompanyAddress = $this->findModelAddress($post['address']['id_delete']);
            DentalAddressMetro::updateAll(
                ['company_address_id' => $saveCompanyAddress->company_address_id],
                "company_address_id = {$deleteCompanyAddress->company_address_id}"
            );
            $deleteCompanyAddress->delete();

            $saveCompany = $this->findModel($post['company']['id_confirm']);
            $saveCompany->load($post, 'company');
            $saveCompany->save();

            if ($post['company']['id_confirm'] !== $post['company']['id_delete']) {
                $deleteCompany = $this->findModel($post['company']['id_delete']);
                DentalCompanyAddress::updateAll(
                    ['company_id' => $saveCompany->company_id],
                    "company_id = {$deleteCompany->company_id}"
                );
                DentalCompanyParser::updateAll(
                    ['company_id' => $saveCompany->company_id],
                    "company_id = {$deleteCompany->company_id}"
                );
                DentalCompanySocial::updateAll(
                    ['company_id' => $saveCompany->company_id],
                    "company_id = {$deleteCompany->company_id}"
                );

                $deleteCompany->delete();
            }

            return $this->redirect('duplicate');
        }


        $listCompanyFind = DentalCompanyAddress::find()
            ->andWhere(['checked_nocopy' => 0])
//            ->andWhere(['company_id' => 4287])
            ->orderBy('RAND()');

        foreach ($listCompanyFind->all() as $model) {
            $duplicate = DentalCompanyAddress::find()->andWhere([
                'country_name' => $model->country_name,
                'locality' => $model->locality,
                'street_address' => $model->street_address,
                'premise_number' => $model->premise_number,
            ])->andWhere(['<>', 'company_address_id', $model->company_address_id])->one();
            if (!is_null($duplicate)) {
                $joinPhones = array_intersect($model->getPhones(), $duplicate->getPhones());
                if ($joinPhones) {
                    $modelCompany = $model->company;
                    $duplicateCompany = $duplicate->company;

                    if (is_null($modelCompany) || is_null($duplicateCompany)) {
//                        echo '<pre>'.print_r($model->company_id,1).'</pre>';
//                        echo '<pre>'.print_r($duplicate->company_id,1).'</pre>';
//                        echo '<pre>'.print_r($modelCompany,1).'</pre>';
//                        echo '<pre>'.print_r($duplicateCompany,1).'</pre>';
//                        die;
                        continue;
                    }

                    foreach ($model->attributes as $attribute => $value) {
                        if ($attribute == 'opening_hours_id') {
                            $model->$attribute = max($value, $duplicate->$attribute);
                        }
                        if ($attribute == 'phone') {
                            $model->$attribute = implode(', ', array_unique(
                                array_merge($model->getPhones(), $duplicate->getPhones())
                            ));
                        }

                        if (empty($value) && !empty($duplicate->$attribute)) {
                            $model->$attribute = $duplicate->$attribute;
                        }
                    }

                    $model->save(false);
                    $duplicate->delete();

                    if ($model->company_id !== $duplicate->company_id) {
                        foreach ($modelCompany->attributes as $attribute => $value) {
                            if ($attribute == 'body') {
                                $modelCompany->$attribute = "{$modelCompany->$attribute} - {$duplicateCompany->$attribute}";
                            }
                            if (empty($value) && !empty($duplicateCompany->$attribute)) {
                                $modelCompany->$attribute = $duplicateCompany->$attribute;
                            }
                        }
                        $modelCompany->save(false);
                        DentalCompanyAddress::updateAll(
                            ['company_id' => $modelCompany->company_id],
                            "company_id = {$duplicateCompany->company_id}"
                        );
                        DentalCompanyParser::updateAll(
                            ['company_id' => $modelCompany->company_id],
                            "company_id = {$duplicateCompany->company_id}"
                        );
                        DentalCompanySocial::updateAll(
                            ['company_id' => $modelCompany->company_id],
                            "company_id = {$duplicateCompany->company_id}"
                        );

                        $duplicateCompany->delete();
                    }
                    continue;
                } else {
//                    break;
                    $model->checked_nocopy = 5;
                    $model->save(false, ['checked_nocopy']);
                }
            } else {
                $model->checked_nocopy = 1;
                $model->save(false, ['checked_nocopy']);
            }
        }

        return $this->render('moderation/duplicate', [
            'model' => $model,
            'duplicate' => $duplicate,
            'count' => $listCompanyFind->count()
        ]);
    }




    /**
     * @return array|string
     */
    public function actionModeration($id = null, $status = DentalCompany::STATUS_IMPORT, $w = true)
    {
        $find = DentalCompany::find()
            ->orderBy('rand()')
            //->andWhere(['company_id' => 14])
            ->andWhere(['status' => $status]);

        if ($w) {
            $find->andWhere(['is not', 'website', null]);
        }

        $count = $find->count();

        if (is_null($id)) {
            $model = $find->one();
            $this->redirect(['moderation', 'id' => $model->company_id]);
        } else {
            $find->andWhere(['company_id' => $id]);
        }

        $model = $find->one();

        // Check if there is an Editable ajax request
        if (!is_null(Yii::$app->request->post('hasEditable'))) {
            // use Yii's response format to encode output as JSON
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $this->saveAddress(
                Yii::$app->request->post('kv-complex'),
                Yii::$app->request->post()
            );
        }

        $lastDaytime = strtotime(
            "-1day",
            mktime(0,0,0,date("m"),date("d"),date("Y"))
        );
        $countModeratedThisDay = DentalCompany::find()->andWhere(['>=', 'updated_at', $lastDaytime])->count();

        return $this->render('moderation/index', [
            'model' => $model,
            'noModerateCount' => $count,
            'countModeratedThisDay' => $countModeratedThisDay
        ]);
    }

    /**
     * @param $id
     * @param $post
     * @return array
     */
    private function saveAddress($id, $post) {
        $model = $this->findModelAddress($id);

        // read your posted model attributes
        if ($model->load($post)) {
            // read or convert your posted information
            $value = $model->getText('full');

            // return JSON encoded output in the below format
            return ['output'=>$value, 'message'=>''];

            // alternatively you can return a validation error
            // return ['output'=>'', 'message'=>'Validation error'];
        }
        // else if nothing to do always return an empty JSON encoded output
        else {
            return ['output'=>'', 'message'=>''];
        }
    }

    public function actionImport()
    {
//        $import = CompanyImport::find()->andWhere(['imported'=>0])->one();
//
//        while (!is_null($import)) {
//            /** @var CompanyImport $import */
//
//            $company = [
//                'title' => $import->h1,
//                'seo_h1' => $import->h1,
//                'body' => $import->body,
//                'website' => "http://" . rtrim($import->website, "http://"),
//                'email' => $import->email,
//            ];
//
//            $model = DentalCompany::import($company);
//            $import->imported = 1;
//
//            if (is_null($model)) {
//                $import->imported = 2;
//            } else {
//                $modelImport = DentalCompanyParser::import([
//                    'company_id' => $model->company_id,
//                    'parsing_class' => $import->parsing_class,
//                    'parsing_id' => $import->parsing_id,
//                    'parsing_time' => $import->parsing_time,
//                    'parsing_url' => $import->parsing_url,
//                    'parsing_hash' => md5(implode("_", $company)),
//                ]);
//
//                if (is_null($modelImport)) {
//                    $import->imported += 100;
//                }
//
//                $openingHours = DentalOpeningHours::import([
//                    'public_text' => $import->opening_hours
//                ]);
//
//                $addressArray = [
//                    'company_id' => $model->company_id,
//                    'type' => 0,
//                    'country_name' => $import->country,
//                    'region' => $import->country,
//                    'locality' => $import->locality,
//                    'metro' => $import->metro,
//                    'district' => $import->district,
//                    'city_id' => 0,
//                    'street_address' => $import->address_street,
//                    'getting_there' => $import->getting_there,
//                    'extended_address' => $import->address_extended,
//                    'latitude' => $import->latitude,
//                    'longitude' => $import->longitude,
//                    'phone' => $import->phone,
//                    'opening_hours_id' => is_null($openingHours)?null:$openingHours->opening_hours_id,
//                ];
//
//                $address = DentalCompanyAddress::import($addressArray);
//                if (is_null($address)) {
//                    $import->imported += 1000;
//                }
//            }
//            $import->save(false, ['imported']);
//            $import = CompanyImport::find()->andWhere(['imported'=>0])->one();
//        }
    }

    public function actionImport1()
    {
//        $import = Company2::find()->andWhere(['imported'=>0])->one();
//
//        while (!is_null($import)) {
//            /** @var Company2 $import */
//
//            $company = [
//                'title' => $import->title,
//                'seo_h1' => $import->title,
//                'body' => $import->body,
//                'logo' => $import->logo,
//                'website' => "http://" . rtrim($import->website, "http://"),
//                'email' => $import->email,
//            ];
//
//            $model = DentalCompany::import($company);
//            $import->imported = 1;
//
//            if (is_null($model)) {
//                $import->imported = 2;
//            } else {
//                $modelImport = DentalCompanyParser::import([
//                    'company_id' => $model->company_id,
//                    'parsing_class' => $import->parsing_class,
//                    'parsing_id' => $import->parsing_id,
//                    'parsing_time' => $import->parsing_time,
//                    'parsing_url' => $import->parsing_url,
//                    'parsing_hash' => md5(implode("_", $company)),
//                ]);
//
//                if (is_null($modelImport)) {
//                    $import->imported += 100;
//                }
//
//                $openingHours = DentalOpeningHours::import([
//                    'public_text' => $import->opening_hours
//                ]);
//
//                $addressArray = [
//                    'company_id' => $model->company_id,
//                    'type' => 0,
//                    'country_name' => $import->country,
//                    'region' => $import->country,
//                    'locality' => $import->locality,
//                    'metro' => $import->metro,
//                    'district' => $import->district,
//                    'city_id' => 0,
//                    'street_address' => $import->address_street,
//                    'getting_there' => $import->getting_there,
//                    'extended_address' => $import->address_extended,
//                    'latitude' => $import->latitude,
//                    'longitude' => $import->longitude,
//                    'phone' => $import->phone,
//                    'opening_hours_id' => is_null($openingHours)?null:$openingHours->opening_hours_id,
//                ];
//
//                $address = DentalCompanyAddress::import($addressArray);
//                if (is_null($address)) {
//                    $import->imported += 1000;
//                }
//            }
//            $import->save(false, ['imported']);
//            $import = Company2::find()->andWhere(['imported'=>0])->one();
//        }
    }

    public function actionImport2()
    {
//        $import = Company3::find()->andWhere(['imported'=>0])->one();
//
//        while (!is_null($import)) {
//            /** @var Company3 $import */
//
//            $company = [
//                'title' => $import->title,
//                'seo_h1' => $import->title,
//                'body' => "{$import->uslugi} <hr /> {$import->pay_method}",
//                'website' => "http://" . rtrim($import->website, "http://"),
//                'email' => $import->email,
//            ];
//
//            $model = DentalCompany::import($company);
//            $import->imported = 1;
//
//            if (is_null($model)) {
//                $import->imported = 2;
//            } else {
//                $categoryID = DentalCategory::importTitle($import->category, $import->parent_category);
//                if (!is_null($categoryID)) {
//                    DentalCompanyCategory::import($model->company_id, $categoryID);
//                }
//
//                $modelImport = DentalCompanyParser::import([
//                    'company_id' => $model->company_id,
//                    'parsing_class' => null,
//                    'parsing_id' => $import->parsing_id,
//                    'parsing_time' => mktime(0, 0, 0, 6, 1, 2015),
//                    'parsing_url' => "https://2gis.ru/",
//                    'parsing_hash' => md5(implode("_", $company)),
//                ]);
//
//                if (!empty($import->vkontakte)) {
//                    DentalCompanySocial::import([
//                        'company_id' => $model->company_id,
//                        'type' => DentalCompanySocial::TYPE_VK,
//                        'url' => $import->vkontakte
//                    ]);
//                }
//                if (!empty($import->skype)) {
//                    DentalCompanySocial::import([
//                        'company_id' => $model->company_id,
//                        'type' => DentalCompanySocial::TYPE_SKYPE,
//                        'url' => $import->skype
//                    ]);
//                }
//                if (!empty($import->facebook)) {
//                    DentalCompanySocial::import([
//                        'company_id' => $model->company_id,
//                        'type' => DentalCompanySocial::TYPE_FB,
//                        'url' => $import->facebook
//                    ]);
//                }
//                if (!empty($import->instagram)) {
//                    DentalCompanySocial::import([
//                        'company_id' => $model->company_id,
//                        'type' => DentalCompanySocial::TYPE_INSTAGRAM,
//                        'url' => $import->instagram
//                    ]);
//                }
//                if (!empty($import->twitter)) {
//                    DentalCompanySocial::import([
//                        'company_id' => $model->company_id,
//                        'type' => DentalCompanySocial::TYPE_TWITTER,
//                        'url' => $import->twitter
//                    ]);
//                }
//
//
//
//                if (is_null($modelImport)) {
//                    $import->imported += 100;
//                }
//
////                $openingHours = DentalOpeningHours::import([
////                    'public_text' => $import->opening_hours
////                ]);
//
//                $addressArray = [
//                    'company_id' => $model->company_id,
//                    'type' => 0,
//                    'country_name' => $import->country,
//                    'region' => $import->country,
//                    'locality' => $import->locality,
//                    'name_build' => $import->name_build,
//                    'type_build' => $import->type_build,
//                    'city_id' => 0,
//                    'street_address' => $import->street,
//                    'extended_address' => trim(trim("{$import->house}, {$import->house_extendent}"), ","),
//                    'latitude' => str_replace(",", ".", $import->lat),
//                    'longitude' => str_replace(",", ".", $import->lon),
//                    'phone' => $import->phone,
//                    'opening_hours_id' => null,
//                ];
//
//                $address = DentalCompanyAddress::import($addressArray);
//                if (is_null($address)) {
//                    $import->imported += 1000;
//                }
//            }
//            $import->save(false, ['imported']);
//            $import = Company3::find()->andWhere(['imported'=>0])->one();
//        }
    }

    public function actionImport3()
    {
//        $all = DentalCompany::find()
//            ->andWhere(['website' => 'http://'])
//            ->select('company_id, website')
//            ->all();
//        foreach ($all as $model) {
//            if (trim($model->website) == "http://") {
//                $model->website = null;
//                $model->save(false, ['website']);
//            }
//        }
    }

    public function actionImport4()
    {
//        $find = DentalCompany::find()
//            ->andWhere(['like', 'title', ', стоматологическая клиника'])
//            ->select('company_id, title, seo_h1');
//        //1020
//        //return $this->renderContent($find->count());
//        foreach ($find->all() as $model) {
//            $model->title = str_replace(", стоматологическая клиника", "", $model->title);
//            $model->seo_h1 = str_replace(", стоматологическая клиника", "", $model->seo_h1);
//            if ($model->save(false, ['title', 'seo_h1'])) {
//                DentalCompanyCategory::import($model->company_id, 1020);
//            } else {
//                echo '<pre>'.print_r($model->errors, 1).'</pre>';
//            }
//        }
    }

    public function actionImport5()
    {
//        $find = DentalCompany::find()->andWhere([
//            'status' => 2
//        ]);
////            ->andWhere(['like', 'title', ', стоматологическая клиника'])
////            ->select('company_id, title, seo_h1');
//        //1020
//        //return $this->renderContent($find->count());
//        foreach ($find->all() as $model) {
//            $newFind = DentalCompany::find()
//                ->andWhere([
//                    'title' => $model->title,
//                    'website' => $model->website,
//                    'status' => 2
//                ])
//                ->andWhere(['<>', 'company_id', $model->company_id])
//                ->all();
//            foreach ($newFind as $modelCopy) {
//                //$modelCopy
//                foreach (DentalCompanyAddress::find()->andWhere([
//                    'company_id' => $modelCopy->company_id
//                ])->all() as $item) {
//                    $item->company_id = $model->company_id;
//                    $item->save(false, ['company_id']);
//                }
//                foreach (DentalCompanyCategory::find()->andWhere([
//                    'company_id' => $modelCopy->company_id
//                ])->all() as $item) {
//                    $item->company_id = $model->company_id;
//                    $item->save(false, ['company_id']);
//                }
//                foreach (DentalCompanyParser::find()->andWhere([
//                    'company_id' => $modelCopy->company_id
//                ])->all() as $item) {
//                    $item->company_id = $model->company_id;
//                    $item->save(false, ['company_id']);
//                }
//                foreach (DentalCompanySocial::find()->andWhere([
//                    'company_id' => $modelCopy->company_id
//                ])->all() as $item) {
//                    $item->company_id = $model->company_id;
//                    $item->save(false, ['company_id']);
//                }
//
//                $modelCopy->status = 0;
//                $modelCopy->save(false, ['status']);
//            }
////            if ($newFind>0) {
////                echo "[{$model->company_id}:{$newFind}]";
////            }
//        }
    }

    public function actionImport6()
    {
//        $find = DentalCompany::find()->leftJoin(
//            'dental_company_address',
//            'dental_company_address.company_id = dental_company.company_id '
//        )->andWhere(['dental_company.status' => 0])
//        ->andWhere(['is not', 'dental_company_address.company_address_id', null]);
//
//        foreach ($find->all() as $model) {
//            $model->status = 2;
//            $model->save(false, ['status']);
//        }
//        return $this->renderContent($find->count());
    }

    public function actionImport7()
    {
//        foreach (DentalCompanyAddress::find()->all() as $address) {
//            $address->getCitySlug();
//        }
    }

    public function actionImport8()
    {
//
//        foreach (DentalCompanyAddress::find()
//                 //     ->andWhere(['company_address_id'=>6662])
//                     ->orderBy("rand()")->limit(10)->batch(5) as $items) {
//            foreach ($items as $address) {
//                /** @var $address DentalCompanyAddress */
//                //$address->slug = null;
//                //echo '<pre>'.print_r($address->attributes, 1).'</pre>';
//                //$address->save();
//
//                echo '<pre>'.print_r($address->attributes, 1).'</pre>';
//
//                $api = new \Yandex\Geo\Api();
//                $api->setQuery($address->getText('full'));
//                $api->setLimit(1) // кол-во результатов
//                    ->setLang(\Yandex\Geo\Api::LANG_RU) // локаль ответа
//                    ->load();
//                $response = $api->getResponse();
//                if ($response->getFoundCount() > 0) {
//                    $yandexAddress = $response->getFirst();
//                    $address->country_name = $yandexAddress->getCountry();
//                    //$address->region = $yandexAddress->getAdministrativeAreaName();
//                    $address->administrative_area_name = $yandexAddress->getAdministrativeAreaName();
//                    $address->sub_administrative_area_name = $yandexAddress->getSubAdministrativeAreaName();
//                    $address->dependent_locality_name = $yandexAddress->getDependentLocalityName();
//                    $address->locality = $yandexAddress->getLocalityName();
//                    $address->street_address = $yandexAddress->getThoroughfareName();
//                    $address->fixStreet();
//
//                    $address->premise_number = $yandexAddress->getPremiseNumber();
//                    $address->extended_address = trim(trim(str_replace($address->premise_number, '', $address->extended_address), ','));
//                }
//
//                echo '<pre>'.print_r($address->attributes, 1).'</pre>';
//
//                echo '<pre>'.print_r($response->getList(), 1).'</pre>';
//                die;
//            }
//        }
    }

    public function actionImport9()
    {
//        foreach (DentalCompanyAddress::find()->select('metro, locality')->andWhere(['is not', 'metro', null])->all() as $address) {
//            $city = GeoCity::createNewOrGetExist($address->locality);
//            $metros = explode("---", $address->metro);
//            foreach ($metros as $metro) {
//
//                DentalMetro::createNewOrGetExist(trim($metro), $city->id);
//            }
//        }
    }

    public function actionImport10()
    {
//        $tmp = DentalOpeningHours::find()->orderBy('RAND()')
//            ->andWhere(['like', 'public_text', 'вых'])
//            //->andWhere(['=', 'opening_hours_id', 1090])
//            ->one();
//
//        echo '<pre>'.print_r($tmp->getArray(), 1).'</pre>';
//        $tmp->public_text = $tmp->getArrayToText();
//        echo '<pre>'.print_r($tmp->getArray(), 1).'</pre>';

//        echo '<pre>'.print_r($tmp->getArray(), 1).'</pre>';
//        echo '<pre>'.print_r($tmp->public_text, 1).'</pre>';
//        echo '<strong>'.print_r($tmp->opening_hours_id, 1).'</strong>';

//        return $this->render("moderation/_form_address_openhours", ['model' => $tmp]);
//        return $this->renderContent(
//            "<pre>". print_r($tmp->getArrayToText(), 1) ."</pre>".
//            "<pre>". print_r($tmp->getArray(), 1)."</pre>"
//        );


//        $models = DentalOpeningHours::find()->orderBy('opening_hours_id')->all();
//
//        foreach ($models as $model) {
//
//            $findCopyToBase = DentalOpeningHours::find()
//                ->where(['<>','opening_hours_id', $model->opening_hours_id])
//                ->andWhere(['public_text' => $model->public_text])
//                ->all();
//
//            foreach ($findCopyToBase as $copy) {
//                $address = DentalCompanyAddress::find()->andWhere([
//                    'opening_hours_id' => $copy->opening_hours_id
//                ])->all();
//                foreach ($address as $addres) {
//                    $addres->updateAttributes([
//                        'opening_hours_id' => $model->opening_hours_id
//                    ]);
//                }
//                $copy->delete();
//            }
//        }
//
//        return $this->renderContent("");
    }

    public function actionImport11() {

//        $models = DentalCompanyParser::find()
//            ->andWhere(['like', 'parsing_class', 'app\parser\company\Stomatologijasu'])
//            ->all();
//
//        foreach ($models as $model) {
//            foreach ($model->company->addresses as $item) {
//                $ll = $item->getYandexCoordinates();
//                if (!is_null($ll)) {
//                    $rLat = ceil($ll[0]);
//                    $rLng = ceil($ll[1]);
//
//                    $rBaseLat = ceil($item->latitude);
//                    $rBaseLng = ceil($item->longitude);
//
//                    if (($rBaseLat == $rLng) && ($rBaseLng = $rLat)) {
//                        $item->updateAttributes([
//                            'latitude' => $item->longitude,
//                            'longitude' => $item->latitude,
//                        ]);
//                    }
//                }
//            }
//        }

    }

    public function actionImport12() {
//        foreach (DentalCompany::find()->active()->all() as $company) {
//            foreach ($company->addresses as $address) {
//                $address->getCitySlug();
//            }
//        }
    }

    public function actionImport13() {
//        foreach (DentalCompanyAddress::find()
//                     ->andWhere(['IS NOT', 'locality', null])
//                     ->andWhere(['<', 'city_id', 1])
//                     ->all() as $companyAddress) {
//            $city = GeoCity::createNewOrGetExist(trim($companyAddress->locality));
//            $companyAddress->city_id = $city->id;
//
//            if ($companyAddress->save(false, ['city_id'])) {
//
//            } else {
//                echo '<pre>'.print_r($companyAddress->errors,1).'</pre>';
//            }
//        }
    }

    public function actionImport14() {
        $idFinds = [];
        $replaseId = [];



//        foreach (GeoCity::find()->andWhere(['LIKE', 'title', ','])->all() as $item) {
//            echo $item->title;
//            echo " - ";
////            $item->title = mb_strtolower($item->title);
////            $item->title = trim($item->title);
////            $item->title = ltrim($item->title, 'г');
////            $item->title = trim($item->title, ',');
////            $item->title = trim($item->title);
////            $item->title = trim($item->title, '.');
////            $item->title = trim($item->title);
////            $item->title = mb_strtoupper(mb_substr($item->title, 0, 1)) . mb_substr($item->title, 1);
////            echo $item->title;
////            echo " - ";
////            $findCity = GeoCity::createNewOrGetExist($item->title);
////            if (!is_null($findCity)) {
////                $address = DentalCompanyAddress::find()->andWhere(['city_id' => $item->id])->all();
////                foreach ($address as $itemAddress) {
////                    $itemAddress->locality = $findCity->title;
////                    $itemAddress->city_id = $findCity->id;
////                    $itemAddress->save();
////                }
////            }
////
////            if (DentalCompanyAddress::find()->andWhere(['city_id' => $item->id])->count() == 0) {
////                $item->delete();
////            }
//
//
//
//            echo "<br />";
//        }
    }

    public function actionImport15() {
//       foreach(DentalCompanyAddress::find()->andWhere(['IS', 'full_address', NULL])->limit(5000)->all() as $item) {
//           $item->full_address = $item->getText('full');
//           $item->save('false', ['full_address']);
//       }
    }

    public function actionImport16() {
//        foreach (DentalCompanyAddress::find()
//                     ->joinWith('addressMetro')
//                     ->andWhere(['IS NOT', 'metro', null])
//                     ->andWhere(['IS NOT', 'city_id', null])
//                     ->andWhere(['IS', 'dental_address_metro.metro_id', null])
//
//                     ->all() as $companyAddress) {
//            $metros = explode('---', $companyAddress->metro);
//            $metros = array_map(function($item) { return trim($item); }, $metros);
////            echo "{$companyAddress->full_address} - ";
////            echo "{$companyAddress->city_id}";
////            echo "<br />";
//            DentalAddressMetro::deleteAll(['company_address_id' => $companyAddress->company_address_id]);
//            foreach ($metros as $metroItem) {
////                echo "{$metroItem}";
////                echo "<br />";
//                $metro = DentalMetro::createNewOrGetExist(trim($metroItem), $companyAddress->city_id);
//                $new = new DentalAddressMetro([
//                   'company_address_id' => $companyAddress->company_address_id,
//                   'metro_id' => $metro->metro_id,
//                ]);
//                $new->save();
//            }
//        }
//        return $this->renderContent('');
    }

    public function actionImport17() {
        foreach(DentalCompany::find()->all() as $item) {
            foreach ($item->addresses as $address) {
                $address->status = $item->status;
                $address->save(false, ['status']);
            }
        }
    }

    /**
     * Lists all DentalCompany models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DentalCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DentalCompany model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DentalCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DentalCompany();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->company_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new DentalCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateAddress($id)
    {
        $model = $this->findModel($id);

        $address = new DentalCompanyAddress();
        $address->company_id = $model->company_id;

        $address->save();

        return $this->redirect(['moderation', 'id'=> $model->company_id]);
    }

    /**
     * Creates a new DentalCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateSocial($id)
    {
        $model = $this->findModel($id);

        $social = new DentalCompanySocial();
        $social->load(Yii::$app->request->post());
        $social->company_id = $model->company_id;

        $social->save();

        return $this->redirect(['moderation', 'id'=> $model->company_id]);
    }

    /**
     * Creates a new DentalCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionDeleteSocial($id)
    {
        $model = $this->findModelSocial($id);

        $companyID = $model->company_id;

        $model->delete();

        return $this->redirect(['moderation', 'id' => $companyID]);
    }

    /**
     * Updates an existing DentalCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->company_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DentalCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing DentalCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteAddress($id)
    {
        $model = $this->findModelAddress($id);
        $companyID = $model->company_id;

        $model->delete();

        return $this->redirect(['moderation', 'id' => $companyID]);
    }

    /**
     * Finds the DentalCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DentalCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DentalCompany::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the DentalCompanyAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DentalCompanyAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelAddress($id)
    {
        if (($model = DentalCompanyAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the DentalCompanySocial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DentalCompanySocial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelSocial($id)
    {
        if (($model = DentalCompanySocial::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
