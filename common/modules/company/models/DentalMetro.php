<?php

namespace common\modules\company\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dental_metro".
 *
 * @property integer $metro_id
 * @property integer $city_id
 * @property string $title
 * @property integer $status
 */
class DentalMetro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'title'], 'required'],
            [['city_id', 'status'], 'integer'],
            [['title'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metro_id' => Yii::t('company', 'Metro ID'),
            'city_id' => Yii::t('company', 'City ID'),
            'title' => Yii::t('company', 'Title'),
            'status' => Yii::t('company', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\query\DentalMetroQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\company\models\query\DentalMetroQuery(get_called_class());
    }


    /**
     * @param $title
     * @return array|DentalMetro|null
     */
    public static function createNewOrGetExist($title, $cityID)
    {
        $model = self::find()->andWhere(['title' => $title, 'city_id' => $cityID])->one();
        if (is_null($model)) {
            $model = new self();
            $model->title = $title;
            $model->city_id = $cityID;
            $model->status = 1;
            $model->save();
        }
        return $model;
    }

    public static function getList($cityID)
    {
        return ArrayHelper::map(self::find()->all(), 'title', 'title');
    }
}
