<?php

namespace common\modules\company\models\import;

use Yii;

/**
 * This is the model class for table "company3".
 *
 * @property integer $id
 * @property integer $parsing_id
 * @property string $parent_category
 * @property string $category
 * @property string $title
 * @property string $country
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property string $type_build
 * @property string $name_build
 * @property string $locality
 * @property string $street
 * @property string $house
 * @property string $house_extendent
 * @property string $lat
 * @property string $lon
 * @property string $pay_method
 * @property string $uslugi
 * @property string $website
 * @property string $fax
 * @property string $phone
 * @property string $icq
 * @property string $skype
 * @property string $email
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property string $vkontakte
 * @property integer $imported
 */
class Company3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company3';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_import');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parsing_id', 'imported'], 'integer'],
            [['parent_category', 'email'], 'string', 'max' => 57],
            [['category'], 'string', 'max' => 996],
            [['title'], 'string', 'max' => 231],
            [['country'], 'string', 'max' => 18],
            [['city'], 'string', 'max' => 22],
            [['postal_code'], 'string', 'max' => 14],
            [['address'], 'string', 'max' => 118],
            [['type_build'], 'string', 'max' => 78],
            [['name_build'], 'string', 'max' => 127],
            [['locality'], 'string', 'max' => 55],
            [['street'], 'string', 'max' => 58],
            [['house'], 'string', 'max' => 17],
            [['house_extendent'], 'string', 'max' => 72],
            [['lat', 'lon'], 'string', 'max' => 11],
            [['pay_method'], 'string', 'max' => 243],
            [['uslugi'], 'string', 'max' => 3144],
            [['website'], 'string', 'max' => 99],
            [['fax'], 'string', 'max' => 64],
            [['phone'], 'string', 'max' => 230],
            [['icq'], 'string', 'max' => 10],
            [['skype'], 'string', 'max' => 16],
            [['facebook'], 'string', 'max' => 70],
            [['instagram'], 'string', 'max' => 45],
            [['twitter', 'vkontakte'], 'string', 'max' => 35],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parsing_id' => 'Parsing ID',
            'parent_category' => 'Parent Category',
            'category' => 'Category',
            'title' => 'Title',
            'country' => 'Country',
            'city' => 'City',
            'postal_code' => 'Postal Code',
            'address' => 'Address',
            'type_build' => 'Type Build',
            'name_build' => 'Name Build',
            'locality' => 'Locality',
            'street' => 'Street',
            'house' => 'House',
            'house_extendent' => 'House Extendent',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'pay_method' => 'Pay Method',
            'uslugi' => 'Uslugi',
            'website' => 'Website',
            'fax' => 'Fax',
            'phone' => 'Phone',
            'icq' => 'Icq',
            'skype' => 'Skype',
            'email' => 'Email',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'vkontakte' => 'Vkontakte',
            'imported' => 'Imported',
        ];
    }
}
