<?php

namespace common\modules\company\models\import;

use Yii;

/**
 * This is the model class for table "company2".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $email
 * @property string $website
 * @property string $skype
 * @property string $instagram
 * @property string $twitter
 * @property string $facebook
 * @property string $vkontakte
 * @property string $logo
 * @property integer $status_id
 * @property string $country
 * @property string $region
 * @property string $locality
 * @property string $district
 * @property string $metro
 * @property string $address_street
 * @property string $address_extended
 * @property string $getting_there
 * @property string $latitude
 * @property string $longitude
 * @property string $opening_hours
 * @property string $phone
 * @property string $price_type
 * @property string $uslugi
 * @property integer $donor_id
 * @property integer $parsing_id
 * @property string $parsing_class
 * @property string $parsing_url
 * @property integer $parsing_time
 * @property integer $imported
 */
class Company2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company2';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_import');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body', 'uslugi'], 'string'],
            [['status_id', 'donor_id', 'parsing_id', 'parsing_time', 'imported'], 'integer'],
            [['title', 'address_street', 'address_extended', 'getting_there', 'opening_hours', 'phone', 'price_type', 'parsing_url'], 'string', 'max' => 1024],
            [['email', 'website', 'skype', 'instagram', 'twitter', 'facebook', 'vkontakte', 'logo', 'country', 'region', 'locality', 'parsing_class'], 'string', 'max' => 512],
            [['district', 'metro', 'latitude', 'longitude'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'email' => 'Email',
            'website' => 'Website',
            'skype' => 'Skype',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'facebook' => 'Facebook',
            'vkontakte' => 'Vkontakte',
            'logo' => 'Logo',
            'status_id' => 'Status ID',
            'country' => 'Country',
            'region' => 'Region',
            'locality' => 'Locality',
            'district' => 'District',
            'metro' => 'Metro',
            'address_street' => 'Address Street',
            'address_extended' => 'Address Extended',
            'getting_there' => 'Getting There',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'opening_hours' => 'Opening Hours',
            'phone' => 'Phone',
            'price_type' => 'Price Type',
            'uslugi' => 'Uslugi',
            'donor_id' => 'Donor ID',
            'parsing_id' => 'Parsing ID',
            'parsing_class' => 'Parsing Class',
            'parsing_url' => 'Parsing Url',
            'parsing_time' => 'Parsing Time',
            'imported' => 'Imported',
        ];
    }
}
