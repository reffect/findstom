<?php

namespace modules\company\models;

use Yii;

/**
 * This is the model class for table "company_nocopy".
 *
 * @property integer $id
 * @property integer $company1_id
 * @property integer $company2_id
 */
class CompanyNocopy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_nocopy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company1_id', 'company2_id'], 'required'],
            [['company1_id', 'company2_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('company', 'ID'),
            'company1_id' => Yii::t('company', 'Company1 ID'),
            'company2_id' => Yii::t('company', 'Company2 ID'),
        ];
    }
}
