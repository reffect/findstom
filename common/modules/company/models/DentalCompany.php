<?php

namespace common\modules\company\models;

use Yii;
use yii\helpers\Url;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use rabadan731\users\models\User;
use common\modules\company\models\query\DentalCompanyQuery;

/**
 * This is the model class for table "dental_company".
 *
 * @property integer $company_id
 * @property string $slug
 * @property string $title
 * @property string $body
 * @property string $seo_h1
 * @property string $seo_key
 * @property string $seo_desc
 * @property string $email
 * @property string $website
 * @property string $logo
 * @property string $logoFile
 * @property string $thumbnail
 * @property integer $isDeleteLogo
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $author
 * @property User $updater
 * 
 * @property DentalCompanyAddress[] $addresses
 * @property DentalCompanySocial[] $social
 * @property DentalCompanyCategory[] $categories
 */
class DentalCompany extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 8;
    const STATUS_GOOD = 6;
    const STATUS_NEW = 4;
    const STATUS_IMPORT = 2;
    const STATUS_CLOSE = 0;

    public $logoFile;
    public $isDeleteLogo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_company';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['logoFile'], 'safe'],
            [['body', 'seo_key', 'seo_desc'], 'string'],
            [['status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['isDeleteLogo'], 'safe', 'skipOnEmpty' => false],
            [['slug', 'title', 'seo_h1', 'email', 'website'], 'string', 'max' => 512],
            [['logo'], 'string', 'max' => 1024],
            [['logoFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('common', 'Company ID'),
            'slug' => Yii::t('common', 'Slug'),
            'title' => Yii::t('common', 'Title'),
            'body' => Yii::t('common', 'Body'),
            'seo_h1' => Yii::t('common', 'Seo H1'),
            'seo_key' => Yii::t('common', 'Seo Key'),
            'seo_desc' => Yii::t('common', 'Seo Desc'),
            'email' => Yii::t('common', 'Email'),
            'website' => Yii::t('common', 'Website'),
            'logo' => Yii::t('common', 'logo'),
            'isDeleteLogo' => Yii::t('common', 'Is Delete Logo'),
            'status' => Yii::t('common', 'Status'),
            'created_by' => Yii::t('common', 'Created By'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(
            DentalCompanyAddress::className(),
            ['company_id' => 'company_id']
        );
    }

    /**
     * @return int|string
     */
    public function getAddressCount()
    {
        return $this->getAddresses()->count();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasMany(
            DentalCompanySocial::className(),
            ['company_id' => 'company_id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(DentalCategory::className(), ['category_id' => 'category_id'])
            ->viaTable('dental_company_category', ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return DentalCompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalCompanyQuery(get_called_class());
    }

    /**
     * Link to object
     *
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['company', 'slug' => $this->slug]);
    }

    /**
     * @param $data
     * @return array|DentalCompany|null
     */
    public static function import($data)
    {
        $model = self::find()->andWhere($data)->one();
        if (is_null($model)) {
            $model = new self();
            $model->load($data, '');
            $model->status = $model::STATUS_IMPORT;
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }

    /**
     * @return bool
     */
    public function deleteLogo()
    {
        if (!empty($this->logo)) {
            $filePath = Yii::getAlias(str_replace("/files/company", "@files/company", $this->logo));
            if (file_exists($filePath)) {
                try {
                    if (unlink($filePath)) {
                        $this->updateAttributes(['logo'=>null]);
                        return true;
                    }
                } catch (\Exception $e) {
                    Yii::error("Unable to delete file: {$filePath}");
                }
            }
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteLogo();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

}
