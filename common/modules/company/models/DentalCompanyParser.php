<?php

namespace common\modules\company\models;

use Yii;
use common\modules\company\models\query\DentalCompanyParserQuery;

/**
 * This is the model class for table "dental_company_parser".
 *
 * @property integer $company_parser_id
 * @property integer $company_id
 * @property string $parsing_url
 * @property string $parsing_hash
 * @property integer $parsing_id
 * @property string $parsing_class
 * @property integer $parsing_time
 *
 * @property DentalCompany $company
 */
class DentalCompanyParser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_company_parser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'parsing_id', 'parsing_time'], 'integer'],
            [['parsing_url'], 'string', 'max' => 1024],
            [['parsing_hash'], 'string', 'max' => 33],
            [['parsing_class'], 'string', 'max' => 512],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalCompany::className(),
                'targetAttribute' => ['company_id' => 'company_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_parser_id' => Yii::t('common', 'Company Parser ID'),
            'company_id' => Yii::t('common', 'Company ID'),
            'parsing_url' => Yii::t('common', 'Parsing Url'),
            'parsing_hash' => Yii::t('common', 'Parsing Hash'),
            'parsing_id' => Yii::t('common', 'Parsing ID'),
            'parsing_class' => Yii::t('common', 'Parsing Class'),
            'parsing_time' => Yii::t('common', 'Parsing Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DentalCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @inheritdoc
     * @return DentalCompanyParserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalCompanyParserQuery(get_called_class());
    }

    /**
     * @param $data
     * @return array|DentalCompanyParser|null
     */
    public static function import($data)
    {
        $model = self::find()->andWhere($data)->one();
        if (is_null($model)) {
            $model = new self();
            $model->load($data, '');
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }
}
