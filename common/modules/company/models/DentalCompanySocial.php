<?php

namespace common\modules\company\models;

use Yii;
use common\modules\company\models\query\DentalCompanySocialQuery;

/**
 * This is the model class for table "dental_company_social".
 *
 * @property integer $company_social_id
 * @property integer $company_id
 * @property integer $type
 * @property string $url
 * @property integer $status
 *
 * @property DentalCompany $company
 */
class DentalCompanySocial extends \yii\db\ActiveRecord
{
    const TYPE_OTHER = 0;
    const TYPE_VK = 10;
    const TYPE_OK = 20;
    const TYPE_FB = 30;
    const TYPE_SKYPE = 40;
    const TYPE_TWITTER = 50;
    const TYPE_INSTAGRAM = 60;

    public $cssClass = [
        self::TYPE_FB => 'facebook',
        self::TYPE_VK => 'facebook',
        self::TYPE_TWITTER => 'twitter',
        self::TYPE_SKYPE => 'skype',
        self::TYPE_INSTAGRAM => 'instagram',
    ];

    public $faIcon = [
        self::TYPE_FB => 'fa-facebook',
        self::TYPE_VK => 'fa-vk',
        self::TYPE_TWITTER => 'fa-twitter',
        self::TYPE_SKYPE => 'fa-skype',
        self::TYPE_INSTAGRAM => 'fa-instagram',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_company_social';
    }

    /**
     * @return mixed|null
     */
    public function getCssClass()
    {
        return isset($this->cssClass[$this->type])?$this->cssClass[$this->type]:null;
    }

    /**
     * @return mixed|null
     */
    public function getFaClass()
    {
        return isset($this->faIcon[$this->type])?$this->faIcon[$this->type]:null;
    }

    public static function getListTypes()
    {
        return [
            self::TYPE_OTHER        => Yii::t("company", "Other"),
            self::TYPE_OK           => Yii::t("company", "Odnoklassniki"),
            self::TYPE_FB           => Yii::t("company", "Facebook"),
            self::TYPE_VK           => Yii::t("company", "Vkontakte"),
            self::TYPE_TWITTER      => Yii::t("company", "Twitter"),
            self::TYPE_SKYPE        => Yii::t("company", "Skype"),
            self::TYPE_INSTAGRAM    => Yii::t("company", "Instagram"),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'status', 'type'], 'integer'],
            [['url'], 'string', 'max' => 1024],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalCompany::className(),
                'targetAttribute' => ['company_id' => 'company_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_social_id' => Yii::t('common', 'Company Social ID'),
            'company_id' => Yii::t('common', 'Company ID'),
            'type' => Yii::t('common', 'Type'),
            'url' => Yii::t('common', 'Url'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DentalCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @inheritdoc
     * @return DentalCompanySocialQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalCompanySocialQuery(get_called_class());
    }

    /**
     * @param $data
     * @return array|DentalCompanySocial|null
     */
    public static function import($data)
    {
        $model = self::find()->andWhere($data)->one();
        if (is_null($model)) {
            $model = new self();
            $model->load($data, '');
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }
}
