<?php

namespace common\modules\company\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\company\models\DentalCompanyAddress]].
 *
 * @see \common\modules\company\models\DentalCompanyAddress
 */
class DentalCompanyAddressQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => 1]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalCompanyAddress[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalCompanyAddress|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
