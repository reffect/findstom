<?php

namespace common\modules\company\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\company\models\DentalCategory]].
 *
 * @see \common\modules\company\models\DentalCategory
 */
class DentalCategoryQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => 1]);
        return $this;
    }

    /**
     * @return $this
     */
    public function parent($parentID)
    {
        $this->andWhere(['parent_id' => (int)$parentID]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
