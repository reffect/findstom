<?php

namespace modules\company\models\query;

/**
 * This is the ActiveQuery class for [[\modules\company\models\DentalAddressMetro]].
 *
 * @see \modules\company\models\DentalAddressMetro
 */
class DentalAddressMetroQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \modules\company\models\DentalAddressMetro[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\company\models\DentalAddressMetro|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
