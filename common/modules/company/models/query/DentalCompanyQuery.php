<?php

namespace common\modules\company\models\query;
use common\modules\company\models\DentalCompany;

/**
 * This is the ActiveQuery class for [[\common\modules\company\models\DentalCompany]].
 *
 * @see \common\modules\company\models\DentalCompany
 */
class DentalCompanyQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['>=' ,'status', DentalCompany::STATUS_GOOD]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalCompany[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalCompany|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
