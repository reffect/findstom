<?php

namespace common\modules\company\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\company\models\DentalOpeningHours]].
 *
 * @see \common\modules\company\models\DentalOpeningHours
 */
class DentalOpeningHoursQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => 1]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalOpeningHours[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\company\models\DentalOpeningHours|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
