<?php

namespace common\modules\company\models;

use Yii;
use common\modules\company\models\query\DentalCategoryQuery;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "dental_category".
 *
 * @property integer $category_id
 * @property string $slug
 * @property integer $hits
 * @property integer $status
 * @property integer $parent_id
 * @property string $title
 * @property string $body
 * @property string $seo_h1
 * @property string $seo_key
 * @property string $seo_desc
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DentalCategory $parent
 * @property DentalCategory[] $dentalCategories
 * @property DentalCompanyCategory[] $dentalCompanyCategories
 */
class DentalCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['hits', 'status', 'parent_id', 'created_at', 'updated_at'], 'integer'],
            [['body', 'seo_key', 'seo_desc'], 'string'],
            [['slug', 'title', 'seo_h1'], 'string', 'max' => 512],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalCategory::className(),
                'targetAttribute' => ['parent_id' => 'category_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('common', 'Category ID'),
            'slug' => Yii::t('common', 'Slug'),
            'hits' => Yii::t('common', 'Hits'),
            'status' => Yii::t('common', 'Status'),
            'parent_id' => Yii::t('common', 'Parent ID'),
            'title' => Yii::t('common', 'Title'),
            'body' => Yii::t('common', 'Body'),
            'seo_h1' => Yii::t('common', 'Seo H1'),
            'seo_key' => Yii::t('common', 'Seo Key'),
            'seo_desc' => Yii::t('common', 'Seo Desc'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(DentalCategory::className(), [
            'category_id' => 'parent_id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasOne(self::className(), ['parent_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDentalCategories()
    {
        return $this->hasMany(DentalCategory::className(), [
            'parent_id' => 'category_id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDentalCompanyCategories()
    {
        return $this->hasMany(DentalCompanyCategory::className(), [
            'category_id' => 'category_id'
        ]);
    }

    /**
     * @inheritdoc
     * @return DentalCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalCategoryQuery(get_called_class());
    }


    public static function import($data)
    {
        $model = self::find()->andWhere($data)->one();
        if (is_null($model)) {
            $model = new self();
            $model->load($data, '');
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }

    /**
     * @param $title
     * @param null $parentTitle
     * @return int|null
     */
    public static function importTitle($title, $parentTitle = null)
    {
        $parent = null;
        if (!is_null($parentTitle)) {
            $parent = self::importTitle($parentTitle);
        }

        $find = self::find()->andWhere([
            'title' => $title
        ]);

        if (is_null($parentTitle)) {
            $find->andWhere(['IS', 'parent_id', null]);
        } else {
            $find->andWhere(['parent_id' => $parent->category_id]);
        }

        $model = $find->one();
        if (is_null($model)) {
            $model = new self();
            $model->title = $title;
            if (is_null($parentTitle)) {
                $model->parent_id = null;
            } else {
                $model->parent_id = $parent->category_id;
            }
            if ($model->save()) {
                return $model->category_id;
            } else {
                return null;
            }
        }
        return $model->category_id;
    }

}
