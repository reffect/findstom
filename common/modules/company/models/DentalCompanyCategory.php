<?php

namespace common\modules\company\models;

use Yii;
use yii\db\ActiveRecord;
use common\modules\company\models\query\DentalCompanyCategoryQuery;

/**
 * This is the model class for table "dental_company_category".
 *
 * @property integer $company_category_id
 * @property integer $company_id
 * @property integer $category_id
 *
 * @property DentalCategory $category
 * @property DentalCompany $company
 */
class DentalCompanyCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_company_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'category_id'], 'required'],
            [['company_id', 'category_id'], 'integer'],
            [
                ['category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalCategory::className(),
                'targetAttribute' => ['category_id' => 'category_id']
            ],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalCompany::className(),
                'targetAttribute' => ['company_id' => 'company_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_category_id' => Yii::t('common', 'Company Category ID'),
            'company_id' => Yii::t('common', 'Company ID'),
            'category_id' => Yii::t('common', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DentalCategory::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DentalCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @inheritdoc
     * @return DentalCompanyCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalCompanyCategoryQuery(get_called_class());
    }

    /**
     * @param $companyId
     * @param $categoryId
     * @return array|DentalCompanyCategory|null
     */
    public static function import($companyId, $categoryId)
    {
        $model = self::find()->andWhere([
            'company_id' => $companyId,
            'category_id' => $categoryId
        ])->one();
        if (is_null($model)) {
            $model = new self();
            $model->company_id = $companyId;
            $model->category_id = $categoryId;
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }
}
