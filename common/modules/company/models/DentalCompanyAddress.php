<?php

namespace common\modules\company\models;

use common\modules\company\models\query\DentalCompanyAddressQuery;
use modules\company\models\DentalAddressMetro;
use Yii;
use modules\geo\models\GeoCity;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "dental_company_address".
 *
 * @property integer $company_address_id
 * @property integer $company_id
 * @property integer $type
 * @property string $slug
 * @property string $country_name
 * @property string $region
 * @property string $locality
 * @property string $metro
 * @property string $district
 * @property integer $city_id
 * @property string $street_type
 * @property string $street_address
 * @property string $extended_address
 * @property string $getting_there
 * @property string $latitude
 * @property string $longitude
 * @property string $premise_number
 * @property integer $opening_hours_id
 * @property string $phone
 * @property string $parsing_hash
 * @property string $type_build
 * @property string $name_build
 * @property string $full_address
 * @property string $administrative_area_name
 * @property string $sub_administrative_area_name
 * @property string $dependent_locality_name
 * @property integer $status
 * @property integer $checked_nocopy
 *
 * @property GeoCity $city
 * @property DentalCompany $company
 * @property DentalOpeningHours $openingHours
 */
class DentalCompanyAddress extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['street_address', 'extended_address'],
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    public function getMapImage()
    {
        $url = "https://static-maps.yandex.ru/1.x/";
        return "$url?ll={$this->longitude},{$this->latitude}&size=320,320&z=17&l=map&pt={$this->longitude},{$this->latitude}";
    }


    /**
     * @param bool $updateInBase
     * @return array|null
     */
    public function getYandexCoordinates($updateInBase = false)
    {
        $api = new \Yandex\Geo\Api();
        $api->setQuery($this->getText('full'));
        $api->setLimit(1)// кол-во результатов
        ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
        ->load();
        $response = $api->getResponse();
        if ($response->getFoundCount() > 0) {
            $yandex = $response->getFirst();
            if ($updateInBase) {
                $this->updateAttributes([
                    'latitude' => $yandex->getLatitude(),
                    'longitude' => $yandex->getLongitude(),
                ]);
            }
            return [$yandex->getLatitude(), $yandex->getLongitude()];
        }
        return null;
    }

    /**
     *
     */
    public function confirmCoordinates()
    {
        $ll = $this->getYandexCoordinates();
        if (!is_null($ll)) {

            $rLng = ceil($ll[1]);

            $rBaseLat = ceil($this->latitude);

            if ($rBaseLat == $rLng) {

                $rBaseLng = ceil($this->longitude);
                $rLat = ceil($ll[0]);

                if ($rBaseLng == $rLat) {
                    $this->updateAttributes([
                        'latitude' => $this->longitude,
                        'longitude' => $this->latitude,
                    ]);
                }
            }
        }
    }


    /**
     *
     */
    public function changeDataToYandexGeoCoder()
    {
        $api = new \Yandex\Geo\Api();
        $api->setQuery($this->getText('full'));
        $api->setLimit(1)// кол-во результатов
        ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
        ->load();
        $response = $api->getResponse();
        if ($response->getFoundCount() > 0) {
            $yandexAddress = $response->getFirst();
            $this->country_name = $yandexAddress->getCountry();
            //$address->region = $yandexAddress->getAdministrativeAreaName();
            $this->administrative_area_name = $yandexAddress->getAdministrativeAreaName();
            $this->sub_administrative_area_name = $yandexAddress->getSubAdministrativeAreaName();
            $this->dependent_locality_name = $yandexAddress->getDependentLocalityName();
            $this->locality = $yandexAddress->getLocalityName();
            $this->street_address = $yandexAddress->getThoroughfareName();
            $this->fixStreet();

            $this->premise_number = $yandexAddress->getPremiseNumber();
            $this->extended_address = str_replace([' к', ' с'], ['к', 'с'], $this->extended_address);

            if (mb_strtolower($this->premise_number) == mb_strtolower($this->extended_address)) {
                $this->extended_address = null;
            }

            $this->extended_address = trim(
                trim(str_replace($this->premise_number, '', $this->extended_address), ',')
            );
            if (mb_strpos($this->premise_number, 'к') !== false) {
                $tasks = [
                    ' корп. ',
                    ' корпус ',
                    ', корп. ',
                    ', корпус ',
                    ', корп.',
                    ', стр. ',
                    ' стр. ',
                    ', стр.',
                    ' стр.',
                    '/',
                ];

                foreach ($tasks as $task) {
                    $rep = str_replace('к', $task, $this->premise_number);
                    $this->extended_address = trim(
                        trim(str_replace($rep, '', $this->extended_address), ',')
                    );
                }
            }
            if (mb_strpos($this->premise_number, 'с') !== false) {
                $tasks = [
                    ' корп. ',
                    ', корп. ',
                    ', корп.',
                    ', стр. ',
                    ', строение. ',
                    ' стр. ',
                    ' строение ',
                    ', стр.',
                    ', строение',
                    ' стр.',
                    ' строение',
                    '/',
                ];

                foreach ($tasks as $task) {
                    $rep = str_replace('с', $task, $this->premise_number);
                    $this->extended_address = trim(
                        trim(str_replace($rep, '', $this->extended_address), ',')
                    );
                }
            }

            if ("{$this->extended_address}с1" == $this->premise_number) {
                $this->extended_address = '';
            }
            if ("{$this->extended_address}к1" == $this->premise_number) {
                $this->extended_address = '';
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_company_address';
    }

    /**
     *
     */
    public function fixStreet()
    {
        $typesAddress = [
            'проспект',
            'переулок',
            'шоссе',
            'улица',
            'бульвар',
            'проезд',
            'аллея',
            'набережная',
        ];

        foreach ($typesAddress as $type) {
            if (mb_strpos($this->street_address, $type) !== false) {
                $this->street_address = trim(str_replace($type, '', $this->street_address));
                $this->street_type = trim($type);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [[
                'company_id',
                'type',
                'city_id',
                'opening_hours_id',
                'status',
                'checked_nocopy',
            ], 'integer'],
            [[
                'slug',
                'country_name',
                'region',
                'locality',
                'extended_address',
                'phone',
                'administrative_area_name',
                'sub_administrative_area_name',
                'dependent_locality_name'
            ], 'string', 'max' => 512],

            [['metro', 'district', 'street_type'], 'string', 'max' => 256],
            [['full_address'], 'string'],
            [
                ['street_address', 'getting_there', 'type_build', 'name_build'],
                'string',
                'max' => 1024
            ],
            [['latitude', 'longitude', 'premise_number'], 'string', 'max' => 128],
            [['parsing_hash'], 'string', 'max' => 33],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalCompany::className(),
                'targetAttribute' => ['company_id' => 'company_id']
            ],
            [
                ['opening_hours_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DentalOpeningHours::className(),
                'targetAttribute' => ['opening_hours_id' => 'opening_hours_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_address_id' => Yii::t('company', 'Company Address ID'),
            'company_id' => Yii::t('company', 'Company ID'),
            'type' => Yii::t('company', 'Type'),
            'slug' => Yii::t('company', 'Slug'),
            'country_name' => Yii::t('company', 'Country'),
            'region' => Yii::t('company', 'Region'),
            'locality' => Yii::t('company', 'Locality'),
            'metro' => Yii::t('company', 'Metro'),
            'district' => Yii::t('company', 'District'),
            'city_id' => Yii::t('company', 'City ID'),
            'street_address' => Yii::t('company', 'Street Address'),
            'extended_address' => Yii::t('company', 'Extended Address'),
            'getting_there' => Yii::t('company', 'Getting There'),
            'latitude' => Yii::t('company', 'Latitude'),
            'longitude' => Yii::t('company', 'Longitude'),
            'opening_hours_id' => Yii::t('company', 'Opening Hours'),
            'phone' => Yii::t('company', 'Phone'),
            'parsing_hash' => Yii::t('company', 'Parsing Hash'),
            'type_build' => Yii::t('company', 'Type Build'),
            'name_build' => Yii::t('company', 'Name Build'),
            'status' => Yii::t('company', 'Status'),
            'premise_number' => Yii::t('company', 'Premise Number'),
            'street_type' => Yii::t('company', 'Street Type'),
            'administrative_area_name' => Yii::t('company', 'Administrative Area Name'),
            'sub_administrative_area_name' => Yii::t('company', 'Sub Administrative Area Name'),
            'dependent_locality_name' => Yii::t('company', 'Dependent Locality Name'),
        ];
    }

    /**
     * @param string $type
     * @return string
     */
    public function getText($type = "normal")
    {
        switch ($type) {
            case "full" : {
                $address = [
                    $this->country_name,
                    $this->administrative_area_name,
                    $this->sub_administrative_area_name,
                    $this->dependent_locality_name,
                    $this->locality,
                    $this->street_type,
                    $this->street_address,
                    $this->premise_number,
                    $this->extended_address,
                ];
            }
                break;
            case "full2" : {
                $address = [
                    $this->country_name,
                    $this->locality,
                    $this->street_type,
                    $this->street_address,
                    $this->premise_number,
                    $this->extended_address,
                ];
            }
                break;
            case "fullAddress" : {
                $address = [
                    $this->locality,
                    $this->street_type,
                    $this->street_address,
                    $this->premise_number,
                    $this->extended_address,
                ];
            }
                break;
            case "address" : {
                $address = [
                    $this->street_type,
                    $this->street_address,
                    $this->premise_number,
                    $this->extended_address,
                ];
            }
                break;
            case "region" : {
                $address = [
                    $this->country_name,
                    $this->administrative_area_name,
                    $this->sub_administrative_area_name,
                    $this->dependent_locality_name,
                    $this->locality,
                ];
            }
                break;
            default : {
                $address =
                    is_null($this->getAttribute($type)) ?
                        [$this->locality, $this->street_address, $this->premise_number] :
                        [$this->getAttribute($type)];
            }
        }

        $address = array_filter($address, function ($item) {
            return !empty($item);
        });

        return implode(', ', $address);
    }


    public function isSimularAddress()
    {
        $full = str_replace('ё', 'е', $this->getText('full2'));
        $h = [',', '.', '-', ' '];
        $adr = mb_strtolower(str_replace($h, '', $full));
        $adr21 = mb_strtolower(str_replace($h, '', $this->full_address));


        if (empty($this->premise_number)) {
            return false;
        }

        $pm = str_replace(['к', 'с'], [', корп. ', ', стр. '], $this->premise_number);
        if (mb_strpos($this->full_address, $pm) !== false) {
            $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
            $two = $adr == $adr22;
        } else {
            $pm = str_replace(['к', 'с'], [', корпус ', ', строение '], $this->premise_number);
            if (mb_strpos($this->full_address, $pm) !== false) {
                $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                $two = $adr == $adr22;
            } else {
                $pm = str_replace(['к', 'с'], [' корпус ', ' строение '], $this->premise_number);
                if (mb_strpos($this->full_address, $pm) !== false) {
                    $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                    $two = $adr == $adr22;
                } else {
                    $pm = str_replace(['к', 'с'], [', корп ', ', стр '], $this->premise_number);
                    if (mb_strpos($this->full_address, $pm) !== false) {
                        $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                        $two = $adr == $adr22;
                    } else {
                        $pm = str_replace(['к', 'с'], [' корп. ', ' стр. '], $this->premise_number);
                        if (mb_strpos($this->full_address, $pm) !== false) {
                            $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                            $two = $adr == $adr22;
                        } else {
                            $pm = str_replace(['к', 'с'], [', корп.', ', стр.'], $this->premise_number);
                            if (mb_strpos($this->full_address, $pm) !== false) {
                                $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                                $two = $adr == $adr22;
                            } else {
                                $pm = str_replace(['к', 'с'], ['корп.', 'стр.'], $this->premise_number);
                                if (mb_strpos($this->full_address, $pm) !== false) {
                                    $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                                    $two = $adr == $adr22;
                                } else {
                                    $pm = str_replace(['к', 'с'], [', стр. ', ', корп. '], $this->premise_number);
                                    if (mb_strpos($this->full_address, $pm) !== false) {
                                        $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                                        $two = $adr == $adr22;
                                    } else {
                                        $pm = str_replace(['к', 'с'], [' стр. ', ' корп. '], $this->premise_number);
                                        if (mb_strpos($this->full_address, $pm) !== false) {
                                            $adr22 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
                                            $two = $adr == $adr22;
                                        } else {
                                            $adr22 = 'not';
                                            $two = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        $pm = str_replace(['к', 'с'], '/', $this->premise_number);
        if (mb_strpos($this->full_address, $pm) !== false) {
            $adr33 = mb_strtolower(str_replace($h, '', str_replace($pm, $this->premise_number, $full)));
            $three = $adr == $adr33;
        } else {
            $adr33 = 'not';
            $three = false;
        }


        $return = ($adr == $adr21 || $adr == "{$adr21}с1" || $adr == "{$adr21}к1" || $two || $three);

//        if (!$return) {
//            echo '<pre>'.print_r($this->premise_number,1).'</pre>';
//            echo '<pre>'.print_r($this->extended_address,1).'</pre>';
//            echo '<pre>'.print_r($this->getText('full2'),1).'</pre>';
//            echo '<pre>'.print_r($this->full_address,1).'</pre>';
//            echo '<pre>'.print_r($pm,1).'</pre>';
//            echo ''.print_r($this->getText('full2'),1).'<br />';
//
//            echo '<hr />'.print_r($adr,1).'<br />';
//            echo ''.print_r($adr21,1).'<br />';
//            var_dump($adr == $adr21);
//
//            echo '<hr />'.print_r($adr,1).'<br />';
//            echo ''.print_r("{$adr21}с1",1).'<br />';
//            var_dump($adr == "{$adr21}с1");
//
//            echo '<hr />'.print_r($adr,1).'<br />';
//            echo ''.print_r("{$adr21}к1",1).'<br />';
//            var_dump($adr == "{$adr21}к1");
//
//            echo '<hr />'.print_r($adr,1).'<br />';
//            echo ''.print_r($adr22,1).'<br />';
//            var_dump($two);
//
//            echo '<hr />'.print_r($adr,1).'<br />';
//            echo ''.print_r($adr33,1).'<br />';
//            var_dump($three);
//        }

        return $return;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DentalCompany::className(), ['company_id' => 'company_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(GeoCity::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpeningHours()
    {
        return $this->hasOne(DentalOpeningHours::className(), [
            'opening_hours_id' => 'opening_hours_id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressMetro()
    {
        return $this->hasMany(DentalAddressMetro::className(), [
            'company_address_id' => 'company_address_id'
        ]);
    }

    /**
     * @inheritdoc
     * @return DentalCompanyAddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalCompanyAddressQuery(get_called_class());
    }

    /**
     * @param $data
     * @return array|DentalCompanyAddress|null
     */
    public static function import($data)
    {
        $model = self::find()->andWhere($data)->one();
        if (is_null($model)) {
            $model = new self();
            $model->load($data, '');
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }

    /**
     * @return mixed|string
     */
    public function getCitySlug()
    {
        /** @var GeoCity $city */
        $city = $this->city;
        if (empty($city)) {
            if (!empty($this->locality)) {
                $city = GeoCity::find()->andWhere(['title' => $this->locality])->one();
                if (is_null($city)) {
                    $city = new GeoCity();
                    $city->title = $this->locality;
                    $city->region_id = 1;
                    if ($city->save()) {
                        $this->updateAttributes(['city_id' => $city->id]);
                    }
                } else {
                    $this->updateAttributes(['city_id' => $city->id]);
                }
                return $city->slug;
            } else {
                return null;
            }
        } else {
            return $city->slug;
        }
    }


    public function getPhones()
    {
        $phone = trim(trim($this->phone, 'тел:'));
        $phoneList = explode(',', $phone);
        $phoneList = array_map(function($item) {
            $temp = trim($item);
            if (strpos($temp, '8') === 0) {
                $temp = "+7" . substr($temp, 1);
            }
            return $temp;
        }, $phoneList);

        return $phoneList;
    }
}
