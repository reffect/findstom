<?php

namespace common\modules\company\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\company\models\DentalOpeningHours;

/**
 * DentalOpeningHoursSearch represents the model behind the search form about `common\modules\company\models\DentalOpeningHours`.
 */
class DentalOpeningHoursSearch extends DentalOpeningHours
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['opening_hours_id', 'opening_hours_id_id'], 'integer'],
            [['itemprop_datetime', 'public_text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DentalOpeningHours::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'opening_hours_id' => $this->opening_hours_id,
            'opening_hours_id_id' => $this->opening_hours_id_id,
        ]);

        $query->andFilterWhere(['like', 'itemprop_datetime', $this->itemprop_datetime])
            ->andFilterWhere(['like', 'public_text', $this->public_text]);

        return $dataProvider;
    }
}
