<?php

namespace common\modules\company\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\company\models\DentalCompany;

/**
 * DentalCompanySearch represents the model behind the search form about `common\modules\company\models\DentalCompany`.
 */
class DentalCompanySearch extends DentalCompany
{
    public $city_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'city_id',
                'company_id',
                'status',
                'created_by',
                'updated_by',
                'created_at',
                'updated_at'
            ], 'integer'],
            [[
                'slug',
                'title',
                'body',
                'seo_h1',
                'seo_key',
                'seo_desc',
                'email',
                'website',
                'logo'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DentalCompany::find();
        $query->with("addresses");
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if (!is_null($this->city_id)) {
            $query->leftJoin(
                'dental_company_address',
                'dental_company_address.company_id = dental_company.company_id'
            )//->groupBy('dental_company.company_id')
             ->andWhere(['dental_company_address.city_id' => $this->city_id]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_id' => $this->company_id,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'seo_h1', $this->seo_h1])
            ->andFilterWhere(['like', 'seo_key', $this->seo_key])
            ->andFilterWhere(['like', 'seo_desc', $this->seo_desc])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website', $this->website]);

        return $dataProvider;
    }
}
