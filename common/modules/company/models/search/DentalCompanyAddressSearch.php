<?php

namespace common\modules\company\models\search;

use common\modules\company\models\DentalCompany;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\company\models\DentalCompanyAddress;

/**
 * DentalCompanyAddressSearch represents the model behind the search form about `common\modules\company\models\DentalCompanyAddress`.
 */
class DentalCompanyAddressSearch extends DentalCompanyAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_address_id', 'company_id', 'type', 'city_id', 'opening_hours_id', 'status'], 'integer'],
            [['slug', 'country_name', 'administrative_area_name', 'sub_administrative_area_name', 'dependent_locality_name', 'region', 'locality', 'metro', 'district', 'street_type', 'street_address', 'premise_number', 'extended_address', 'getting_there', 'latitude', 'longitude', 'phone', 'parsing_hash', 'type_build', 'name_build'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DentalCompanyAddress::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_address_id' => $this->company_address_id,
            'company_id' => $this->company_id,
            'type' => $this->type,
            'city_id' => $this->city_id,
            'opening_hours_id' => $this->opening_hours_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'country_name', $this->country_name])
            ->andFilterWhere(['like', 'administrative_area_name', $this->administrative_area_name])
            ->andFilterWhere(['like', 'sub_administrative_area_name', $this->sub_administrative_area_name])
            ->andFilterWhere(['like', 'dependent_locality_name', $this->dependent_locality_name])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'locality', $this->locality])
            ->andFilterWhere(['like', 'metro', $this->metro])
            ->andFilterWhere(['like', 'district', $this->district])
            ->andFilterWhere(['like', 'street_type', $this->street_type])
            ->andFilterWhere(['like', 'street_address', $this->street_address])
            ->andFilterWhere(['like', 'premise_number', $this->premise_number])
            ->andFilterWhere(['like', 'extended_address', $this->extended_address])
            ->andFilterWhere(['like', 'getting_there', $this->getting_there])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'parsing_hash', $this->parsing_hash])
            ->andFilterWhere(['like', 'type_build', $this->type_build])
            ->andFilterWhere(['like', 'name_build', $this->name_build]);

        return $dataProvider;
    }


    /**
     * @param $cityID
     * @return ActiveDataProvider
     */
    public function city($cityID)
    {
        $query = DentalCompanyAddress::find()->with(['company', 'openingHours']);

        // add conditions that should always apply here

        $query->leftJoin(
            'dental_company',
            'dental_company_address.company_id = dental_company.company_id'
        )->andWhere(['>=', 'dental_company.status', DentalCompany::STATUS_GOOD]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // grid filtering conditions
        $query->andWhere([
            'city_id' => $cityID,
        ]);

        return $dataProvider;
    }
}
