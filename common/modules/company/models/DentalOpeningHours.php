<?php

namespace common\modules\company\models;

use Yii;
use common\modules\company\models\query\DentalOpeningHoursQuery;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dental_opening_hours".
 *
 * @property integer $opening_hours_id
 * @property string $itemprop_datetime
 * @property string $public_text
 *
 * @property DentalCompanyAddress[] $dentalCompanyAddresses
 */
class DentalOpeningHours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_opening_hours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itemprop_datetime'], 'string', 'max' => 128],
            [['public_text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'opening_hours_id' => Yii::t('common', 'Opening Hours ID'),
            'itemprop_datetime' => Yii::t('common', 'Itemprop Datetime'),
            'public_text' => Yii::t('common', 'Public Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDentalCompanyAddresses()
    {
        return $this->hasMany(DentalCompanyAddress::className(), [
            'opening_hours_id' => 'opening_hours_id'
        ]);
    }

    /**
     * @inheritdoc
     * @return DentalOpeningHoursQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalOpeningHoursQuery(get_called_class());
    }

    /**
     * @param $data
     * @return array|DentalOpeningHours|null
     */
    public static function import($data)
    {
        $model = self::find()->andWhere($data)->one();
        if (is_null($model)) {
            $model = new self();
            $model->load($data, '');
            if ($model->save()) {
                return $model;
            } else {
                return null;
            }
        }
        return $model;
    }

    /**
     * @return array
     */
    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'opening_hours_id', 'public_text');
    }

    public function getNiceText()
    {
        return str_replace([
            'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'
        ], [
            Yii::t("common", "Monday"),
            Yii::t("common", "Tuesday"),
            Yii::t("common", "Wednesday"),
            Yii::t("common", "Thursday"),
            Yii::t("common", "Friday"),
            Yii::t("common", "Saturday"),
            Yii::t("common", "Sunday"),
        ], $this->public_text);
    }

    /**
     * @param null $array
     * @return array
     */
    public function getText($array=null)
    {
        if (is_null($array)) {
            $array = $this->getArray();
        }
        $temp = array_map(function ($item) {
            return "{$item[0]}-{$item[1]}";
        }, $array);

        return $temp;
    }

    /**
     * @param null $array
     * @return string
     */
    public function getArrayToText($array=null)
    {
        if (is_null($array)) {
            $mas = $this->getText();
        } else {
            $mas = $this->getText($array);
        }

        $intWeekday = [
            0 => 'Mo',
            1 => 'Tu',
            2 => 'We',
            3 => 'Th',
            4 => 'Fr',
            5 => 'Sa',
            6 => 'Su'
        ];

        $out = [];
        $thisTime = $mas[0];
        $dayCount = 0;
        $weekDay = "Mo";
        for ($i = 1; $i < 7; $i++) {
            if ($thisTime != $mas[$i]) {

                if ($thisTime != '-') {
                    if ($dayCount == 0) {
                        $out[] = "{$weekDay} {$thisTime}";
                    } else {
                        $out[] = "{$weekDay}-{$intWeekday[$i-1]} {$thisTime}";
                    }
                }

                $thisTime = $mas[$i];
                $weekDay = $intWeekday[$i];
                $dayCount = 0;
            } else {
                $dayCount++;
            }
        }

        if ($thisTime != '-') {
            if ($dayCount == 0) {
                $out[] = "{$weekDay} {$thisTime}";
            } else {
                $out[] = "{$weekDay}-{$intWeekday[$i-1]} {$thisTime}";
            }
        }

        return implode(", ", $out);
    }

    /**
     *
     */
    public static function getEmptyArray()
    {
        $data = [];
        $data[0][0] = null;
        $data[0][1] = null;
        $data[1][0] = null;
        $data[1][1] = null;
        $data[2][0] = null;
        $data[2][1] = null;
        $data[3][0] = null;
        $data[3][1] = null;
        $data[4][0] = null;
        $data[4][1] = null;
        $data[5][0] = null;
        $data[5][1] = null;
        $data[6][0] = null;
        $data[6][1] = null;
        return $data;
    }


    public function getArray()
    {
        $data = self::getEmptyArray();
        try {
            $weekdayInt = [
                'Mo' => 0,
                'Tu' => 1,
                'We' => 2,
                'Th' => 3,
                'Fr' => 4,
                'Sa' => 5,
                'Su' => 6
        ];
            if (strpos($this->public_text, 'table')) {
                require_once Yii::getAlias("@backend/components/simple_html_dom.php");

                $html = \str_get_html($this->public_text);

                for ($i = 0; $i < 7; $i++) {

                    $t1 = trim($html->find("tr", 1)->find("td", $i)->innertext);
                    $t2 = trim($html->find("tr", 2)->find("td", $i)->innertext);

                    if (mb_strpos(mb_strtolower($t1), 'вых') === false
                        && mb_strpos(mb_strtolower($t2), 'вых') === false
                    ) {
                        $data[$i][0] = $t1;
                        $data[$i][1] = $t2;
                    }
                }
            } else {
                $tmp1 = array_map(function ($item) {
                    $t = str_replace("- ", '-', $item);
                    $t = str_replace(" -", '-', $t);
                    return trim($t);
                }, explode(",", $this->public_text));

                foreach ($tmp1 as $item) {
                    $tmp2 = explode(" ", trim($item));
                    if (strpos($tmp2[0], '-') !== false) {
                        $tmp3 = explode("-", trim($tmp2[0]));
                        $start = $weekdayInt[trim(trim($tmp3[0]), ":")];
                        $end = $weekdayInt[trim(trim($tmp3[1]), ":")];
                        if (strlen($start) && strlen($end)) {
                            for ($i = $start; $i <= $end; $i++) {
                                if (strpos($tmp2[1], '-') !== false) {
                                    $tmp4 = explode("-", trim($tmp2[1]));
                                } else {
                                    $tmp4[0] = "";
                                    $tmp4[1] = trim($tmp2[1]);
                                }

                                if (mb_strpos(mb_strtolower($tmp4[0]), 'вых') === false
                                    && mb_strpos(mb_strtolower($tmp4[1]), 'вых') === false
                                ) {
                                    $data[$i][0] = $tmp4[0];
                                    $data[$i][1] = $tmp4[1];
                                }
                            }
                        }
                    } else {
                        $v = trim(trim($tmp2[0]), ":");
                        if (strlen($v)) {
                            $i = $weekdayInt[$v];
                            if (strpos($tmp2[1], '-') !== false) {
                                $tmp4 = explode("-", trim($tmp2[1]));
                            } else {
                                $tmp4[0] = "";
                                $tmp4[1] = trim($tmp2[1]);
                            }

                            if (mb_strpos(mb_strtolower($tmp4[0]), 'вых') === false
                                && mb_strpos(mb_strtolower($tmp4[1]), 'вых') === false
                            ) {
                                $data[$i][0] = $tmp4[0];
                                $data[$i][1] = $tmp4[1];
                            }
                        }
                    }
                }
            }

        } catch (\Exception $e) {
            echo '<pre>'.print_r($this->public_text, 1).'</pre>';
        }
        return $data;
    }
}
