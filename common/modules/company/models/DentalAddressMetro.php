<?php

namespace modules\company\models; 

use Yii;
use modules\company\models\query\DentalAddressMetroQuery;
use modules\company\models\DentalMetro;


/**
 * This is the model class for table "dental_address_metro".
 *
 * @property integer $address_metro_id
 * @property integer $company_address_id
 * @property integer $metro_id
 */
class DentalAddressMetro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dental_address_metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_address_id', 'metro_id'], 'required'],
            [['company_address_id', 'metro_id'], 'integer'],
        ];
    }
    
        /**
     * @return \yii\db\ActiveQuery
   
    public function getMmm()
    {
        return $this->hasOne(DentalMetro::className(), ['metro_id' => 'metro_id']);
    }
  */
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'address_metro_id' => Yii::t('company', 'Address Metro ID'),
            'company_address_id' => Yii::t('company', 'Company Address ID'),
            'metro_id' => Yii::t('company', 'Metro ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return DentalAddressMetroQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DentalAddressMetroQuery(get_called_class());
    }
}
