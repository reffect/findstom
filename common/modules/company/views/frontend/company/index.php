<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $seo \common\modules\seo\models\Seo */
/* @var $searchModel common\modules\company\models\search\DentalCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $seo->breadcrumb;

?>
<section class="main-container paddi">
    <div class="container">
        <div class="dental-company-index">

            <h1><?= Html::encode($seo->seo_h1) ?></h1>
            <?php /* echo $this->render('_search', ['model' => $searchModel]); */ ?>

            <?php Pjax::begin(); ?>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => "_item"
                ]) ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</section>

