<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $seo \common\modules\seo\models\Seo */
/* @var $breadcrumb string */
/* @var $company common\modules\company\models\DentalCompany */
/* @var $socialLinks common\modules\company\models\DentalCompanySocial[] */
/* @var $address common\modules\company\models\DentalCompanyAddress */
/* @var $othersAddress common\modules\company\models\DentalCompanyAddress[] */

\themes\theproject\assets\MapAsset::register($this);

$this->params['breadcrumbs'][] = [
    'label' => $breadcrumb,
    'url' => ['index', 'city' => Yii::$app->geo->slug]
];
$this->params['breadcrumbs'][] = $seo->breadcrumb;

$socialLinks = $company->social;
$css = <<< CSS
.imglogo {
    float:left;
    padding:10px;
}
#map {
    width: 100%;
    height: 300px;
}
CSS;

$this->registerCss($css);

$js = <<<JS
    ymaps.ready(init);
    
    function init(){ 
        myMap = new ymaps.Map("map", {
            center: [{$address->latitude}, {$address->longitude}],
            zoom: 17
        }); 
        
        myPlacemark = new ymaps.Placemark([{$address->latitude}, {$address->longitude}], {
            hintContent: '{$address->text}',
            balloonContent: '{$company->title}'
        });
        
        myMap.geoObjects.add(myPlacemark);
    }
JS;

$this->registerJs($js);
?>
<br />
<section class="main-container padding-10">
    <div class="container">

        <!-- page-title start -->
        <!-- ================ -->
        <h1 class="page-title">
            <?= $company->seo_h1; ?>
        </h1>
        <!-- page-title end -->

        <!-- blogpost start -->
        <!-- ================ -->
        <article class="blogpost full">
            <header>
                <div class="post-info">
                        <span class="post-date">
                            <i class="icon-calendar"></i>
                            <span class=""><?= Yii::$app->formatter->asDate($company->created_at, 'long'); ?></span>
                        </span>
                    <span class="submitted">
                        <i class="icon-user-1"></i>
                        <a href="<?= ""; ?>"><?= $company->author->username; ?></a>
                    </span>
                    <?php /* <span class="comments"><i class="icon-chat"></i> <a href="#">22 comments</a></span> */ ?>
                </div>
            </header>
            <div class="blogpost-content">
                <?php if (!empty($company->logo)) { ?>
                    <?= Html::img($company->logo, ['class' => 'imglogo']); ?>
                <?php } ?>

                <?= $company->body; ?>
            </div>
            <footer class="clearfix">
                <?php /* <div class="tags pull-left">
                            <i class="icon-tags"></i> <a href="#">tag 1</a>, <a href="#">tag 2</a>, <a href="#">long tag 3</a>
                        </div> */ ?>
                <div class="link pull-right">
                    <ul class="social-links circle small colored clearfix margin-clear text-right animated-effect-1">
                        <?php foreach ($socialLinks as $socialLink) { ?>
                            <li class="<?= $socialLink->getCssClass(); ?>">
                                <a target="_blank" href="<?= $socialLink->url; ?>">
                                    <i class="fa <?= $socialLink->getFaClass(); ?> "></i>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </footer>
        </article>
        <!-- blogpost end -->

        <div class="row">
            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-12 space-bottom">
                <div class="row">
                    <?php /* if ($k%2!=0) { ?>
                        <div class="col-md-6">
                        <?php echo '<pre>'.print_r($address->attributes, 1).'</pre>'; ?>
                        </div>
                    <?php } */ ?>
                    <div class="col-md-6">
                        <ul class="list">
                            <li>
                                <i class="fa fa-home pr-10"></i>
                                <?= $address->getText('full'); ?>
                            </li>
                            <?php if (!empty($address->opening_hours_id)) { ?>
                                <li>
                                    <i class="fa fa-envelope-o pr-10"></i>
                                    <?= $address->openingHours->getNiceText(); ?>
                                </li>
                            <?php } ?>
                            <li>
                                <i class="fa fa-phone pr-10"></i>
                                <?= $address->phone; ?>
                            </li>
                            <?php if (!empty($company->website)) { ?>
                                <li>
                                    <a href="<?= $company->website; ?>">
                                        <i class="fa fa-globe pr-10"></i>
                                        <?= $company->website; ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (!empty($company->email)) { ?>
                                <li>
                                    <a href="mailto:<?= $company->email; ?>">
                                        <i class="fa fa-envelope-o pr-10"></i>
                                        <?= $company->email; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
            <!-- main end -->
        </div>

        <?php if (count($othersAddress)) { ?>
            <div class="row">
                <h3 class="">
                    Филиалы
                </h3>
                <?php foreach ($othersAddress as $oAddress) { ?>
                    <div class="feature-box-2 object-non-visible animated object-visible fadeInDownSmall"
                         data-animation-effect="fadeInDownSmall"
                         data-effect-delay="150">
                        <span class="icon without-bg"><i class="fa fa-connectdevelop"></i></span>
                        <div class="body">
                            <h4 class="title"><?= $oAddress->getText('full'); ?></h4>
                            <p>  </p>
                            <a href="<?= Url::to([
                                '/company/company/view',
                                'slug' => $company->slug,
                                'city' => $oAddress->getCitySlug(),
                                'address' => $oAddress->slug,
                            ]); ?>">
                                Смотреть подробнее
                                <i class="pl-5 fa fa-angle-double-right"></i>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</section>
<!-- main-container end -->
