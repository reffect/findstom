<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $seo \common\modules\seo\models\Seo */
/* @var $model \common\modules\company\models\DentalCompanyAddress */
$company = $model->company;
$url = Url::to([
    '/company/company/view',
    'slug' => $company->slug,
    'city' => Yii::$app->geo->slug,
    'address' => $model->slug
]);
?>


<div class="listing-item mb-20">
    <div class="row grid-space-0">
        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="overlay-container">
                <?= Html::img(empty($company->logo)?$model->getMapImage():$company->logo); ?>
            </div>
        </div>
        <div class="col-sm-6 col-md-8 col-lg-9">
            <div class="body">
                <h3 class="margin-clear">
                    <?= Html::a($company->title, $url); ?>
                    <small>
                        <?= $model->getText('address') ?>
                    </small>
                </h3>
                <p>
                    <?php if(!is_null($model->opening_hours_id)) { ?>
                        <span class="btn-sm-link">
                            <i class="fa fa-clock-o"></i>
                            <?= $model->openingHours->getNiceText() ?>
                        </span>
                    <?php } ?>
                </p>
                <p>
                    <?= \yii\helpers\StringHelper::truncate($company->body, 400); ?>
                </p>

                <div class="elements-list clearfix">
                    <button onclick="return location.href = '<?=$url; ?>'"
                            class="pull-right btn btn-sm btn-default-transparent btn-animated">
                        Подробнее<i class="fa fa-arrow-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
