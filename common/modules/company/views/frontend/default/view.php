<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $seo \common\modules\seo\models\Seo */
/* @var $category \common\modules\company\models\DentalCompanyCategory */
/* @var $company \common\modules\company\models\DentalCompany */
/* @var $address \common\modules\company\models\DentalCompanyAddress */
/* @var $replace String */
/* @var $controllerID string */

$this->params['breadcrumbs'][] = [
    'url' => ['/company/index'],
    'label' => 'Стоматологии'];
$this->params['breadcrumbs'][] = [
    'url' => ['/company/category', 'category' => $category->slug],
    'label' => \common\modules\seo\models\Seo::br($controllerID, 'category', $seo->replaceData, "{{category}}")];
$this->params['breadcrumbs'][] = $seo->breadcrumb;

?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">

        <!-- page-title start -->
        <!-- ================ -->
        <h1 class="page-title">
            <?= $company->seo_h1; ?>
        </h1>
        <!-- page-title end -->

        <!-- blogpost start -->
        <!-- ================ -->
        <article class="blogpost full">
            <header>
                <div class="post-info">
                            <span class="post-date">
                                <i class="icon-calendar"></i>
                                <span class=""><?= Yii::$app->formatter->asDate($company->created_at, 'long'); ?></span>
                            </span>
                            <span class="submitted">
                                <i class="icon-user-1"></i>
                                <a href="<?= $company->author->url; ?>"><?= $company->author->name; ?></a>
                            </span>
                    <?php /* <span class="comments"><i class="icon-chat"></i> <a href="#">22 comments</a></span> */ ?>
                </div>
            </header>
            <div class="blogpost-content">
                <?= $company->body; ?>
            </div>
            <footer class="clearfix">
                <?php /* <div class="tags pull-left">
                            <i class="icon-tags"></i> <a href="#">tag 1</a>, <a href="#">tag 2</a>, <a href="#">long tag 3</a>
                        </div> */ ?>
                <div class="link pull-right">
                    <ul class="social-links circle small colored clearfix margin-clear text-right animated-effect-1">
                        <?php if (!empty($company->facebook)) { ?>
                            <li class="facebook"><a target="_blank" href="<?= $company->facebook; ?>"><i
                                        class="fa fa-facebook"></i></a></li>
                        <?php } ?>
                        <?php if (!empty($company->twitter)) { ?>
                            <li class="twitter"><a target="_blank" href="<?= $company->twitter; ?>"><i
                                        class="fa fa-twitter"></i></a></li>
                        <?php } ?>
                        <?php if (!empty($company->vkontakte)) { ?>
                            <li class="facebook"><a target="_blank" href="<?= $company->vkontakte; ?>"><i
                                        class="fa fa-vk"></i></a></li>
                        <?php } ?>
                        <?php if (!empty($company->skype)) { ?>
                            <li class="skype"><a target="_blank" href="<?= $company->skype; ?>"><i
                                        class="fa fa-skype"></i></a></li>
                        <?php } ?>
                        <?php if (!empty($company->instagram)) { ?>
                            <li class="instagram"><a target="_blank" href="<?= $company->instagram; ?>"><i
                                        class="fa fa-instagram"></i></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </footer>
        </article>
        <!-- blogpost end -->
    </div>
    <!-- main end -->

    <div class="dark-bg  default-hovered footer-top animated-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call-to-action text-center">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2><?=$company->website;?></h2>
                                <h2>Перейти на сайт: <?=$company->website;?></h2>
                            </div>
                            <div class="col-sm-4">
                                <p class="mt-10">
                                    <a target="_blank" href="<?=$company->website;?>" class="btn btn-animated btn-lg btn-gray-transparent ">
                                        <?= Yii::t("common", "Open"); ?>
                                        <i class="fa fa-arrow-right pl-20"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- comments start -->
        <!-- ================ -->
        <div id="comments" class="comments">
            <h2 class="title">There are 3 comments</h2>

            <!-- comment start -->
            <div class="comment clearfix">
                <div class="comment-avatar">
                    <img class="img-circle" src="images/avatar.jpg" alt="avatar">
                </div>
                <header>
                    <h3>Comment title</h3>
                    <div class="comment-meta">By <a href="#">admin</a> | Today, 12:31</div>
                </header>
                <div class="comment-content">
                    <div class="comment-body clearfix">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo </p>
                        <a href="blog-post.html" class="btn-sm-link link-dark pull-right"><i class="fa fa-reply"></i>
                            Reply</a>
                    </div>
                </div>

                <!-- comment start -->
                <div class="comment clearfix">
                    <div class="comment-avatar">
                        <img class="img-circle" src="images/avatar.jpg" alt="avatar">
                    </div>
                    <header>
                        <h3>Comment title</h3>
                        <div class="comment-meta">By <a href="#">admin</a> | Today, 12:31</div>
                    </header>
                    <div class="comment-content">
                        <div class="comment-body clearfix">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                            <a href="blog-post.html" class="btn-sm-link link-dark pull-right"><i
                                    class="fa fa-reply"></i> Reply</a>
                        </div>
                    </div>
                </div>
                <!-- comment end -->

            </div>
            <!-- comment end -->

            <!-- comment start -->
            <div class="comment clearfix">
                <div class="comment-avatar">
                    <img class="img-circle" src="images/avatar.jpg" alt="avatar">
                </div>
                <header>
                    <h3>Comment title</h3>
                    <div class="comment-meta">By <a href="#">admin</a> | Today, 12:31</div>
                </header>
                <div class="comment-content">
                    <div class="comment-body clearfix">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo </p>
                        <a href="blog-post.html" class="btn-sm-link link-dark pull-right"><i class="fa fa-reply"></i>
                            Reply</a>
                    </div>
                </div>
            </div>
            <!-- comment end -->

        </div>
        <!-- comments end -->

        <!-- comments form start -->
        <!-- ================ -->
        <div class="comments-form">
            <h2 class="title">Add your comment</h2>
            <form role="form" id="comment-form">
                <div class="form-group has-feedback">
                    <label for="name4">Name</label>
                    <input type="text" class="form-control" id="name4" placeholder="" name="name4" required>
                    <i class="fa fa-user form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    <label for="subject4">Subject</label>
                    <input type="text" class="form-control" id="subject4" placeholder="" name="subject4" required>
                    <i class="fa fa-pencil form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    <label for="message4">Message</label>
                    <textarea class="form-control" rows="8" id="message4" placeholder="" name="message4"
                              required></textarea>
                    <i class="fa fa-envelope-o form-control-feedback"></i>
                </div>
                <input type="submit" value="Submit" class="btn btn-default">
            </form>
        </div>
        <!-- comments form end -->
    </div>
</section>