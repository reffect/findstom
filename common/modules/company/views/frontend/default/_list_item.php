<?php
use yii\helpers\Html;

/** @var $model \common\models\Company */

?>


    <div class="col-xs-12 col-sm-6 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><b><?=$model->title;?></div>
            <div class="panel-body">
                <?=Html::a(
                    "Просмотреть",
                    ['company/view','slug'=>$model->slug],
                    ['class'=>"btn btn-primary"]
                ); ?>
            </div>
        </div>
    </div>


<?php
