<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCategory */
?>
<div class="dental-category-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category_id',
            'parent_id',
            'slug',
            'hits',
            'status',
            'title',
            'body:ntext',
            'seo_h1',
            'seo_key:ntext',
            'seo_desc:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
