<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCategory */

?>
<div class="dental-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
