<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalOpeningHours */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dental-opening-hours-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'itemprop_datetime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'public_text')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('company', 'Create') : Yii::t('company', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
