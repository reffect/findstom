<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalOpeningHours */
?>
<div class="dental-opening-hours-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'opening_hours_id',
            'itemprop_datetime',
            'public_text:ntext',
            'opening_hours_id_id',
        ],
    ]) ?>

</div>
