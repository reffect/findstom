<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalOpeningHours */
?>
<div class="dental-opening-hours-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
