<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCompany */

$this->title = Yii::t('common', 'Create Dental Company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Dental Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dental-company-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
