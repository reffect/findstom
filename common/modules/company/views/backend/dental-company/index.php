<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\modules\company\models\search\DentalCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Dental Companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dental-company-index">
    <p>
        <?= Html::a(Yii::t('common', 'Create Dental Company'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                'company_id',
                'title',
                'slug',
                //'body:ntext',
                //'seo_h1',
                // 'seo_key:ntext',
                // 'seo_desc:ntext',
                // 'email:email',
                'website',
                // 'logo',
                'addressCount',
                // 'status',
                // 'created_by',
                // 'updated_by',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
