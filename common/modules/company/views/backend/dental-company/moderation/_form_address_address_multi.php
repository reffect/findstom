<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\modules\company\models\DentalCompany */
/* @var $model common\modules\company\models\DentalCompanyAddress */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-md-3">
        <?= Html::activeLabel($model, 'country_name'); ?>
        <?= Html::textInput(
            "country_name[{$model->company_address_id}]",
            $model->country_name,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-3">
        <?= Html::activeLabel($model, 'administrative_area_name'); ?>
        <?= Html::textInput(
            "administrative_area_name[{$model->company_address_id}]",
            $model->administrative_area_name,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-3">
        <?= Html::activeLabel($model, 'sub_administrative_area_name'); ?>
        <?= Html::textInput(
            "sub_administrative_area_name[{$model->company_address_id}]",
            $model->sub_administrative_area_name,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-3">
        <?= Html::activeLabel($model, 'dependent_locality_name'); ?>
        <?= Html::textInput(
            "dependent_locality_name[{$model->company_address_id}]",
            $model->dependent_locality_name,
            ['class' => ['form-control']]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <?= Html::activeLabel($model, 'locality'); ?>
        <?= Html::textInput(
            "locality[{$model->company_address_id}]",
            $model->locality,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-2">
        <?= Html::activeLabel($model, 'street_type'); ?>
        <?= Html::textInput(
            "street_type[{$model->company_address_id}]",
            $model->street_type,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-4">
        <?= Html::activeLabel($model, 'street_address'); ?>
        <?= Html::textInput(
            "street_address[{$model->company_address_id}]",
            $model->street_address,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-2">
        <?= Html::activeLabel($model, 'premise_number'); ?>
        <?= Html::textInput(
            "premise_number[{$model->company_address_id}]",
            $model->premise_number,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-2">
        <?= Html::activeLabel($model, 'extended_address'); ?>
        <?= Html::textInput(
            "extended_address[{$model->company_address_id}]",
            $model->extended_address,
            ['class' => ['form-control']]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <?= Html::activeLabel($model, 'getting_there'); ?>
        <?= Html::textInput(
            "getting_there[{$model->company_address_id}]",
            $model->getting_there,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-4">
        <?= Html::activeLabel($model, 'type_build'); ?>
        <?= Html::textInput(
            "type_build[{$model->company_address_id}]",
            $model->type_build,
            ['class' => ['form-control']]
        ); ?>
    </div>
    <div class="col-md-4">
        <?= Html::activeLabel($model, 'name_build'); ?>
        <?= Html::textInput(
            "name_build[{$model->company_address_id}]",
            $model->name_build,
            ['class' => ['form-control']]
        ); ?>
    </div>
</div>
