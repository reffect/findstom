<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $noModerateCount integer */
/* @var $countModeratedThisDay integer */
/* @var $model \common\modules\company\models\DentalCompany */

$this->title = "Осталось: {$noModerateCount}, сегодня промодерировано: {$countModeratedThisDay}";

?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                Основные данные
            </a>
        </li>
        <li class="">
            <a href="#tab_body" data-toggle="tab" aria-expanded="false">
                Текст страницы
            </a>
        </li>
        <li class="">
            <a href="#tab_2" data-toggle="tab" aria-expanded="false">
                Адреса
            </a>
        </li>
        <li class="pull-right">
            <?= Html::a("Обновить", ['moderation', 'id' => $model->company_id]) ?>
        </li>
        <li class="pull-right">
            <?= Html::a("Сохранить", ['check-company', 'id' => $model->company_id], [
                'style' => 'background: #0F0'
            ]) ?>
        </li>
        <li class="pull-right">
            <?= Html::a("Пропустить", ['moderation']) ?>
        </li>
        <li class="pull-right">
            <?= Html::a("Закрыть фирму", [
                'check-company', 'id' => $model->company_id,
                'status' => \common\modules\company\models\DentalCompany::STATUS_CLOSE
            ]) ?>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <?= $this->render("_form_base", [
                'model' => $model
            ]); ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_body">
            <?= $this->render("_form_body", [
                'model' => $model
            ]); ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            <?= $this->render("_form_address", [
                'company' => $model
            ]); ?>
        </div>

    </div>
    <!-- /.tab-content -->
</div>
<div>
    <?php if (!is_null($model->website)) { ?>
        <div class="tab-pane" id="tab_3">
            <iframe width="100%"
                    height="1000px"
                    sandbox="allow-same-origin"
                    src="<?= $model->website; ?>"
            ></iframe>
        </div>
    <?php } ?>
</div>

