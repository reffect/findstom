<?php

use yii\helpers\Html;
use yii\helpers\Url;
use rabadan731\base\widgets\ck\CKEditor;
use yii\widgets\ActiveForm;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCompany */
/* @var $form yii\widgets\ActiveForm */

$urlSaveCompany = Url::to(['set-company']);

?>

<div class="dental-company-form">

    <table class="table table-bordered">
        <tr>
            <td>
                <?= Html::activeLabel($model, 'title'); ?>
            </td>
            <td>
                <?php $editable = Editable::begin([
                    'model' => $model,
                    'attribute' => 'company_id',
                    'inputType' => Editable::INPUT_HIDDEN,
                    'asPopover' => false,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'showButtonLabels' => true,
                    'buttonsTemplate' => "{submit}",
                    'contentOptions' => ['style' => 'width:600px'],
                    'displayValue' => $model->title,
                    'options' => ['placeholder' => 'Enter location...']
                ]);
                $form = $editable->getForm();
                $form->action = $urlSaveCompany;
                echo Html::hiddenInput('attribute-primary', $model->company_id);
                $editable->afterInput = $form->field($model, 'title')->textInput(['maxlength' => true]).
                    $form->field($model, 'seo_h1')->textInput(['maxlength' => true]).
                    $form->field($model, 'slug')->textInput(['maxlength' => true]);
                Editable::end();
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Html::activeLabel($model, 'website'); ?>
            </td>
            <td>
                <?php $editable = Editable::begin([
                    'model' => $model,
                    'attribute' => 'website',
                    'inputType' => Editable::INPUT_TEXT,
                    'asPopover' => true,
                    'size' => \kartik\popover\PopoverX::SIZE_LARGE,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'showButtonLabels' => true,
                    'buttonsTemplate' => "{submit}",
                    //'contentOptions' => ['style' => 'width:550px'],
                    'displayValue' => $model->website,
                ]);
                $form = $editable->getForm();
                $form->action = $urlSaveCompany;
                echo Html::hiddenInput('attribute-primary', $model->company_id);
                Editable::end();
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= Html::activeLabel($model, 'email'); ?>
            </td>
            <td>
                <?php $editable = Editable::begin([
                    'model' => $model,
                    'attribute' => 'email',
                    'inputType' => Editable::INPUT_TEXT,
                    'asPopover' => true,
                    'size' => \kartik\popover\PopoverX::SIZE_LARGE,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'showButtonLabels' => true,
                    'buttonsTemplate' => "{submit}",
                    'contentOptions' => ['style' => 'width:100%'],
                    'displayValue' => $model->email,
                ]);
                $form = $editable->getForm();
                $form->action = $urlSaveCompany;
                echo Html::hiddenInput('attribute-primary', $model->company_id);
                Editable::end();
                ?>
            </td>
        </tr>
        <tr>
            <td><strong>Логотип</strong></td>
            <td>
                <?php $editable = Editable::begin([
                    'model' => $model,
                    //'format' => Editable::FORMAT_BUTTON,
                    'attribute' => 'logoFile',
                    'inputType' => Editable::INPUT_FILE,
                    'asPopover' => true,
                    'size' => \kartik\popover\PopoverX::SIZE_LARGE,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'showButtonLabels' => true,
                   // 'buttonsTemplate' => "{submit}",
                    'contentOptions' => ['style' => 'width:100%'],
                    'displayValue' => empty($model->logo)?null:Html::img($model->logo),
                ]);
                $form = $editable->getForm();
                $form->action = $urlSaveCompany;
                echo Html::hiddenInput('attribute-primary', $model->company_id);
                $editable->afterInput = Html::activeCheckbox($model, 'isDeleteLogo', [
                    'label' => Yii::t("common","Delete logo")
                ]);
                Editable::end();
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Социальные сети</strong>
            </td>
            <td>

                <?= $this->render("_form_social", [
                    'model' => $model
                ]); ?>
            </td>
        </tr>
    </table>
</div>
