<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\company\models\DentalCompanyAddress;
use common\modules\company\models\DentalCompany;

/**
 * @var $moderateModels DentalCompanyAddress
 * @var $model \common\modules\company\models\DentalCompanyAddress
 */

echo DentalCompanyAddress::find()->andWhere([
    'status' => DentalCompany::STATUS_IMPORT
])->count();

?>

<?php $form = ActiveForm::begin(); ?>
<?php foreach ($moderateModels as $model) { ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h1 class="box-title">
                <?= $model->full_address; ?>
            </h1>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <?= $this->render("_form_address_address_multi", [
                'form' => $form,
                'model' => $model,
            ]); ?>

        </div>
    </div>
<?php } ?>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-success">Сохранить</button>
    </div>
<?php ActiveForm::end(); ?>

<?= $good; ?>