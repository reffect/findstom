<?php

use yii\helpers\Html;
use modules\geo\models\GeoCity;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $duplicates common\modules\company\models\DentalCompanyAddress[] */

$skipAttribute = [
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',
    'slug',
    'latitude',
    'longitude',
    'status',
    'opening_hours_id',
    'company_id',
    'company_address_id',
];

$duplicates['0'] = $model;
$duplicates['1'] = $duplicate;

$sm = $duplicates['0']->company_id < $duplicates['1']->company_id ? 0 : 1;

$this->title = "Осталось: {$count}";
$form = ActiveForm::begin();
echo Html::hiddenInput("company[id_confirm]", $duplicates[$sm]->company_id);
echo Html::hiddenInput("company[id_delete]", $duplicates[$sm == 0 ? 1 : 0]->company_id);
echo Html::hiddenInput("address[id_confirm]", $duplicates[$sm]->company_address_id);
echo Html::hiddenInput("address[id_delete]", $duplicates[$sm == 0 ? 1 : 0]->company_address_id);


echo '<pre>'.print_r(count($m3),1).'</pre>';
?>


<div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $duplicates[0]->company->title; ?></h3>
            </div>
            <div class="box-body">
                <p><a href="<?= $duplicates[0]->company->website; ?>"><?= $duplicates[0]->company->website; ?></a></p>
                <p><?= $duplicates[0]->company->email; ?></p>
                <p><?= $duplicates[0]->getText('full2'); ?></p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $duplicates[1]->company->title; ?></h3>
            </div>
            <div class="box-body">
                <p><a href="<?= $duplicates[1]->company->website; ?>"><?= $duplicates[1]->company->website; ?></a></p>
                <p><?= $duplicates[1]->company->email; ?></p>
                <p><?= $duplicates[1]->getText('full2'); ?></p>
            </div>
        </div>
    </div>
</div>


    <div class="box box-default">
    <div class="box-body">


        <?php foreach ($duplicates[$sm]->company->attributes as $attribute => $value) {
            $v1 = $duplicates[0]->company->{$attribute};
            $v2 = $duplicates[1]->company->{$attribute};
            $v = (strlen($v1) > strlen($v2)) ? $v1 : $v2;

            if ($attribute == 'body') {
                echo Html::hiddenInput("company[body]", $v1 . "<br /><br />" . $v2);
                continue;
            }

            if ($attribute == 'seo_h1') {
                echo Html::hiddenInput("company[seo_h1]", $v);
                continue;
            }





            ?>
            <?php if (in_array($attribute, $skipAttribute)) {
                continue;
            } ?>
            <?php if ($duplicates[0]->company->{$attribute} == $duplicates[1]->company->{$attribute}) {
                continue;
            } ?>

            <?php if (empty($duplicates[0]->company->{$attribute}) || empty($duplicates[1]->company->{$attribute})) {
                echo Html::hiddenInput("company[{$attribute}]", $v);
                continue;
            } ?>
            <?= Html::activeLabel($duplicates[$sm]->company, $attribute); ?> <br/>
            <div class="row">
                <div class="col-md-4">
                    <div style="border: 1px solid #d2d6de;padding: 5px;text-align: right">
                        <?= $duplicates[0]->company->{$attribute}; ?>
                    </div>
                    <div style="border: 1px solid #d2d6de;padding: 5px;text-align: right">
                        <?= $duplicates[1]->company->{$attribute}; ?>
                    </div>
                </div>
                <div class="col-md-5" style="text-align: center">
                    <?= Html::textInput("company[{$attribute}]", $v, ['class' => ['form-control'], 'style'=>'margin-top:12px;']); ?>
                </div>
            </div>
            <hr />
        <?php } ?>




        <?php foreach ($duplicates[$sm]->attributes as $attribute => $value) {

            $v1 = $duplicates[0]->{$attribute};
            $v2 = $duplicates[1]->{$attribute};
            $v = (strlen($v1) > strlen($v2)) ? $v1 : $v2;


            if ($attribute == 'city_id') {
                $cityName1 = $duplicates[0]->attributes['locality'];
                $cityName2 = $duplicates[1]->attributes['locality'];
                $cityName = (strlen($cityName1) > strlen($cityName2)) ? $cityName1 : $cityName2;
                $cityModel = GeoCity::createNewOrGetExist($cityName);
                echo Html::hiddenInput("address[city_id]", $cityModel->id);
                continue;
            }
            if ($attribute == 'full_address') {
                echo Html::hiddenInput("address[full_address]", implode('||', [
                    $duplicates[0]->full_address, $duplicates[1]->full_address
                ]));
                continue;
            }
            if ($attribute == 'metro') {
                echo Html::hiddenInput("company[metro]", $v);
                continue;
            }
            ?>


            <?php if (in_array($attribute, $skipAttribute)) {
                continue;
            } ?>
            <?php if ($duplicates[0]->{$attribute} == $duplicates[1]->{$attribute}) {
                continue;
            } ?>

            <?php


            ?>

            <?php if (empty($duplicates[0]->{$attribute}) || empty($duplicates[1]->{$attribute})) {
                echo Html::hiddenInput("address[{$attribute}]", $v);
                continue;
            } ?>
            <?= Html::activeLabel($duplicates[$sm], $attribute); ?> <br/>
            <div class="row">
                <div class="col-md-4">
                    <div style="border: 1px solid #d2d6de;padding: 5px;text-align: right">
                        <?= $duplicates[0]->{$attribute}; ?>
                    </div>
                    <div style="border: 1px solid #d2d6de;padding: 5px;text-align: right">
                        <?= $duplicates[1]->{$attribute}; ?>
                    </div>
                </div>
                <div class="col-md-5" style="text-align: center">
                    <?= Html::textInput("address[{$attribute}]", $v, ['class' => ['form-control'], 'style'=>'margin-top:12px;']); ?>
                </div>
            </div>
            <hr />
        <?php } ?>


        <?php
        //[company_id] => 8169
        //[slug] => stomatologia-ug-stom
        //[title] => Стоматология Юг-Стом
        //[body] =>
        //[seo_h1] => Стоматология Юг-Стом
        //[seo_key] =>
        //[seo_desc] =>
        //[email] =>
        //[website] => http://yug-stom.ru
        //[logo] =>
        //[status] => 2
        //[created_by] => 1
        //[updated_by] => 1
        //[created_at] => 1484210243
        //[updated_at] => 1484210243


        //
        //[company_address_id] => 9255
        //[country_name] => Россия
        //[region] =>
        //[locality] => Ростов-на-Дону
        //[metro] =>
        //[district] => Ленинский район
        //[city_id] => 19
        //[street_type] => улица
        //[street_address] => Казахская
        //[premise_number] => 153
        //[extended_address] =>
        //[getting_there] =>
        //[latitude] => 47.275698
        //[longitude] => 39.736096
        //[opening_hours_id] => 157
        //[phone] => +7 (863) 231-32-81
        //[parsing_hash] =>
        //[type_build] =>
        //[name_build] =>

        ?>


        <?php
        /**
         * Created by PhpStorm.
         * User: rabadan731
         * Date: 10.11.2017
         * Time: 15:14
         */
        /*
        foreach ($duplicates as $duplicate) {
        echo '<pre>'.print_r($duplicate->company->attributes,1).'</pre>';
        echo '<pre>'.print_r($duplicate->attributes,1).'</pre>';

        }

        */
        ?>

        <?= Html::submitButton('Объединить', ['class' => 'btn btn-success']); ?>

        <?php ActiveForm::end(); ?>

        <a class="btn btn-default" href="<?= \yii\helpers\Url::to([
            'nocopy',
            'id1'=> $duplicates['0']->company_id,
            'id2'=> $duplicates['1']->company_id
        ]); ?>">Это разные компании</a>
    </div>
</div>

