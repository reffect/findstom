<?php

use yii\helpers\Html;
use yii\helpers\Url;
use rabadan731\base\widgets\ck\CKEditor;
use yii\widgets\ActiveForm;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCompany */
/* @var $form yii\widgets\ActiveForm */

$urlSaveCompany = Url::to(['set-company']);

$js = <<< JS
CKEDITOR.on('instanceCreated', function (e) {
    e.editor.on('change', function (ev) { 
        $.ajax({
            url: '{$urlSaveCompany}',
            type: 'POST',
            data: {
                hasEditable : 1, 
                'attribute-primary' : '{$model->company_id}',
                'DentalCompany': {
                    body : ev.editor.getData(),
                },
                _csrf: yii.getCsrfToken()
            }
        });
    });
});
JS;
$this->registerJs($js);

?>

<div class="dental-company-form">

    <table class="table table-bordered">
        <tr>
            <td>
                <?= Html::activeLabel($model, 'body'); ?>
            </td>
            <td>
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'body')->widget(CKEditor::className(), [
                    'options' => [
                        'id' => 'ck_editor_body',
                    ],
                    'preset' => 'full',
                    'clientOptions' => [
                        'filebrowserUploadUrl' => Url::to([
                            '/file-storage/ckload',
                            'item_dir'=> "article",
                            'item_id' => $model->getPrimaryKey()
                        ])
                    ]
                ])->label(false); ?>
                <?php ActiveForm::end(); ?>
            </td>
        </tr>
    </table>
</div>
