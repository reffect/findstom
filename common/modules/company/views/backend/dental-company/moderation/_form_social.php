<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\company\models\DentalCompanySocial;
use yii\widgets\ActiveForm;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCompany */
/* @var $form yii\widgets\ActiveForm */

$urlSaveCompany = Url::to(['set-social']);

?>
<div>
    <?php foreach ($model->social as $social) { ?>
        <?php $editable = Editable::begin([
            'options' => [
                'id' => "dentalcompanysocial-url-{$social->company_social_id}",
            ],
            'model' => $social,
            'format' => Editable::FORMAT_BUTTON,
            'attribute' => 'url',
            'inputType' => Editable::INPUT_TEXT,
            'asPopover' => true,
            'size' => \kartik\popover\PopoverX::SIZE_LARGE,
            'inlineSettings' => [
                'templateBefore' => Editable::INLINE_BEFORE_2,
                'templateAfter' => Editable::INLINE_AFTER_2
            ],
            'showButtonLabels' => true,
            'buttonsTemplate' => Html::a(
                'Удалить',
                ['delete-social', 'id' => $social->company_social_id],
                ['class'=>'btn btn-danger']
            )."{submit}",
            'contentOptions' => ['style' => 'width:100%'],
            'displayValue' => Html::a($social->url, $social->url, [
                "class" => "btn btn-default",
                "target" => "_blank",
            ])
        ]);
        $form = $editable->getForm();
        $form->action = $urlSaveCompany;
        echo Html::hiddenInput('attribute-primary', $social->company_social_id);
        Editable::end();
        ?>
        <br/>
    <?php } ?>
</div>
<div class="dental-company-social-form">

    <?php
    $newSocial = new DentalCompanySocial();
    $js = <<< JS
            $('body').on('beforeSubmit', 'form#create-social-form', function () {
                var form = $(this);
                // return false if form still have some validation errors
                if (form.find('.has-error').length) {
                    return false;
                }
                // submit form
                $.ajax({
                    url: form.attr('action'),
                    type: 'post',
                    data: form.serialize(),
                    success: function (response) {
                        
                    }
                });
                return false;
            });
JS;


    $form = ActiveForm::begin([
        'id' => 'create-social-form',
        'action' => Url::to(['create-social', 'id' => $model->company_id])
    ]);
    ?>

    <?= $form->field($newSocial, 'company_id')
        ->hiddenInput(['value' => $model->company_id])
        ->label(false);
    ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($newSocial, 'type')->dropDownList(DentalCompanySocial::getListTypes()) ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($newSocial, 'url')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('common', 'Add'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>