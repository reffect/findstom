<?php

use common\modules\company\models\DentalOpeningHours;

/* @var $this yii\web\View */
/* @var $model DentalOpeningHours */

if (is_null($model)) {
    $mas = DentalOpeningHours::getEmptyArray();
    $ohid = 0;
} else {
    $mas = $model->getArray();
    $ohid = $model->opening_hours_id;
}

$this->registerCss("

th {
    text-align: center;
    font-size: 106%;
}
td {
    text-align: center; 
    padding: 2px 8px;
}

.tr_input td input {
    width: 40%;
    text-align:center;
}

.tr_label label {
    font-weight: normal;
}
");

?>
<div style="background-color: #fff">
    <table class="table table-border">
        <tr>
            <th>Понедельник</th>
            <th>Вторник</th>
            <th>Среда</th>
            <th>Четверг</th>
            <th>Пятница</th>
            <th>Суббота</th>
            <th>Воскресенье</th>
        </tr>
        <tr class="tr_input">
            <?php for ($i = 0; $i < 7; $i++) { ?>
                <td>
                    <input type="text"
                           name="oh[<?= $i; ?>][0]"
                           value="<?= $mas[$i][0]; ?>"/>
                    -
                    <input type="text"
                           name="oh[<?= $i; ?>][1]"
                           value="<?= $mas[$i][1]; ?>"/>
                </td>
            <?php } ?>
        </tr>

    </table>
</div>