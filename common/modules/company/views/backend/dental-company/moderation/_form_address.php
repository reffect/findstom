<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\company\models\DentalOpeningHours;
use common\modules\company\models\DentalCompanyAddress;
use common\modules\company\models\DentalMetro;
use common\modules\company\assets\MapAsset;
use yii\widgets\Pjax;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $company common\modules\company\models\DentalCompany */
/* @var $model DentalCompanyAddress */
/* @var $form yii\widgets\ActiveForm */

$listOpenHours = DentalOpeningHours::getList();

$js = <<< JS



$(".setGeoCoderAddress").on('click', function () {
    companyID = $(this).data('item-id');
    
    $('#address-pjax-'+companyID).one('pjax:end', function () {
        $(("#dentalcompanyaddress-address-"+companyID+"-targ")).click();
    });
    
    $.pjax.reload(
        {
            container : '#address-pjax-'+companyID, 
            data : {setGeoCoderAddress : companyID}, 
            timeout : 20000
        }
    );
    
});
JS;

$this->registerJs($js);

MapAsset::register($this);

foreach ($company->addresses as $model) {

    $oldModel = DentalCompanyAddress::findOne($model->getPrimaryKey());
    $oldModel->changeDataToYandexGeoCoder();
    $cgca = Yii::$app->request->get('setGeoCoderAddress');
    if ($cgca !== null && $cgca == $model->company_address_id) {
        $model->changeDataToYandexGeoCoder();
    }
    ?>
    <table class="table table-bordered">
        <tr>
            <td>
                Адресс геокодера
            </td>
            <td>
                <?= $oldModel->getText('full'); ?>
                <?= Html::button('Использовать его', [
                    'class' => 'setGeoCoderAddress',
                    'data-item-id' => $oldModel->company_address_id,
                    'data-item-url' => Url::to(['set-geo-address'])
                ]); ?>
            </td>
        </tr>
        <tr>
            <td>
                Адресс
            </td>
            <td>
                <?php Pjax::begin(['id' => "address-pjax-{$oldModel->company_address_id}"]); ?>
                <?php
                $editable = Editable::begin([
                    'model' => $model,
                    'attribute' => 'company_id',
                    'inputType' => Editable::INPUT_HIDDEN,
                    'asPopover' => false,
                    'showButtonLabels' => true,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'contentOptions' => ['style' => 'width:850px'],
                    'displayValue' => $model->getText('full'),
                    'options' => [
                        'placeholder' => 'Enter location...',
                        'id' => "dentalcompanyaddress-address-{$model->company_address_id}",
                    ]
                ]);
                $form = $editable->getForm();
                $form->action = Url::to(['set-address', 'type' => 'full']);
                echo Html::hiddenInput('attribute-primary', $model->company_address_id);
                $editable->afterInput = $this->render("_form_address_address", [
                    'form' => $form,
                    'model' => $model,
                ]);
                Editable::end();
                ?>
                <?php Pjax::end(); ?>
            </td>
        </tr>

        <tr>
            <td>
                Телефон
            </td>
            <td>
                <?php
                if (strpos($model->phone, 'тел:') !== false) {
                    $model->phone = trim(trim($model->phone, "тел:"));
                    $model->updateAttributes(['phone' => trim(trim($model->phone, "тел:"))]);
                }
                $editable = Editable::begin([
                    'model' => $model,
                    'attribute' => 'phone',
                    'inputType' => Editable::INPUT_TEXT,
                    'asPopover' => false,
                    'showButtonLabels' => true,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'contentOptions' => ['style' => 'width:850px'],

                    'displayValue' => $model->phone,
                    'options' => [
                        'placeholder' => 'Enter location...',
                        'id' => "dentalcompanyaddress-phone-{$model->company_address_id}",
                    ]
                ]);
                $form = $editable->getForm();
                $form->action = Url::to(['set-address', 'type' => 'phone']);
                echo Html::hiddenInput('attribute-primary', $model->company_address_id);
                Editable::end();
                ?>
            </td>
        </tr>
        <tr>
            <td>
                Часы работы
            </td>
            <td>
                <?php
                $openingHours = $model->openingHours;
                if (is_null($openingHours)) {
                    $openingHours = new DentalOpeningHours();
                }
                $editable = Editable::begin([
                    'model' => $openingHours,
                    'attribute' => 'public_text',
                    'inputType' => Editable::INPUT_HIDDEN,
                    'asPopover' => false,
                    'showButtonLabels' => true,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    'contentOptions' => ['style' => 'width:1060px'],
                    'displayValue' => $openingHours->getNiceText(),
                    'options' => [
                        'id' => "dentalcompanyaddress-openingHours-{$model->company_address_id}",
                    ]
                ]);
                $form = $editable->getForm();
                $form->action = Url::to(['set-open-hours', 'id' => $model->company_address_id]);
                echo Html::hiddenInput('attribute-primary', $openingHours->opening_hours_id);
                $editable->afterInput = $this->render("_form_address_openhours", [
                    'model' => $openingHours
                ]);
                Editable::end();
                ?>
            </td>
        </tr>
        <?php /*  <tr>
            <td>
                Часы работы
            </td>
            <td>
                <?='<pre>'.print_r($model->openingHours->public_text, 1).'</pre>'; ?>
                <table class="table table-bordered">
                    <tr>
                        <th class="text-center">Пн <?=Html::checkbox('week[0]');?></th>
                        <th class="text-center">Вт <?=Html::checkbox('week[1]');?></th>
                        <th class="text-center">Ср <?=Html::checkbox('week[2]');?></th>
                        <th class="text-center">Чт <?=Html::checkbox('week[3]');?></th>
                        <th class="text-center">Пт <?=Html::checkbox('week[4]');?></th>
                        <th class="text-center">Сб <?=Html::checkbox('week[5]');?></th>
                        <th class="text-center">Вс <?=Html::checkbox('week[6]');?></th>
                    </tr>
                    <tr>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                        <td>
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                            -
                            <?= Html::textInput('week', null, [
                                'style' => 'width:50px'
                            ]);?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> */ ?>
        <?php /*
        <tr>
            <td>
                Метро
            </td>
            <td>
                <?php $editable = Editable::begin([
                    'model' => $model,
                    'attribute' => 'metro',
                    'inputType' => Editable::INPUT_TEXT,
                    'asPopover' => false,
                    'showButtonLabels' => true,
                    'inlineSettings' => [
                        'templateBefore' => Editable::INLINE_BEFORE_2,
                        'templateAfter' => Editable::INLINE_AFTER_2
                    ],
                    //'contentOptions' => ['style' => 'width:850px'],
                    'displayValue' => str_replace('---', ', ', $model->metro),
                ]);
                $form = $editable->getForm();
                $form->action = Url::to(['set-address', 'type' => 'metro']);
                echo Html::hiddenInput('attribute-primary', $model->company_address_id);
                Editable::end();
                ?>
            </td>
        </tr>
        */

        $model->confirmCoordinates();
        ?>
        <tr class="maps"
            data-model-id="<?= $model->company_address_id; ?>"
            data-title="<?= $company->title; ?>"
            data-set-url="<?= Url::to([
                'set-address',
                'id' => $model->company_address_id
            ]); ?>"
            data-address="<?= $model->getText('address'); ?>">
            <td>
                <?= $form->field($model, 'latitude')->textInput([
                    'class' => "latitude{$model->company_address_id}",
                    'maxlength' => true
                ]) ?>
                <?= $form->field($model, 'longitude')->textInput([
                    'class' => "longitude{$model->company_address_id}",
                    'maxlength' => true
                ]) ?>
                <br/>
                <button type="button"
                        data-address-id="<?= $model->company_address_id; ?>"
                        data-load-url="<?= Url::to([
                            'find-coordinates',
                            'id' => $model->company_address_id
                        ]); ?>"
                        class="btn btn-default search_geo">
                    Найти координаты
                </button>
            </td>
            <td>
                <div id="map<?= $model->company_address_id; ?>" style="width:100%; height:200px"></div>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td class="text-right">
                <?= Html::a(
                    Yii::t('common', 'Delete'),
                    [
                        'delete-address',
                        'id' => $model->company_address_id
                    ],
                    [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]
                ) ?>
            </td>
        </tr>
    </table>
<?php } ?>

<?= Html::a(
    "Добавить адрес",
    [
        'create-address',
        'id' => $company->company_id
    ],
    [
        'class' => 'btn btn-default',
        'data' => [
            'confirm' => "Добавить новый адрес",
            'method' => 'post',
        ],
    ]
) ?>
