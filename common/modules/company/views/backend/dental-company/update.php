<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\company\models\DentalCompany */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Dental Company',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Dental Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->company_id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="dental-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
