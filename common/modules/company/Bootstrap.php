<?php

namespace modules\company;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\company
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $rules = [];

        if (Yii::$app->id == 'app-frontend') {
            $rules['<city>/company/index'] = 'company/company/index';
            $rules['<city>/company/import'] = 'company/company/import';
            
            $rules['<city>/company/<slug>/<address>'] = 'company/company/view';
            $rules['<city>/company/<slug>'] = 'company/company/view';
        } else {
            $rules['company/default/index'] = 'company/default/index';
        }

        $app->urlManager->addRules($rules, false);
    }
}
