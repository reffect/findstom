<?php

namespace modules\company;

use Yii;

/**
 * Class Module
 * @package modules\main
 */
class Module extends \yii\base\Module
{
    /**
     * @var bool
     */
    public $isBackend = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isBackend === true) {
            $this->setViewPath('@common/modules/company/views/backend');
            $this->setLayoutPath('@backend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                'common\modules\company\controllers\backend' : $this->controllerNamespace;
        } else {
            $this->setViewPath('@common/modules/company/views/frontend');
            $this->setLayoutPath('@frontend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                'common\modules\company\controllers\frontend' : $this->controllerNamespace;
        }

        self::registerTranslations();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function registerTranslations()
    {
        $translations = Yii::$app->i18n->translations;
        if (!isset($translations['company']) && !isset($translations['company/*'])) {
            Yii::$app->i18n->translations['company'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/company/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'company' => 'company.php'
                ]
            ];
        }
    }
}
