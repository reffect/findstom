<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use modules\log\models\Log;
use modules\users\models\backend\Users;

/* @var $this yii\web\View */
/* @var $searchModel modules\log\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("common", "Logs action");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'retweet';
$this->params['place'] = 'log';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="jarviswidget">
    <div>
        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'user_id',
                    'format' => 'html',
                    'filter' => (new Users)->valuesAll(),
                    'value' => function ($model) {
                        /* @var $model Log */
                        if ($model->user !== null) {
                            return Html::a($model->user->name, ['/users/default/view', 'id' => $model->user_id]);
                        } else {
                            return Yii::t("common", "Unknown");
                        }
                    }
                ],
                'description',
                [
                    'attribute' => 'type',
                    'format' => 'html',
                    'filter' => (new Log())->valueTypes(),
                    'value' => function ($model) {
                        /* @var $model Log */
                        $title = isset($model->valueTypes()[$model->type]) ? $model->valueTypes()[$model->type] :
                            Yii::t("common", "Unknown");

                        switch ($model->type) {
                            case Log::TYPE_INSERT:
                                return '<span class="btn btn-success btn-xs btn-block">' . $title . '</span>';
                                break;
                            case Log::TYPE_UPDATE:
                                return '<span class="btn btn-primary btn-xs btn-block">' . $title . '</span>';
                                break;
                            case Log::TYPE_DELETE:
                                return '<span class="btn btn-danger btn-xs btn-block">' . $title . '</span>';
                                break;
                            default:
                                return '<span class="btn btn-xs btn-block">' . $title . '</span>';
                                break;
                        }
                    },
                ],
                'created_at:datetime',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="text-center">{view}</div>'
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>
    </div>
</div>