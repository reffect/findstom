<?php

use yii\db\Migration;

/**
 * Handles the creation for table `log`.
 */
class m160506_141207_create_table_log extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%log}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->string(250),
            'text' => $this->text(),
            'ip' => $this->string(25),
            'ua' => $this->string(200),
            'url' => $this->string(250),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('user_type', $this->table, ['user_id', 'type']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
