<?php

namespace modules\log\models;

use modules\users\models\backend\Users;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\log\models\Log;
use yii\helpers\ArrayHelper;

/**
 * LogSearch represents the model behind the search form about `modules\log\models\Log`.
 */
class LogSearch extends Log
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type', 'created_at'], 'integer'],
            [['description', 'ip', 'ua', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $userFind = Users::find()
            ->where(['agency_id'=>Yii::$app->user->identity->agency_id])
            ->asArray()
            ->select('id');

        $userIds = ArrayHelper::getColumn($userFind->all(), 'id');

        $query = Log::find()->with('user')->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (Yii::$app->user->can('admin')) {
            $query->andFilterWhere([
                'user_id' => $this->user_id
            ]);
        } else {
            if (!empty($this->user_id) && in_array($this->user_id, $userIds)) {
                $query->andFilterWhere([
                    'user_id' => $this->user_id
                ]);
            } else {
                $query->andFilterWhere([
                    'user_id' => $userIds
                ]);
            }
        }


        //'user_id' => $this->user_id,
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'ua', $this->ua])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
