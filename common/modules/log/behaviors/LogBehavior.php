<?php

namespace modules\log\behaviors;

use yii;
use yii\helpers\Url;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use modules\log\models\Log;

/**
 * Class LogBehavior
 * @link  http://rakovskiy.com
 * @package modules\log\behaviors
 * @author Rakovskiy Andrey <rakovskiy28@mail.ru>
 *
 * Данный класс предназначен для логирования действий
 * пользователей. Пример использования:
 *
 * ```php
 * use modules\log\behaviors\LogBehavior;
 *
 * public function behaviors()
 * {
 *      return [
 *          [
 *              'class' => LogBehavior::className(),
 *              'types' => [
 *                  'insert' => 'Создал нового пользователя',
 *                  'update' => 'Изменил данные пользователя',
 *                  'delete' => function ($model){
 *                      return "Удалил пользователя $model->name с id $model->id";
 *                  }
 *              ]
 *          ],
 *      ];
 * }
 * ```
 *
 * Как видно в примере с delete в качестве описания
 * действия можно использовать анонимную функцию.
 */
class LogBehavior extends Behavior
{
    /**
     * @var array
     */
    public $types = [];
    /**
     * Тип дейсвия
     * @var
     */
    protected $type;
    /**
     * Описание действия
     * @var
     */
    protected $description;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * Событие, вызываемое при добавлении новой записи
     */
    public function afterInsert()
    {
        if (isset($this->types['insert']) && $this->types['insert'] !== false) {
            $this->type = Log::TYPE_INSERT;
            $this->description = $this->text($this->types['insert']);
            $this->save();
        }
    }

    /**
     * Событие, вызываемое при изменении записи
     */
    public function beforeUpdate()
    {
        $a = $this->owner->dirtyAttributes;
        if (count($a) == 3 && isset($a['tariff_id']) && isset($a['date_begin']) && isset($a['date_end'])) {
            if (isset($this->types['tariff']) && $this->types['tariff'] !== false) {
                $this->type = Log::TYPE_UPDATE_TARIFF;
                $this->description = $this->text($this->types['tariff']);
                $this->save();
            }
        } elseif (isset($a['delete'])) {
            $this->type = Log::TYPE_DELETE;
            $this->description = $this->text($this->types['delete']);
            $this->save();
        } else {
            if (isset($this->types['update']) && $this->types['update'] !== false && count($a)) {
                $this->type = Log::TYPE_UPDATE;
                $this->description = $this->text($this->types['update']);
                $this->save();
            }
        }

        return true;
    }

    /**
     * Событие, вызываемое при удалении записи
     */
    public function afterDelete()
    {
        if (isset($this->types['delete']) && $this->types['delete'] !== false) {
            $this->type = Log::TYPE_DELETE;
            $this->description = $this->text($this->types['delete']);
            $this->save();
        }
    }

    /**
     * Получаем описание действия
     * @param $value
     * @return null
     */
    public function text($value)
    {
        if (empty($value)) {
            return null;
        } elseif (is_string($value)) {
            return $value;
        } elseif ($value instanceof \Closure) {
            return call_user_func($value, $this->owner);
        }

        return null;
    }

    /**
     * Сохраняем действие
     */
    public function save()
    {
        $model = new Log();
        $model->type = $this->type;
        $model->description = $this->description;


        $model->created_at = time();

        if (Yii::$app->id == "app-console") {
            $model->user_id = 0;
            $model->ip = '0.0.0.0';
            $model->ua = Yii::$app->id;
            $model->url = Yii::$app->id;
        } else {
            if (count($this->owner->dirtyAttributes)) {
                $res = [];
                foreach ($this->owner->dirtyAttributes as $attr => $val) {
                    $res[$attr] = ['old' => $this->owner->oldAttributes[$attr], 'new' => $val];
                }
                $model->text = print_r($res, 1);
            }
            $model->user_id = Yii::$app->user->isGuest ? 0 : Yii::$app->user->id;
            $model->ip = Yii::$app->request->userIP;
            $model->ua = Yii::$app->request->userAgent;
            $model->url = Url::to();
        }

        $model->save();
    }
}
