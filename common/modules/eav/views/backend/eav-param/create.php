<?php

$this->title = Yii::t("common", "Create {modelClass}", ["modelClass" => Yii::t("common", "Parameter")]);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Parameters of object'),
    'url' => ['/eav/eav-param/index']
];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavParam */

?>
<div class="eav-param-create padding-10">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
