<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\eav\models\EavParam;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavParam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eav-param-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'param_type')->dropDownList(EavParam::getTypes(), [
        'prompt'=>Yii::t("common","-- Select --")
    ]); ?>

    <?= $form->field($model, 'dimension')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
