<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavListItem */
?>
<div class="eav-list-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
