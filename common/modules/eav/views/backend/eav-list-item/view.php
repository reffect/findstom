<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavListItem */
?>
<div class="eav-list-item-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'list_id',
            'param_id',
        ],
    ]) ?>

</div>
