<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavList */
?>
<div class="eav-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
