<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\eav\models\search\EavListItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\modules\eav\models\EavList */
/* @var $eavParamDropDown [] */

$this->title = $model->title;
$this->params['pageTitle'] = Yii::t('common', 'Kit parameters');

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Parameters of object'),
    'url' => ['/eav/eav-param/index']
];

$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<?php \yii\widgets\Pjax::begin(['id' => ('pjax_list'),'timeout' => 10000]); ?>

<div class="eav-list-item-index padding-10">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns_item.php'),
            'toolbar'=> [
                ['content'=> ""],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => "<i class=\"glyphicon glyphicon-list\"></i> {$model->title}",
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>&nbsp; '.Yii::t("common","Delete All"),
                            ["/eav/eav-list-item/bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Are you sure?',
                                'data-confirm-message'=>'Are you sure want to delete this item'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
    <?php
    if (count($eavParamDropDown)) { ?>
        <div class="row">
            <div class="col-md-6">
                <div class="smart-form">
                    <?php echo Html::dropDownList('eav-param-value', null, $eavParamDropDown, [
                        'id' => 'eavParamValueParamId',
                        'class' => 'form-control',
                        'prompt' => Yii::t("common","-- Select --")
                    ]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <?php echo Html::a(Yii::t('common', 'Create'),
                    null,
                    [
                        'title' => Yii::t('common', 'Create'),
                        'class' => 'btn btn-primary',
                        'data-pjax'=>'0',
                        'onclick' => '
                        $.ajax({
                            type     : "POST",
                            dataType : "json",
                            data : {
                                model_id : "'.($model->id).'",
                                param_id : $("#eavParamValueParamId").val(), 
                                "'.(Yii::$app->request->csrfParam).'" : "'.(Yii::$app->request->getCsrfToken()).'"
                            },
                            url  : "'.(Url::to(['create-eav-value'])).'",
                            success  : function(data) {
                                if (data == "GOOD") {
                                    $.pjax.reload({container:"#pjax_list",async:false});
                                    $("#eavParamValueParamId").val("");
                                } else {
                                    alert("Ошибка сохранения: "+data);
                                }
                            }
                        });
                    ',
                    ]
                );  ?>
            </div>
        </div>
    <?php } else { ?>
        <?= Yii::t("common", "All parameters have been added"); ?>
    <?php } ?>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

