<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavParamValue */

?>
<div class="eav-param-value-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
