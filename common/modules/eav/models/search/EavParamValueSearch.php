<?php

namespace common\modules\eav\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\eav\models\EavParamValue;

/**
 * EavParamValueSearch represents the model behind the search form about `common\modules\eav\models\EavParamValue`.
 */
class EavParamValueSearch extends EavParamValue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'object_id', 'param_id', 'param_type'], 'integer'],
            [['object_table', 'value', 'dimension'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EavParamValue::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'object_id' => $this->object_id,
            'param_id' => $this->param_id,
            'param_type' => $this->param_type,
        ]);

        $query->andFilterWhere(['like', 'object_table', $this->object_table])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'dimension', $this->dimension]);

        return $dataProvider;
    }

    public function searchParam($table, $id) {
        $query = EavParamValue::find();

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => ['defaultOrder'=>'id asc'],
                'pagination' => false
            ]);

        $query->andFilterWhere(['like', 'object_table', $table]);

        $query->andFilterWhere(['object_id' => $id]);

        return $dataProvider;
    }

}
