<?php

namespace common\modules\eav\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\eav\models\EavListItem;

/**
 * EavListItemSearch represents the model behind the search form about `common\modules\eav\models\EavListItem`.
 */
class EavListItemSearch extends EavListItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'list_id', 'param_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EavListItem::find();

        $query->joinWith(['param']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'paramCategory' => [
                    'asc' => ['eav_param.category' => SORT_ASC],
                    'desc' => ['eav_param.category' => SORT_DESC],
                    'label' => Yii::t("common","Category")
                ],
                'paramTitle' => [
                    'asc' => ['eav_param.title' => SORT_ASC],
                    'desc' => ['eav_param.title' => SORT_DESC],
                    'label' => Yii::t("common","Parameter")
                ],

            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'list_id' => $this->list_id,
            'param_id' => $this->param_id,
        ]);

        

        return $dataProvider;
    }
}
