<?php

namespace common\modules\eav\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\eav\models\EavParam;

/**
 * EavParamSearch represents the model behind the search form about `common\modules\eav\models\EavParam`.
 */
class EavParamSearch extends EavParam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'param_type'], 'integer'],
            [['title', 'category', 'dimension'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EavParam::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'param_type' => $this->param_type,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'dimension', $this->dimension]);

        return $dataProvider;
    }
}
