<?php

namespace common\modules\eav\models;

use Yii;

/**
 * This is the model class for table "eav_list_item".
 *
 * @property integer $id
 * @property integer $list_id
 * @property integer $param_id
 *
 * @property EavList $list
 * @property EavParam $param
 */
class EavListItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eav_list_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'param_id'], 'required'],
            [['list_id', 'param_id'], 'integer'],
            [
                ['list_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => EavList::className(),
                'targetAttribute' => ['list_id' => 'id']
            ],
            [
                ['param_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => EavParam::className(),
                'targetAttribute' => ['param_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'list_id' => Yii::t('common', 'List ID'),
            'param_id' => Yii::t('common', 'Param ID'),
            'paramCategory' => Yii::t('common', 'Category'),
            'paramTitle' => Yii::t('common', 'Parameter'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(EavList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(EavParam::className(), ['id' => 'param_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\query\EavListItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\eav\models\query\EavListItemQuery(get_called_class());
    }


    public function getParamCategory()
    {
        return $this->param->category;
    }

    public function getParamTitle()
    {
        return $this->param->title;
    }
}
