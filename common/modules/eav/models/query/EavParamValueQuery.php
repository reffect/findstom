<?php

namespace common\modules\eav\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\eav\models\EavParamValue]].
 *
 * @see \common\modules\eav\models\EavParamValue
 */
class EavParamValueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavParamValue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavParamValue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function object($table, $id)
    {
        $this->andWhere(['object_table' => $table]);
        $this->andWhere(['object_id' => $id]);

        return $this;
    }

}
