<?php

namespace common\modules\eav\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\eav\models\EavList]].
 *
 * @see \common\modules\eav\models\EavList
 */
class EavListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
