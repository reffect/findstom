<?php

namespace common\modules\eav\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\eav\models\EavListItem]].
 *
 * @see \common\modules\eav\models\EavListItem
 */
class EavListItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavListItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavListItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
