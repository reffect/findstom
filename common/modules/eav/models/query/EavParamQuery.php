<?php

namespace common\modules\eav\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\eav\models\EavParam]].
 *
 * @see \common\modules\eav\models\EavParam
 */
class EavParamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavParam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\EavParam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
