<?php

namespace common\modules\eav\models;

use Yii;

/**
 * This is the model class for table "eav_list".
 *
 * @property integer $id
 * @property string $title
 * @property string $paramsCount
 *
 * @property EavListItem[] $eavListItems
 */
class EavList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eav_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEavListItems()
    {
        return $this->hasMany(EavListItem::className(), ['list_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\query\EavListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\eav\models\query\EavListQuery(get_called_class());
    }
    
    public static function getDropDownList()
    {
        $lists = self::find()->orderBy('title')->all();
        $result = [];
        foreach ($lists as $list) {
            $result[$list->id] = "{$list->title} ({$list->paramsCount})";
        }
        return $result;
    }

    public function getParamsCount()
    {
        return $this->getEavListItems()->count();
    }
}
