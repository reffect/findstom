<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_101244_eav_basic extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_param}}', [
            'id'             => $this->primaryKey(),
            'title'          => $this->string(256)->notNull(),
            'category'       => $this->string(256)->notNull(),
            'param_type'     => $this->integer()->notNull()->defaultValue(0),
            'dimension'      => $this->string(256),
        ], $tableOptions);

        $this->createTable('{{%eav_param_value}}', [
            'id'            => $this->primaryKey(),
            'object_table'  => $this->string(256)->notNull(),
            'object_id'     => $this->integer()->notNull(),
            'param_id'      => $this->integer()->notNull(),
            'param_type'    => $this->integer()->notNull()->defaultValue(0),
            'value'         => $this->string(256)->notNull(),
            'dimension'     => $this->string(256),
        ], $tableOptions);


        $this->createTable('{{%eav_list}}', [
            'id'             => $this->primaryKey(),
            'title'          => $this->string(256)->notNull(),
        ], $tableOptions);

        $this->createTable('{{%eav_list_item}}', [
            'id'             => $this->primaryKey(),
            'list_id'        => $this->integer()->notNull(),
            'param_id'        => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_eav_list_item_list_id', '{{%eav_list_item}}', 'list_id');
        $this->addForeignKey('fk_eav_list_list_id', '{{%eav_list_item}}', 'list_id', '{{%eav_list}}', 'id');

        $this->createIndex('idx_eav_list_item_param_id', '{{%eav_list_item}}', 'param_id');
        $this->addForeignKey('fk_eav_list_param_id', '{{%eav_list_item}}', 'param_id', '{{%eav_param}}', 'id');



        $this->createIndex('idx_eav_param_id', '{{%eav_param_value}}', 'param_id');
        $this->addForeignKey('fk_eav_param', '{{%eav_param_value}}', 'param_id', '{{%eav_param}}', 'id');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_eav_list_list_id', '{{%eav_list_item}}');
        $this->dropForeignKey('fk_eav_list_param_id', '{{%eav_list_item}}');
        $this->dropForeignKey('fk_eav_param', '{{%eav_param_value}}');

        $this->dropTable('{{%eav_list_item}}');
        $this->dropTable('{{%eav_list}}');
        $this->dropTable('{{%eav_param_value}}');
        $this->dropTable('{{%eav_param}}');
    }

}
