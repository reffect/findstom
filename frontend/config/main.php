<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'main' => [
            'class' => 'modules\main\Module',
        ],
        'company' => [
            'class' => 'modules\company\Module',
        ],
        'user' => [
            'class' => 'rabadan731\users\Module'
        ],
        'geo' => [
            'class' => 'modules\geo\Module'
        ],
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => '@common/files/images/store', //path to origin images
            'imagesCachePath' => '@common/files/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
            'placeHolderPath' => '@common/files/noimage.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
            'waterMark'       => false //'@frontend/web/images/watermark.png'
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'rabadan731\users\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/login',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
            ]
        ],
        'request' => [
            'baseUrl' => ''
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'view' => [
            'theme' => 'themes\theproject\Theme'
        ],
        'geo' => [
            'class' => 'modules\geo\components\Geo',
        ],
    ],
    'params' => $params,
];