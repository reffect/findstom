install:
	composer install
    composer exec 'php yii migrate/up'

autoload:
	composer dump-autoload

lint:
	composer exec 'phpcs --standard=PSR2 common backend frontend'

test: