## Install


If asset plugin not installed yet:
    
- `composer global require "fxp/composer-asset-plugin:~1.1.1"`


Install depends:

- `composer install`


Install yii2

- `php init`
- `0`
- `yes`


RBAC files:

- `chmod 666 common/modules/rbac/data/assignments.php`
- `chmod 666 common/modules/rbac/data/items.php`
- `chmod 666 common/modules/rbac/data/rules.php`


Fill DB access /common/config/main-local.php

Migrations
    
- `php yii migrate`

migrationPath
- `--migrationPath=@modules/log/migrations`
- `--migrationPath=@modules/users/migrations`
- `--migrationPath=@modules/agency/migrations`


Nginx config:

    server {
    	charset utf-8;
    
    	listen 80;
    	listen [::]:80;
    
    	server_name example.loc www.example.loc;
    	root        /path/to/example.loc;
    	index       index.php;
    
        location /files {
              alias /path/to/example.loc/common/files/;
        }
        
    	location / {
    		if ($request_uri ~ "^/backend"){
    			rewrite ^/backend/(.*)$ /backend/web/$1;
    		}
    		if ($request_uri !~ "^/backend"){
    			rewrite ^(.*)$ /frontend/web/$1;
    		}
    	}
    
    	location /frontend/web/ {
    		if (!-e $request_filename){
    			rewrite ^(.*)$ /frontend/web/index.php;
    		}
    	}
    
    	location /backend/web/ {
    		if (!-e $request_filename){
    			rewrite ^(.*)$ /backend/web/index.php;
    		}
    	}
    
    	location ~ \.php$ {
            	try_files $uri =404;
            	fastcgi_split_path_info ^(.+\.php)(/.+)$;
            	fastcgi_pass unix:/var/run/php5-fpm.sock;
            	fastcgi_index index.php;
            	include fastcgi_params;
        	}
    }
    
Do not forget change "/path/to/example.loc" and "example.loc" to your own.


Apach config (on .htaccess):

    Options FollowSymLinks
    AddDefaultCharset utf-8
    
    <IfModule mod_rewrite.c>
    
        RewriteEngine On
    
        RewriteCond %{REQUEST_URI} ^/files
        RewriteRule ^files(.*) /common/files/$1 [L]
        
        # the main rewrite rule for the frontend application
        RewriteCond %{REQUEST_URI} !^/(backend/web|backend)
        RewriteRule !^frontend/web /frontend/web%{REQUEST_URI} [L]
    
        # the main rewrite rule for the backend application
        RewriteCond %{REQUEST_URI} ^/backend
        RewriteRule ^backend(.*) /backend/web/$1 [L]
    
        # if a directory or a file of the frontend application exists, use the request directly
        RewriteCond %{REQUEST_URI} ^/frontend/web
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        # otherwise forward the request to index.php
        RewriteRule . /frontend/web/index.php [L]
    
        # if a directory or a file of the backend application exists, use the request directly
        RewriteCond %{REQUEST_URI} ^/backend/web
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        # otherwise forward the request to index.php
        RewriteRule . /backend/web/index.php [L]
    
        RewriteCond %{REQUEST_URI} \.(htaccess|htpasswd|svn|git)
        RewriteRule \.(htaccess|htpasswd|svn|git) - [F]
    </IfModule>
