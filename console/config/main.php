<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerMap' => [
        'migrate'=>[
            'class'=>'app\commands\MigrateController',
            'migrationLookup'=>[
                '@common/modules/company/migrations',
                '@common/modules/eav/migrations',
                '@common/modules/geo/migrations',
                '@common/modules/log/migrations',
                '@common/modules/main/migrations',
                '@common/modules/seo/migrations',
                '@rabadan731/users/migrations',
            ]
        ]
    ],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
